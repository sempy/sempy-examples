"""
Halo to Halo direct transfer in the CR3BP model.
================================================

This example demonstrates the use of the CR3BP Lambert solver to compute a direct transfer
between two specified Earth-Moon Halo orbits. Boundary conditions and time of flight are taken
from Davis et al. [1] and summarized below:

Departure and arrival orbits:

+-----------------+-----------------+-----------------+-----------------+-----------------+
| Orbit           | Family          | Type            | Jacobi constant | Period          |
+=================+=================+=================+=================+=================+
| Departure       | L2 southern     | Halo            | 3.0327          | 7.5 days        |
+-----------------+-----------------+-----------------+-----------------+-----------------+
| Arrival         | L2 southern     | Halo            | 3.06            | 13.6 days       |
+-----------------+-----------------+-----------------+-----------------+-----------------+

Two-impulses direct transfer trajectory:

+-----------------+-----------------+-----------------+-----------------+-----------------+
| Departure       | Arrival         | First impulse   | Second impulse  | Time of Flight  |
+=================+=================+=================+=================+=================+
| 1.07 days coast | 4.51 days coast | 48.00 m/s       | 69.01 m/s       | 12.68 days      |
+-----------------+-----------------+-----------------+-----------------+-----------------+

[1] Davis et al., *Locally Optimal Transfers Between Libration Point Orbits Using Invariant
Manifolds*, Advances in the Astronautical Sciences, vol. 135, 2009.

@author: Alberto FOSSA'

"""

# %%
# Import statements
#
# As usual, we will start importing all required modules and classes to generate the selected
# departure and arrival orbits and compute the Lambert arc.

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.constants import DAYS2SEC
from sempy.core.orbits.halo import Halo
from sempy.core.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm
from sempy.core.plotting.simple.utils import decorate_3d_axes
from sempy.core.diffcorr.ode_event import generate_impact_event

# %%
# Dynamical model
#
# At this point, we define the Earth-Moon CR3BP system as the dynamical model in which the
# transfer is computed.

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
l_c, t_c = cr3bp.L, cr3bp.T / 2.0 / np.pi  # CR3BP characteristic length [km] and time [s]

impact = generate_impact_event(cr3bp, 'm2')

# %%
# Departure and arrival orbits
#
# Then, the departure and arrival L2 southern Halo orbits are instantiated starting from the
# values of their respective periods reported in the first table above.

t_halo_dep = 12 * DAYS2SEC / t_c
t_halo_arr = 14 * DAYS2SEC / t_c

halo_dep = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, T=t_halo_dep)
halo_arr = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=t_halo_arr)

halo_dep.interpolation()
halo_arr.interpolation()

time_dep = 10.6 * DAYS2SEC / t_c
time_arr = 19 * DAYS2SEC / t_c
time_lamb = [5 * DAYS2SEC / t_c]

theta_dep = [0.2]
theta_arr = [0.14]

# % Manifold generation
dv_list = []
th_list = []
for th_dep in theta_dep:
    for th_arr in theta_arr:
        for t_lamb in time_lamb:
            branch_dep = halo_dep.ManifoldBranch(halo_dep, 'unstable', 'exterior')
            branch_dep.computation(th_dep, time_dep, events=impact)
            branch_arr = halo_arr.ManifoldBranch(halo_arr, 'stable', 'interior')
            branch_arr.computation(th_arr, time_arr, events=impact)
            # Lambert problem object
            lamb_pbm = Cr3bpLambertPbm(cr3bp, cr3bp.m2)
            # Lambert problem solution
            state1 = branch_dep.state_vec[-1, :6]
            state2 = branch_arr.state_vec[-1, :6]
            sol = lamb_pbm.solve(tof=t_lamb, nb_revs=1, nb_pts=2, guess='r2bp',
                                 state1=state1, state2=state2, verbose=False)
            dv_tuple = sol[5] * l_c / t_c * 1e3
            dv_list.append(dv_tuple[2])  # dV1, dV2 and total dV magnitude [
            # m/s]
            th_list.append([th_dep, th_arr, t_lamb])


# %
th_dep_best, th_arr_best, t_lamb_best = th_list[np.argmin(dv_list)]
print(f"Best departure theta: {th_dep_best}")
print(f"Best arrival theta: {th_arr_best}")
print(f"Best lambert time: {t_lamb_best}")
print(f"Best DV: {dv_list[np.argmin(dv_list)]}")

# %%
branch_dep = halo_dep.ManifoldBranch(halo_dep, 'unstable', 'exterior')
branch_dep.computation(th_dep_best, time_dep, events=impact)

branch_arr = halo_arr.ManifoldBranch(halo_arr, 'stable', 'interior')
branch_arr.computation(th_arr_best, time_arr, events=impact)

# Lambert problem object

lamb_pbm = Cr3bpLambertPbm(cr3bp, cr3bp.m2)

# Lambert problem solution

state1 = branch_dep.state_vec[-1, :6]
state2 = branch_arr.state_vec[-1, :6]

sol = lamb_pbm.solve(tof=t_lamb_best, nb_revs=1, nb_pts=2, guess='r2bp',
                     state1=state1, state2=state2, verbose=False)

dv = sol[5] * l_c / t_c * 1e3  # dV1, dV2 and total dV magnitude

# %
# Patch point propagation
#
# Once the Lambert Problem has been solved, a continuous transfer trajectory is obtained
# propagating the corrected patch points returned by the aforementioned `solve` method.
# The propagation is accomplished with the `Cr3bpLambertPbm` method `propagate` which ensure the
# same dynamical model and scaling parameters are used for the explicit integration of the
# corrected transfer arc.

t_vec, state_vec = lamb_pbm.propagate(sol[1], sol[2])

# %%
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = "14"
plt.rcParams['xtick.major.pad'] = '10'
plt.rcParams['ytick.major.pad'] = '10'
# Three-dimensional trajectory plot
#
# Finally, the departure orbit, the arrival orbit and the corresponding transfer trajectory are
# plotted on a three-dimensional figure for a visual inspection of the computed results.

fig = plt.figure(figsize=(8, 7))
ax = fig.add_subplot(projection='3d')

ax.plot(halo_dep.state_vec[:, 0] * l_c, halo_dep.state_vec[:, 1] * l_c,
        halo_dep.state_vec[:, 2] * l_c, label='Dep. Halo', zorder=9)
ax.plot(branch_dep.state_vec[:, 0] * l_c,
        branch_dep.state_vec[:, 1] * l_c,
        branch_dep.state_vec[:, 2] * l_c,
        label='Dep. manifold', color='skyblue')
ax.plot(state_vec[:, 0] * l_c,
        state_vec[:, 1] * l_c,
        state_vec[:, 2] * l_c,
        label='Patching arc', color='tab:green', zorder=7)
ax.plot(branch_arr.state_vec[:, 0] * l_c,
        branch_arr.state_vec[:, 1] * l_c,
        branch_arr.state_vec[:, 2] * l_c,
        label='Arr. manifold', color='lightsalmon')
ax.plot(halo_arr.state_vec[:, 0] * l_c,
        halo_arr.state_vec[:, 1] * l_c,
        halo_arr.state_vec[:, 2] * l_c,
        label='Arr. Halo', zorder=9, color='tab:red')

ax.scatter(cr3bp.l1.position[0]*cr3bp.L, 0, 0, marker='*', color='b', label='$L_1$')
ax.scatter(cr3bp.l2.position[0]*cr3bp.L, 0, 0, marker='*', color='r', label='$L_2$')
ax.scatter(cr3bp.m2_pos[0]*cr3bp.L, 0, 0, marker='.', color='grey', label='Moon', s=150)
ax.scatter(np.nan, np.nan, np.nan, color='none', label=' ')
ax.scatter(np.nan, np.nan, np.nan, color='none', label=' ')

ax.view_init(elev=25., azim=-108)
decorate_3d_axes(ax, '', 'km')
ax.set_xlabel('x [km]', labelpad=20)
ax.set_ylabel('y [km]', labelpad=20)
ax.set_zlabel('z [km]', labelpad=20)
ax.legend(loc='upper right', bbox_to_anchor=(1, 1), fancybox=True, shadow=True, ncol=2)
fig.tight_layout()


# %%
fig = plt.figure()
ax = fig.add_subplot()
ax.grid(True, zorder=0)
ax.plot(halo_dep.state_vec[:, 0] * l_c, halo_dep.state_vec[:, 1] * l_c, label='Dep. Halo', zorder=9)
ax.plot(halo_arr.state_vec[:, 0] * l_c, halo_arr.state_vec[:, 1] * l_c, label='Arr. Halo',
        zorder=9, color='tab:red')
ax.plot(state_vec[:, 0] * l_c, state_vec[:, 1] * l_c, label='Transfer', color='tab:green')
ax.plot(branch_dep.state_vec[:, 0] * l_c, branch_dep.state_vec[:, 1] * l_c,
        label='Dep. branch', color='skyblue')
ax.plot(branch_arr.state_vec[:, 0] * l_c, branch_arr.state_vec[:, 1] * l_c,
        label='Arr. branch', color='lightsalmon')
ax.scatter(cr3bp.l2.position[0]*cr3bp.L, 0, marker='*', color='r', label='$L_2$', zorder=9)
ax.scatter(cr3bp.m2_pos[0]*cr3bp.L, 0, marker='.', color='grey', label='Moon', zorder=9, s=150)
ax.set_xlabel('x [km]')
ax.set_ylabel('y [km]')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),
                 fancybox=True, shadow=True, ncol=4)
fig.suptitle('Projection on the $x$-$y$ plane')
fig.tight_layout()



fig = plt.figure()

ax = fig.add_subplot()
ax.grid(True, zorder=0)
ax.plot(halo_dep.state_vec[:, 0] * l_c, halo_dep.state_vec[:, 2] * l_c, label='Dep. Halo', zorder=9)
ax.plot(halo_arr.state_vec[:, 0] * l_c, halo_arr.state_vec[:, 2] * l_c, label='Arr. Halo',
        zorder=9, color='tab:red')
ax.plot(state_vec[:, 0] * l_c, state_vec[:, 2] * l_c, label='Transfer', color='tab:green')
ax.plot(branch_dep.state_vec[:, 0] * l_c, branch_dep.state_vec[:, 2] * l_c,
        label='Dep. branch', color='skyblue')
ax.plot(branch_arr.state_vec[:, 0] * l_c, branch_arr.state_vec[:, 2] * l_c,
        label='Arr. branch', color='lightsalmon')
ax.scatter(cr3bp.l2.position[0]*cr3bp.L, 0, marker='*', color='r', label='$L_2$', zorder=9)
ax.scatter(cr3bp.m2_pos[0]*cr3bp.L, 0, marker='.', color='grey', label='Moon', zorder=9, s=150)
ax.set_xlabel('x [km]')
ax.set_ylabel('z [km]')
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),
                 fancybox=True, shadow=True, ncol=4)
fig.suptitle('Projection on the $x$-$z$ plane')
fig.tight_layout()
