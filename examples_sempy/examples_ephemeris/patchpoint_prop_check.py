# -*- coding: utf-8 -*-
# pylint: disable=invalid-name

"""
This script is intended to check that the PatchPoints propagation and the initial state
propagation both work for the propagation of results of ephemeris propagation.

@author: Paolo GUARDABASSO

"""
# %% Import statements and SPICE kernels loading:
import numpy as np
import spiceypy as sp
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from scipy.interpolate import Akima1DInterpolator

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.init.ephemeris import Ephemeris
from sempy.core.orbits.nrho import NRHO
from sempy.core.init.load_kernels import load_kernels
from sempy.core.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from sempy.core.diffcorr.multiple_shooting import MultipleShooting
from sempy.core.propagation.ephemeris_propagator import EphemerisPropagator
from sempy.core.init.constants import DAYS2SEC
from sempy.core.propagation.patch_points_propagator import PatchPointsPropagator

load_kernels()

# %% Definition of the environment:
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
t_c = cr3bp.T / 2.0 / np.pi  # characteristic time [s]
l_c = cr3bp.L  # characteristic length in kilometers

# %% Halo orbit initialization and sampling:
nrho_gateway = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Rpdim=5930)
nrho_gateway.interpolation()

nb_revs = 10  # Number of revolutions
nb_patch_rev = 50  # Number of points for each revolution
t_patch, state_patch = nrho_gateway.sampling(nb_revs, nb_patch_rev)

# %% Change of coordinates from CR3BP synodic to J2000 inertial frame:
et0 = sp.str2et('2020 JUN 18 12:00:00.000')  # initial epoch in ephemeris seconds

t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,
                                                    adim=True, bary_from_spice=True)

# %% Initialise MultipleShooting object
multiple_shooting = MultipleShooting(ephemeris, epoch_constr=True, t_c=t_c, l_c=l_c,
                                     precision=1e-6, maxiter=20)

# %% Correction
print('\nStart of the multiple shooting procedure\n')
t_patch_j2000_corr, state_patch_j2000_corr, _ = \
    multiple_shooting.correct(t_patch_j2000, state_patch_j2000, t_fix=nb_patch_rev)
print('\nEnd of the multiple shooting procedure')

# %% Propagation of trajectory through patch points
# Here, the pruning of the initial and final revolution are done automatically by the propagator.
# Time steps are referred to the steps of each interval.
patch_points_prop = PatchPointsPropagator(ephemeris, t_c=t_c, l_c=l_c, time_steps=100)
t_vec_j2000_pp, state_vec_j2000_pp = \
    patch_points_prop.propagate(t_patch_j2000_corr, state_patch_j2000_corr,
                                prune_first=nb_patch_rev,
                                prune_last=nb_patch_rev)

# %% Propagation of trajectory through Initial State (IS)
# Here it is necessary to prune the time and state vectors (to delete initial and final revolution)
t_patch_j2000_corr_pruned = t_patch_j2000_corr[nb_patch_rev:-nb_patch_rev]
state_patched_corr_j2000_pruned = state_patch_j2000_corr[nb_patch_rev:-nb_patch_rev]

# Initialise propagator (with same number of steps of the previous one). Attention! This will not
# automatically mean that the two methods have the same time step (PatchPoint prop has in fact a
# variable step size).
prop = EphemerisPropagator(ephemeris, obs=Primary.EARTH, t_c=t_c, l_c=cr3bp.L,
                           time_steps=20000)

# Initial and final propagation epochs (normalised)
et0_adim = t_patch_j2000_corr_pruned[0]
etf_adim = t_patch_j2000_corr_pruned[-1]

t_vec_j2000_is, state_vec_j2000_is, _, _ = prop.propagate([et0_adim, etf_adim],
                                                          state_patched_corr_j2000_pruned[0])


# %% Check initial and final dates for PP and IS (they should be the same)
et0_corr_pp = t_vec_j2000_pp[0] * t_c  # corrected initial epoch in ephemeris seconds
utc0_corr_pp = sp.et2utc(et0_corr_pp, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"Corrected initial epoch PP: {utc0_corr_pp} UTC")

et0_corr_is = t_patch_j2000_corr_pruned[0] * t_c  # corrected initial epoch in ephemeris seconds
utc0_corr_is = sp.et2utc(et0_corr_is, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"Corrected initial epoch IS: {utc0_corr_is} UTC")

etf_corr_pp = t_vec_j2000_pp[-1] * t_c  # corrected initial epoch in ephemeris seconds
utcf_corr_pp = sp.et2utc(etf_corr_pp, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"Corrected final epoch PP: {utcf_corr_pp} UTC")

etf_corr_is = t_patch_j2000_corr_pruned[-1] * t_c  # corrected initial epoch in ephemeris seconds
utcf_corr_is = sp.et2utc(etf_corr_is, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"Corrected final epoch IS: {utcf_corr_is} UTC")

# %% Change of coordinates from J2000 inertial to Earth-Moon barycenter centered rotating frame (
# similar to the synodic frame of the CR3BP one but with moving primaries!).

# For PP
t_vec_rot_pp, state_vec_rot_pp = j2000_to_synodic(t_vec_j2000_pp, state_vec_j2000_pp,
                                                  et0_corr_pp, cr3bp, cr3bp.m1,
                                                  adim=True, bary_from_spice=True)
# For IS
t_vec_rot_is, state_vec_rot_is = j2000_to_synodic(t_vec_j2000_is, state_vec_j2000_is,
                                                  et0_corr_is, cr3bp, cr3bp.m1,
                                                  adim=True, bary_from_spice=True)

# %% Time step comparison
t_step_pp = np.diff(t_vec_rot_pp) * t_c
t_step_is = np.diff(t_vec_rot_is) * t_c

fig2, ax2 = plt.subplots(1, 1)

ax2.plot(t_vec_rot_is[:-1]*t_c/DAYS2SEC, t_step_is, label='IS')
ax2.plot(t_vec_rot_pp[:-1]*t_c/DAYS2SEC, t_step_pp, label='PP')
ax2.legend()
fig2.suptitle('Time step of propagation output vectors')
ax2.set_ylabel('Time step [s]')
ax2.set_xlabel('Integration time [days]')

# %% Interpolate PatchPoint state vector with the IS time vector (so that they can be compared
# and an error can be computed at each time step)
f_pp = Akima1DInterpolator(t_vec_rot_pp, state_vec_rot_pp)  # spline: orbits parameters as
state_vec_rot_pp_interp = f_pp.__call__(t_vec_rot_is)

# Compute the position and velocity relative error between IS propagation
err_pos_vec = (np.linalg.norm(state_vec_rot_is[:, :3]-state_vec_rot_pp_interp[:, :3], axis=1)) / \
           np.linalg.norm(state_vec_rot_pp_interp[:, :3], axis=1)

err_vel_vec = (np.linalg.norm(state_vec_rot_is[:, 3:6]-state_vec_rot_pp_interp[:, 3:6], axis=1)) / \
           np.linalg.norm(state_vec_rot_pp_interp[:, 3:6], axis=1)

err_pos_avg = np.average(err_pos_vec)
err_vel_avg = np.average(err_vel_vec)

fig3, ax = plt.subplots(2, 1, sharex='col')
fig3.suptitle("Relative errors between PatchPoints and Initial States propagation")

ax[0].plot(t_vec_rot_is*t_c/DAYS2SEC, err_pos_vec)
ax[0].set_ylabel(r'Position error')
ax[0].yaxis.set_major_formatter(FuncFormatter('{0:.0e}'.format))
ax[1].plot(t_vec_rot_is * t_c / DAYS2SEC, err_vel_vec)
ax[1].set_ylabel(r'Velocity error')
ax[1].yaxis.set_major_formatter(FuncFormatter('{0:.0e}'.format))
ax[1].set_xlabel('Integration time [days]')
fig3.tight_layout()

print(f'Average relative error between PP and IC for {nb_revs} revolutions ('
      f'{t_vec_rot_is[-1]*t_c/DAYS2SEC:.2f} days)')
print(f'Average relative error between PP and IC in position: {err_pos_avg}')
print(f'Average relative error in position: {err_vel_avg}')

# %% Kernel unloading:
# We will conclude unloading the kernel pool using the SPICE routine kclear.
sp.kclear()
