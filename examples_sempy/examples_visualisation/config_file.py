import bpy # imports
from bpy import context, data, ops

for a in bpy.context.screen.areas: # Initialisation for some parameters of the camera and the light
    if a.type == 'VIEW_3D':
        for s in a.spaces:
            if s.type == 'VIEW_3D':
                s.clip_start = 0.0001
                s.clip_end = 1000000
ob = bpy.data.objects['Camera']
ob.data.clip_start = 0.0001
ob.data.clip_end = 1000000
bpy.ops.object.delete(use_global=False)
bpy.ops.object.select_all(action='DESELECT')
lamp1 = bpy.data.objects['Light']
lamp1.select_set(True)
bpy.context.view_layer.objects.active = lamp1
bpy.context.object.data.type = 'SUN'
bpy.context.object.data.energy = 0.15
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-90*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-0*3.14/180-3.14, orient_axis='Y')
bpy.ops.transform.rotate(value=-30*3.14/180-3.14, orient_axis='Z')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.light_add(type='SUN', radius=1, location=(0, 0, 0))
lamp2 = bpy.data.objects['Sun']
lamp2.select_set(True)
bpy.context.view_layer.objects.active = lamp2
bpy.context.object.data.energy = 7
bpy.ops.object.rotation_clear(clear_delta=False)
bpy.ops.transform.rotate(value=-90*3.14/180, orient_axis='X')
bpy.ops.transform.rotate(value=-0*3.14/180, orient_axis='Y')
bpy.ops.transform.rotate(value=-30*3.14/180, orient_axis='Z')
bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0, 0, 0, 1)
#bpy.context.scene.render.engine = 'CYCLES'
#bpy.context.scene.cycles.device = 'GPU'
bpy.context.scene.cycles.max_bounces = 1
bpy.context.scene.cycles.diffuse_bounces = 1
bpy.context.scene.cycles.glossy_bounces = 1
bpy.context.scene.cycles.transparent_max_bounces = 6
bpy.context.scene.cycles.transmission_bounces = 1
bpy.context.scene.render.tile_x = 512
bpy.context.scene.render.tile_y = 512

bpy.context.scene.sync_mode = 'FRAME_DROP'
bpy.context.scene.frame_end = 10001.0
FRAME_END = 10001.0
bpy.context.scene.frame_start = 0  # set the start and end frame
bpy.context.scene.frame_step = 1
RATIO=6.757172728455109e-05 # set the ratio for the scene

print("importing Earth.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Earth.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Earth'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Earth']
ob.select_set(True)
ob.active_material.use_backface_culling = True
bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Earth = bpy.data.objects['Earth']
Earth.name = 'Earth'

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Earth = bpy.data.objects['Earth']
Earth.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

ob = bpy.data.objects['Earth']
bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (-25639.51586914781, -0.1031332582099157, -517.7410049852292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (-25639.51586914781, -0.13751105423750754, -517.7410007282103)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (-25639.51586914781, -0.1718888499948125, -517.7409951873287)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (-25639.515875904977, -0.2406444401579878, -517.7409805918356)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (-25639.515875904977, -0.3781556130514485, -517.7409368053563)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2) # set the points to keyframes
ob.location = (-25639.515896176497, -0.6531779108624434, -517.7407907828537)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4) # set the points to keyframes
ob.location = (-25639.515916448017, -0.9282001102890035, -517.7405668501494)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6) # set the points to keyframes
ob.location = (-25639.51595023388, -1.2032221690312273, -517.7402650072437)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8) # set the points to keyframes
ob.location = (-25639.51599077692, -1.478244046681222, -517.7398852541363)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10) # set the points to keyframes
ob.location = (-25639.516038077127, -1.7532657006687995, -517.7394276583992)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12) # set the points to keyframes
ob.location = (-25639.51616646341, -2.303308174133122, -517.7382788038918)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16) # set the points to keyframes
ob.location = (-25639.516321878382, -2.8533492569712973, -517.73681837615)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20) # set the points to keyframes
ob.location = (-25639.51651107922, -3.4033886167304273, -517.7350463751737)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24) # set the points to keyframes
ob.location = (-25639.516734065917, -3.9534259209576126, -517.7329628009629)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28) # set the points to keyframes
ob.location = (-25639.51699083848, -4.503460839227108, -517.7305677210893)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32) # set the points to keyframes
ob.location = (-25639.517281396907, -5.053493037734579, -517.727861135553)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36) # set the points to keyframes
ob.location = (-25639.517963871356, -6.153547950381881, -517.7215133123484)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44) # set the points to keyframes
ob.location = (-25639.518781489252, -7.253588004006355, -517.7139193313493)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52) # set the points to keyframes
ob.location = (-25639.519734250607, -8.35361053898481, -517.7050793952707)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60) # set the points to keyframes
ob.location = (-25639.52081539825, -9.453612903802673, -517.6949935716844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68) # set the points to keyframes
ob.location = (-25639.522031689336, -10.553592442891059, -517.6836618605906)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76) # set the points to keyframes
ob.location = (-25639.523383123887, -11.653546493923912, -517.6710843971325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84) # set the points to keyframes
ob.location = (-25639.524869701883, -12.753472414846698, -517.6572612488819)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92) # set the points to keyframes
ob.location = (-25639.526491423338, -13.85336755009053, -517.642192618554)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100) # set the points to keyframes
ob.location = (-25639.528241531076, -14.95322923732936, -517.6258785737206)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108) # set the points to keyframes
ob.location = (-25639.530133539436, -16.053054834508643, -517.6083193170966)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116) # set the points to keyframes
ob.location = (-25639.532153934084, -17.152841686059503, -517.5895149838258)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124) # set the points to keyframes
ob.location = (-25639.534309472187, -18.252587143170235, -517.5694656414797)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132) # set the points to keyframes
ob.location = (-25639.536593396566, -19.352288557029112, -517.5481716279171)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140) # set the points to keyframes
ob.location = (-25639.53901922158, -20.451943272067272, -517.5256330107096)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148) # set the points to keyframes
ob.location = (-25639.541573432867, -21.551548646230163, -517.5018499925724)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156) # set the points to keyframes
ob.location = (-25639.54426278762, -22.651102037463254, -517.4768227762208)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164) # set the points to keyframes
ob.location = (-25639.547087285813, -23.750600790197666, -517.4505516319415)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172) # set the points to keyframes
ob.location = (-25639.55004692747, -24.850042269136033, -517.4230367624499)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180) # set the points to keyframes
ob.location = (-25639.553134955408, -25.94942382546664, -517.3942784380328)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188) # set the points to keyframes
ob.location = (-25639.559723198818, -28.147996602086437, -517.3330322352828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204) # set the points to keyframes
ob.location = (-25639.566014126627, -30.0994558150386, -517.2744986995247)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218) # set the points to keyframes
ob.location = (-25639.572723999147, -32.050686419323014, -517.2120496524573)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233) # set the points to keyframes
ob.location = (-25639.579859573547, -34.001673670788776, -517.1456867833739)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247) # set the points to keyframes
ob.location = (-25639.587414092653, -35.9524028523137, -517.075411916711)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261) # set the points to keyframes
ob.location = (-25639.59539431365, -37.90285925353275, -517.0012269444771)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275) # set the points to keyframes
ob.location = (-25639.60378672218, -39.853028170838066, -516.9231338262522)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289) # set the points to keyframes
ob.location = (-25639.612604832586, -41.802894947922006, -516.8411348594751)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304) # set the points to keyframes
ob.location = (-25639.62184188771, -43.752444914962574, -516.7552321388695)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318) # set the points to keyframes
ob.location = (-25639.631504644713, -45.70166345619515, -516.6654280294454)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332) # set the points to keyframes
ob.location = (-25639.641579589246, -47.650535955855126, -516.5717249637854)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346) # set the points to keyframes
ob.location = (-25639.652080235668, -49.599047818449414, -516.474125509615)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360) # set the points to keyframes
ob.location = (-25639.662993069625, -51.54718449578513, -516.3726323022315)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375) # set the points to keyframes
ob.location = (-25639.674331605464, -53.49493143966939, -516.2672481120758)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389) # set the points to keyframes
ob.location = (-25639.686082328837, -55.44227414245234, -516.1579757095886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403) # set the points to keyframes
ob.location = (-25639.698258754095, -57.389198109998475, -516.0448182030696)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417) # set the points to keyframes
ob.location = (-25639.710854124063, -59.335688895472515, -515.9277785656745)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432) # set the points to keyframes
ob.location = (-25639.723861681563, -61.28173205879632, -515.806859905703)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446) # set the points to keyframes
ob.location = (-25639.737294940947, -63.227313207192005, -515.6820656017413)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460) # set the points to keyframes
ob.location = (-25639.751140387867, -65.17241796815316, -515.5533988972325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474) # set the points to keyframes
ob.location = (-25639.765404779497, -67.11703200295923, -515.4208633734779)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488) # set the points to keyframes
ob.location = (-25639.78008811584, -69.06114099991844, -515.2844625442075)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503) # set the points to keyframes
ob.location = (-25639.795183639708, -71.00473071491062, -515.1442000582945)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517) # set the points to keyframes
ob.location = (-25639.81070486547, -72.94778681597242, -515.0000797673274)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531) # set the points to keyframes
ob.location = (-25639.826638278766, -74.89029524818461, -514.8521054553229)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545) # set the points to keyframes
ob.location = (-25639.842983879593, -76.83224168634096, -514.7002810414411)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559) # set the points to keyframes
ob.location = (-25639.859748425133, -78.77361207552221, -514.5446107151292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574) # set the points to keyframes
ob.location = (-25639.87693191538, -80.71439236080909, -514.3850985982626)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588) # set the points to keyframes
ob.location = (-25639.894527593166, -82.65456835213884, -514.2217489478601)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602) # set the points to keyframes
ob.location = (-25639.912535458487, -84.59412619730743, -514.054566088512)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616) # set the points to keyframes
ob.location = (-25639.930962268518, -86.53305177382379, -513.8835544799526)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630) # set the points to keyframes
ob.location = (-25639.949801266084, -88.47133122948385, -513.7087187170594)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645) # set the points to keyframes
ob.location = (-25639.969052451186, -90.40895064451182, -513.5300633947095)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659) # set the points to keyframes
ob.location = (-25639.988715823827, -92.34589616670355, -513.3475932429242)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673) # set the points to keyframes
ob.location = (-25640.008798141178, -94.282153943855, -513.1613131944393)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687) # set the points to keyframes
ob.location = (-25640.05019258131, -98.15255139479444, -512.7773429356002)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716) # set the points to keyframes
ob.location = (-25640.087221887865, -101.48808301397638, -512.4340113946978)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740) # set the points to keyframes
ob.location = (-25640.125467485504, -104.82137645483546, -512.0794169307198)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765) # set the points to keyframes
ob.location = (-25640.164936131412, -108.15236211849259, -511.7135872480746)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789) # set the points to keyframes
ob.location = (-25640.205627825584, -111.48097114935767, -511.33655099717436)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814) # set the points to keyframes
ob.location = (-25640.247535810846, -114.8071349621275, -510.94833730143364)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838) # set the points to keyframes
ob.location = (-25640.290653330027, -118.13078544450099, -510.54897636541443)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863) # set the points to keyframes
ob.location = (-25640.3349871403, -121.45185488960739, -510.1384989342528)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887) # set the points to keyframes
ob.location = (-25640.380530484486, -124.77027626629324, -509.71693669908876)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912) # set the points to keyframes
ob.location = (-25640.427276605424, -128.08598281369197, -509.284321959208)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936) # set the points to keyframes
ob.location = (-25640.475232260276, -131.39890837908257, -508.8406878247568)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961) # set the points to keyframes
ob.location = (-25640.524390691877, -134.70898735031787, -508.38606821674233)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985) # set the points to keyframes
ob.location = (-25640.57475190022, -138.0161544531093, -507.92049752917376)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010) # set the points to keyframes
ob.location = (-25640.62630237097, -141.3203451564573, -507.4440111696362)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034) # set the points to keyframes
ob.location = (-25640.679048861286, -144.6214954023644, -506.9566450187168)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059) # set the points to keyframes
ob.location = (-25640.732991371173, -147.9195415382635, -506.4584357678636)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083) # set the points to keyframes
ob.location = (-25640.788116386295, -151.2144207224482, -505.94942071666986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108) # set the points to keyframes
ob.location = (-25640.844430663812, -154.50607038349912, -505.4296378404464)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132) # set the points to keyframes
ob.location = (-25640.901927446557, -157.79442869328568, -504.8991257902213)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157) # set the points to keyframes
ob.location = (-25640.96059997736, -161.07943436425134, -504.35792382516814)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181) # set the points to keyframes
ob.location = (-25641.02045501339, -164.36102678455663, -503.8060719477493)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206) # set the points to keyframes
ob.location = (-25641.081479040302, -167.63914574779258, -503.2436106334295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230) # set the points to keyframes
ob.location = (-25641.143672058093, -170.91373179083925, -502.6705810333907)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255) # set the points to keyframes
ob.location = (-25641.207034066767, -174.18472599115032, -502.08702483938856)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279) # set the points to keyframes
ob.location = (-25641.271565066327, -177.4520700343252, -501.4929844188961)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304) # set the points to keyframes
ob.location = (-25641.33725154242, -180.71570628168055, -500.8885026799603)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328) # set the points to keyframes
ob.location = (-25641.472090924217, -187.23162787492683, -499.64838944667116)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377) # set the points to keyframes
ob.location = (-25641.58335453036, -192.4369623019125, -498.625845823188)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416) # set the points to keyframes
ob.location = (-25641.68600274128, -197.11307860967884, -497.6831123155201)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452) # set the points to keyframes
ob.location = (-25641.79102271983, -201.7807998060473, -496.7192583860568)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487) # set the points to keyframes
ob.location = (-25641.89839419448, -206.439967300174, -495.7344237055587)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522) # set the points to keyframes
ob.location = (-25642.0081036509, -211.0904251365124, -494.728749904366)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557) # set the points to keyframes
ob.location = (-25642.120157846257, -215.73202053538705, -493.70238050482754)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593) # set the points to keyframes
ob.location = (-25642.234522994688, -220.36460335241995, -492.65546105644376)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628) # set the points to keyframes
ob.location = (-25642.351205853363, -224.98802641638906, -491.5881386628651)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663) # set the points to keyframes
ob.location = (-25642.47018615076, -229.60214559680009, -490.50056225217844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699) # set the points to keyframes
ob.location = (-25642.591450372547, -234.20681953359951, -489.39288230662044)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734) # set the points to keyframes
ob.location = (-25642.714991761542, -238.8019099074617, -488.26525093014914)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769) # set the points to keyframes
ob.location = (-25642.840796803397, -243.3872814397885, -487.11782171330094)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805) # set the points to keyframes
ob.location = (-25642.968851983776, -247.96280155485104, -485.95074953047435)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840) # set the points to keyframes
ob.location = (-25643.099150545495, -252.528340987935, -484.7641906075032)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875) # set the points to keyframes
ob.location = (-25643.23167221705, -257.0837731096235, -483.5583024540837)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910) # set the points to keyframes
ob.location = (-25643.366410241255, -261.6289745339425, -482.3332437286313)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946) # set the points to keyframes
ob.location = (-25643.503344346598, -266.1638247805021, -481.08917417070967)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981) # set the points to keyframes
ob.location = (-25643.642474533077, -270.6882062069254, -479.8262546010295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016) # set the points to keyframes
ob.location = (-25643.783773772004, -275.2020042791344, -478.54464665116257)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052) # set the points to keyframes
ob.location = (-25643.927242063368, -279.70510736863577, -477.244513033828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087) # set the points to keyframes
ob.location = (-25644.07285913567, -284.1974067525202, -475.92601706989086)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122) # set the points to keyframes
ob.location = (-25644.220618231724, -288.67879681617796, -474.5893228910766)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158) # set the points to keyframes
ob.location = (-25644.370499080014, -293.1491747154402, -473.2345954399716)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193) # set the points to keyframes
ob.location = (-25644.522488166192, -297.6084405792939, -471.8620000645924)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228) # set the points to keyframes
ob.location = (-25644.67657873309, -302.05649737473874, -470.4717027211012)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264) # set the points to keyframes
ob.location = (-25644.832757266362, -306.4932511095021, -469.06386990623423)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299) # set the points to keyframes
ob.location = (-25644.991003494495, -310.9186104266087, -467.6386683870144)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334) # set the points to keyframes
ob.location = (-25645.1513106603, -315.33248707738255, -466.1962654034665)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369) # set the points to keyframes
ob.location = (-25645.31366524945, -319.73479544844537, -464.73682846590265)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405) # set the points to keyframes
ob.location = (-25645.47805374759, -324.12545283200285, -463.26052521977823)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440) # set the points to keyframes
ob.location = (-25645.6444558832, -328.5043792231299, -461.7675235808356)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475) # set the points to keyframes
ob.location = (-25645.812871656282, -332.8714975224857, -460.2579915323886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511) # set the points to keyframes
ob.location = (-25645.983274038153, -337.22673326602677, -458.73209712532315)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546) # set the points to keyframes
ob.location = (-25646.155656271625, -341.57001462500705, -457.19000827538144)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581) # set the points to keyframes
ob.location = (-25646.329998085195, -345.9012726086929, -455.63189310102115)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617) # set the points to keyframes
ob.location = (-25646.50629947885, -350.2204407265048, -454.0579194504126)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652) # set the points to keyframes
ob.location = (-25646.684540181082, -354.5274553258755, -452.4682549014396)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687) # set the points to keyframes
ob.location = (-25646.864699920367, -358.8222551192485, -450.86306709955755)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722) # set the points to keyframes
ob.location = (-25647.046771939535, -363.10478158950787, -449.2425232172198)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758) # set the points to keyframes
ob.location = (-25647.230749481412, -367.3749785845483, -447.60679022416423)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793) # set the points to keyframes
ob.location = (-25647.416605517312, -371.63279251998995, -445.95603461712716)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828) # set the points to keyframes
ob.location = (-25647.604333290055, -375.87817231160705, -444.2904226901292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864) # set the points to keyframes
ob.location = (-25647.79391928529, -380.111069307756, -442.6101201966176)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899) # set the points to keyframes
ob.location = (-25647.98534998869, -384.3314372893753, -440.9152922818937)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934) # set the points to keyframes
ob.location = (-25648.1786118859, -388.53923226727045, -439.206103820972)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970) # set the points to keyframes
ob.location = (-25648.373691462566, -392.7344127524011, -437.48271887800655)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005) # set the points to keyframes
ob.location = (-25648.570575204354, -396.9169394180216, -435.7453011792924)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040) # set the points to keyframes
ob.location = (-25648.76924959692, -401.08677536996925, -433.99401364026414)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075) # set the points to keyframes
ob.location = (-25648.969701125905, -405.2438858763758, -432.229018500639)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111) # set the points to keyframes
ob.location = (-25649.171923034148, -409.3882382325257, -430.45047739198844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146) # set the points to keyframes
ob.location = (-25649.37588829296, -413.5198021662848, -428.6585512025954)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181) # set the points to keyframes
ob.location = (-25649.581596902335, -417.63854936509955, -426.85339994230986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217) # set the points to keyframes
ob.location = (-25649.78902859075, -421.74445361113965, -425.0351829452649)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252) # set the points to keyframes
ob.location = (-25649.99817660104, -425.83749078129847, -423.2040586671609)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287) # set the points to keyframes
ob.location = (-25650.20902066169, -429.9176387796212, -421.36018475283777)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323) # set the points to keyframes
ob.location = (-25650.421554015516, -433.98487740216143, -419.50371783355916)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358) # set the points to keyframes
ob.location = (-25650.63575639101, -438.0391884721245, -417.6348138648718)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393) # set the points to keyframes
ob.location = (-25650.851621030994, -442.08055570472413, -415.7536277211745)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428) # set the points to keyframes
ob.location = (-25651.069134421123, -446.10896457203904, -413.8603132632905)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464) # set the points to keyframes
ob.location = (-25651.50906015161, -454.12685889702914, -410.0379105236632)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534) # set the points to keyframes
ob.location = (-25651.84355371601, -460.1120107890542, -407.1375587347015)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587) # set the points to keyframes
ob.location = (-25652.181642096308, -466.0678533092261, -404.21139927593805)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640) # set the points to keyframes
ob.location = (-25652.523277992284, -471.99437152419307, -401.2599332593023)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693) # set the points to keyframes
ob.location = (-25652.868414103737, -477.891558811926, -398.2836565937009)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746) # set the points to keyframes
ob.location = (-25653.217009887627, -483.7594163211441, -395.2830593093)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799) # set the points to keyframes
ob.location = (-25653.569024800916, -489.59795297131546, -392.25862582781264)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852) # set the points to keyframes
ob.location = (-25653.92442505774, -495.4071850472264, -389.21083482735474)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905) # set the points to keyframes
ob.location = (-25654.283163357893, -501.18713599626614, -386.1401590397305)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958) # set the points to keyframes
ob.location = (-25654.645205915513, -506.93783622571203, -383.047065250432)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011) # set the points to keyframes
ob.location = (-25655.010505430382, -512.6593226297272, -379.93201423106757)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064) # set the points to keyframes
ob.location = (-25655.379028116648, -518.3516385893605, -376.79546073936183)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117) # set the points to keyframes
ob.location = (-25655.75073343127, -524.0148337022598, -373.63785345158396)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170) # set the points to keyframes
ob.location = (-25656.125587588383, -529.6489633096699, -370.45963475983245)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223) # set the points to keyframes
ob.location = (-25656.50354328777, -535.2540885640036, -367.2612410423221)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276) # set the points to keyframes
ob.location = (-25656.884573500756, -540.8302759558409, -364.043102460669)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329) # set the points to keyframes
ob.location = (-25657.268637684298, -546.377597246356, -360.80564295988984)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382) # set the points to keyframes
ob.location = (-25657.655695295358, -551.8961292646029, -357.54928013325934)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436) # set the points to keyframes
ob.location = (-25658.04571254807, -557.3859535020847, -354.27442569531195)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489) # set the points to keyframes
ob.location = (-25658.438655656573, -562.8471560451819, -350.9814847385527)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542) # set the points to keyframes
ob.location = (-25658.83449083501, -568.2798274400092, -347.6708566118898)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595) # set the points to keyframes
ob.location = (-25659.23317754033, -573.6840622869846, -344.3429340422021)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648) # set the points to keyframes
ob.location = (-25659.63468198668, -579.0599591732584, -340.99810387762864)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701) # set the points to keyframes
ob.location = (-25660.0389703882, -584.4076206727126, -337.63674674970883)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754) # set the points to keyframes
ob.location = (-25660.446015716192, -589.7271526026718, -334.25923720852734)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807) # set the points to keyframes
ob.location = (-25660.855784184783, -595.0186645644782, -330.8659436551411)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860) # set the points to keyframes
ob.location = (-25661.268235250962, -600.2822690650572, -327.4572286118671)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913) # set the points to keyframes
ob.location = (-25661.683335128844, -605.5180819223498, -324.03344838442325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966) # set the points to keyframes
ob.location = (-25662.10106354691, -610.7262218598806, -320.59495346735906)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019) # set the points to keyframes
ob.location = (-25662.521379962145, -615.906810236472, -317.1420884764836)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072) # set the points to keyframes
ob.location = (-25662.944257345836, -621.059971046243, -313.675192081294)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125) # set the points to keyframes
ob.location = (-25663.36966191213, -626.1858307158961, -310.1945972076907)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178) # set the points to keyframes
ob.location = (-25663.79756663233, -631.284518104715, -306.7006309704054)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231) # set the points to keyframes
ob.location = (-25664.22793772058, -636.356164099135, -303.1936148081449)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284) # set the points to keyframes
ob.location = (-25664.660748148177, -641.4009015451725, -299.673864618734)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337) # set the points to keyframes
ob.location = (-25665.09597088645, -646.4188653835666, -296.14169069154474)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390) # set the points to keyframes
ob.location = (-25665.53357890669, -651.4101921767783, -292.59739763992343)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443) # set the points to keyframes
ob.location = (-25665.973531665866, -656.3750202441332, -289.0412848066221)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496) # set the points to keyframes
ob.location = (-25666.415815649634, -661.31348918882, -285.4736461962261)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549) # set the points to keyframes
ob.location = (-25666.860397072127, -666.2257403708921, -281.89477027243925)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602) # set the points to keyframes
ob.location = (-25667.30724890466, -671.1119162315507, -278.30494043108575)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655) # set the points to keyframes
ob.location = (-25667.75634411854, -675.9721601580009, -274.7044348649668)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708) # set the points to keyframes
ob.location = (-25668.207655685077, -680.8066179024587, -271.0935266314324)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761) # set the points to keyframes
ob.location = (-25668.661163332745, -685.6154335278464, -267.47248371995295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814) # set the points to keyframes
ob.location = (-25669.116833275686, -690.3987556919643, -263.8415691196911)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867) # set the points to keyframes
ob.location = (-25669.57464524239, -695.1567303497433, -260.2010410897884)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920) # set the points to keyframes
ob.location = (-25670.03457220415, -699.8895061589832, -256.5511528215071)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973) # set the points to keyframes
ob.location = (-25670.496593889457, -704.5972317774844, -252.89215284366014)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026) # set the points to keyframes
ob.location = (-25670.960683269623, -709.2800572144811, -249.22428515775468)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079) # set the points to keyframes
ob.location = (-25671.426813315946, -713.9381318034906, -245.5477888325619)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132) # set the points to keyframes
ob.location = (-25671.89496375692, -718.5716055537478, -241.86289867983413)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185) # set the points to keyframes
ob.location = (-25672.365107563848, -723.1806298259216, -238.16984484887445)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238) # set the points to keyframes
ob.location = (-25672.837231222384, -727.7653546292464, -234.46885309682375)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291) # set the points to keyframes
ob.location = (-25673.31130094667, -732.3259320001091, -230.7601449913758)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344) # set the points to keyframes
ob.location = (-25673.78730322235, -736.8625119477438, -227.04393770806203)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397) # set the points to keyframes
ob.location = (-25674.265211020742, -741.3752465085372, -223.32044416539523)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450) # set the points to keyframes
ob.location = (-25674.7450108275, -745.864287043158, -219.58987336272784)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503) # set the points to keyframes
ob.location = (-25675.226668856758, -750.3297835608412, -215.85243010996518)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556) # set the points to keyframes
ob.location = (-25675.710171594168, -754.7718887736903, -212.10831523028085)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609) # set the points to keyframes
ob.location = (-25676.195498768215, -759.1907526909399, -208.35772562768815)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662) # set the points to keyframes
ob.location = (-25676.682630107385, -763.586526673259, -204.60085435461184)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715) # set the points to keyframes
ob.location = (-25677.171538582978, -767.9593600541651, -200.83789074703188)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768) # set the points to keyframes
ob.location = (-25677.662217437828, -772.3094048700444, -197.06902035691127)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821) # set the points to keyframes
ob.location = (-25678.15463288607, -776.6368097786973, -193.29442501976823)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874) # set the points to keyframes
ob.location = (-25678.648778170525, -780.9417247893577, -189.51428312496282)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927) # set the points to keyframes
ob.location = (-25679.14461950534, -785.2242992355434, -185.72876948055338)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980) # set the points to keyframes
ob.location = (-25679.642156890513, -789.4846810993372, -181.93805538086872)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033) # set the points to keyframes
ob.location = (-25680.141363297345, -793.723019714256, -178.1423087416511)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086) # set the points to keyframes
ob.location = (-25680.642211697148, -797.9394623866657, -174.3416942351999)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139) # set the points to keyframes
ob.location = (-25681.14470208993, -802.1341564229315, -170.53637308765636)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192) # set the points to keyframes
ob.location = (-25681.648800689818, -806.3072477779843, -166.72650348443418)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245) # set the points to keyframes
ob.location = (-25682.154500739645, -810.45888375819, -162.91224036750393)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298) # set the points to keyframes
ob.location = (-25682.661775210716, -814.5892089670446, -159.09373557053684)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351) # set the points to keyframes
ob.location = (-25683.170617345855, -818.6983680080444, -155.27113788647642)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404) # set the points to keyframes
ob.location = (-25683.681006873554, -822.7865061604035, -151.4445931351101)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457) # set the points to keyframes
ob.location = (-25684.19292352229, -826.8537660004661, -147.61424429821287)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510) # set the points to keyframes
ob.location = (-25684.706360534885, -830.9002907802941, -143.7802313168319)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563) # set the points to keyframes
ob.location = (-25685.22129088266, -834.9262217247978, -139.94269149671706)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616) # set the points to keyframes
ob.location = (-25685.737707808436, -838.9317014103212, -136.10175923803385)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669) # set the points to keyframes
ob.location = (-25686.255591040688, -842.9168697103405, -132.25756623807882)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722) # set the points to keyframes
ob.location = (-25686.77492706508, -846.8818658226132, -128.41024169399452)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775) # set the points to keyframes
ob.location = (-25687.29569561009, -850.8268296206155, -124.55991196491098)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828) # set the points to keyframes
ob.location = (-25687.81788991854, -854.7518996263879, -120.70670104494772)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881) # set the points to keyframes
ob.location = (-25688.341496476096, -858.657211659102, -116.85073036049873)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934) # set the points to keyframes
ob.location = (-25688.866488254065, -862.5429035650819, -112.992118837804)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987) # set the points to keyframes
ob.location = (-25689.392865252437, -866.409110487782, -109.13098297052137)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040) # set the points to keyframes
ob.location = (-25689.92060044253, -870.255967570657, -105.26743702244168)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093) # set the points to keyframes
ob.location = (-25690.449693824343, -874.0836086057268, -101.4015928247736)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146) # set the points to keyframes
ob.location = (-25690.980118369178, -877.8921667092942, -97.53355991128703)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199) # set the points to keyframes
ob.location = (-25691.511874077045, -881.6817743219445, -93.6634457210284)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252) # set the points to keyframes
ob.location = (-25692.044933919242, -885.4525625328288, -89.79135532803357)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305) # set the points to keyframes
ob.location = (-25692.579297895783, -889.2046624310981, -85.91739184675842)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358) # set the points to keyframes
ob.location = (-25693.114945735142, -892.9382030787515, -82.04165622936353)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411) # set the points to keyframes
ob.location = (-25693.65186392297, -896.6533128620706, -78.16424726571412)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464) # set the points to keyframes
ob.location = (-25694.19003894493, -900.3501201673375, -74.28526192123893)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517) # set the points to keyframes
ob.location = (-25694.72946404384, -904.0287513536821, -70.40479499907127)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570) # set the points to keyframes
ob.location = (-25695.270125705363, -907.6893334559518, -66.52293944412206)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623) # set the points to keyframes
ob.location = (-25695.812010415153, -911.3319901304073, -62.639786322808156)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676) # set the points to keyframes
ob.location = (-25696.35510465886, -914.9568470604617, -58.75542480953805)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729) # set the points to keyframes
ob.location = (-25696.899401679304, -918.5640258752238, -54.86994226104077)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782) # set the points to keyframes
ob.location = (-25697.444881204978, -922.1536502309544, -50.98342424339458)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835) # set the points to keyframes
ob.location = (-25697.991543235887, -925.7258404053288, -47.09595457257001)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888) # set the points to keyframes
ob.location = (-25698.5393675005, -929.2807180274554, -43.20761534145853)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941) # set the points to keyframes
ob.location = (-25699.088347241654, -932.8184020235743, -39.31848696041561)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994) # set the points to keyframes
ob.location = (-25699.63846894499, -936.3390106442084, -35.42864819104658)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047) # set the points to keyframes
ob.location = (-25700.189719096183, -939.842662815598, -31.53817618674965)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100) # set the points to keyframes
ob.location = (-25700.742097695213, -943.3294740853962, -27.64714649271592)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153) # set the points to keyframes
ob.location = (-25701.295584470572, -946.7995613526917, -23.75563312025828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206) # set the points to keyframes
ob.location = (-25701.85017266509, -950.2530388137031, -19.863708553568603)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259) # set the points to keyframes
ob.location = (-25702.405855521585, -953.6900213403674, -15.971443776746392)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312) # set the points to keyframes
ob.location = (-25702.962612768548, -957.1106217774687, -12.078908321099023)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365) # set the points to keyframes
ob.location = (-25703.520444405975, -960.514951618357, -8.186170278656084)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418) # set the points to keyframes
ob.location = (-25704.079336919516, -963.9031237078171, -4.293296346766707)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471) # set the points to keyframes
ob.location = (-25704.639276794827, -967.2752475120473, -0.40035183614060793)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524) # set the points to keyframes
ob.location = (-25705.200257274748, -970.6314324972454, 3.4925992875952967)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577) # set the points to keyframes
ob.location = (-25705.762278359263, -973.9717881296099, 7.385494380922507)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630) # set the points to keyframes
ob.location = (-25706.325313019694, -977.2964218481872, 11.278272094726537)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683) # set the points to keyframes
ob.location = (-25706.889361256024, -980.6054404163061, 15.170872344903197)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736) # set the points to keyframes
ob.location = (-25707.4544163111, -983.8989492458614, 19.06323630350672)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789) # set the points to keyframes
ob.location = (-25708.02047142773, -987.1770551001824, 22.955306345368065)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842) # set the points to keyframes
ob.location = (-25708.58750633441, -990.4398613640116, 26.84702606160931)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895) # set the points to keyframes
ob.location = (-25709.155521031138, -993.6874707463744, 30.738340185314698)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948) # set the points to keyframes
ob.location = (-25709.724502003566, -996.9199873077309, 34.62919463207372)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10001) # set the points to keyframes
ob.location = (-25710.29444249452, -1000.1375110542375, 38.51953640538066)
ob.keyframe_insert(data_path="location", index =-1)

print("importing Moon.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Moon.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Moon'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Moon']
ob.select_set(True)
ob.active_material.use_backface_culling = True
bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Moon = bpy.data.objects['Moon']
Moon.name = 'Moon'

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Moon = bpy.data.objects['Moon']
Moon.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

ob = bpy.data.objects['Moon']
bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (335.0560990336271, -0.1031332582099157, -517.7410049852292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (335.0560990336271, -0.13751105423750754, -517.7410007282103)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (335.0560990336271, -0.1718888499948125, -517.7409951873287)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (335.0560922764598, -0.2406444401579878, -517.7409805918356)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (335.0560922764598, -0.3781556130514485, -517.7409368053563)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2) # set the points to keyframes
ob.location = (335.0560720049398, -0.6531779108624434, -517.7407907828537)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4) # set the points to keyframes
ob.location = (335.05605173341974, -0.9282001102890035, -517.7405668501494)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6) # set the points to keyframes
ob.location = (335.05601794755785, -1.2032221690312273, -517.7402650072437)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8) # set the points to keyframes
ob.location = (335.05597740451776, -1.478244046681222, -517.7398852541363)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10) # set the points to keyframes
ob.location = (335.0559301043104, -1.7532657006687995, -517.7394276583992)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12) # set the points to keyframes
ob.location = (335.0558017180265, -2.303308174133122, -517.7382788038918)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16) # set the points to keyframes
ob.location = (335.05564630305525, -2.8533492569712973, -517.73681837615)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20) # set the points to keyframes
ob.location = (335.0554571022185, -3.4033886167304273, -517.7350463751737)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24) # set the points to keyframes
ob.location = (335.05523411551985, -3.9534259209576126, -517.7329628009629)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28) # set the points to keyframes
ob.location = (335.0549773429557, -4.503460839227108, -517.7305677210893)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32) # set the points to keyframes
ob.location = (335.0546867845296, -5.053493037734579, -517.727861135553)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36) # set the points to keyframes
ob.location = (335.0540043100809, -6.153547950381881, -517.7215133123484)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44) # set the points to keyframes
ob.location = (335.0531866921847, -7.253588004006355, -517.7139193313493)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52) # set the points to keyframes
ob.location = (335.05223393082997, -8.35361053898481, -517.7050793952707)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60) # set the points to keyframes
ob.location = (335.0511527831877, -9.453612903802673, -517.6949935716844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68) # set the points to keyframes
ob.location = (335.04993649210155, -10.553592442891059, -517.6836618605906)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76) # set the points to keyframes
ob.location = (335.04858505754964, -11.653546493923912, -517.6710843971325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84) # set the points to keyframes
ob.location = (335.0470984795538, -12.753472414846698, -517.6572612488819)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92) # set the points to keyframes
ob.location = (335.04547675809954, -13.85336755009053, -517.642192618554)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100) # set the points to keyframes
ob.location = (335.04372665036135, -14.95322923732936, -517.6258785737206)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108) # set the points to keyframes
ob.location = (335.04183464200105, -16.053054834508643, -517.6083193170966)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116) # set the points to keyframes
ob.location = (335.0398142473532, -17.152841686059503, -517.5895149838258)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124) # set the points to keyframes
ob.location = (335.03765870925054, -18.252587143170235, -517.5694656414797)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132) # set the points to keyframes
ob.location = (335.03537478487124, -19.352288557029112, -517.5481716279171)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140) # set the points to keyframes
ob.location = (335.0329489598589, -20.451943272067272, -517.5256330107096)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148) # set the points to keyframes
ob.location = (335.03039474856996, -21.551548646230163, -517.5018499925724)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156) # set the points to keyframes
ob.location = (335.0277053938189, -22.651102037463254, -517.4768227762208)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164) # set the points to keyframes
ob.location = (335.02488089562394, -23.750600790197666, -517.4505516319415)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172) # set the points to keyframes
ob.location = (335.02192125396687, -24.850042269136033, -517.4230367624499)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180) # set the points to keyframes
ob.location = (335.0188332260295, -25.94942382546664, -517.3942784380328)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188) # set the points to keyframes
ob.location = (335.01224498261945, -28.147996602086437, -517.3330322352828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204) # set the points to keyframes
ob.location = (335.00595405481, -30.0994558150386, -517.2744986995247)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218) # set the points to keyframes
ob.location = (334.99924418229057, -32.050686419323014, -517.2120496524573)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233) # set the points to keyframes
ob.location = (334.99210860789026, -34.001673670788776, -517.1456867833739)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247) # set the points to keyframes
ob.location = (334.9845540887836, -35.9524028523137, -517.075411916711)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261) # set the points to keyframes
ob.location = (334.97657386778883, -37.90285925353275, -517.0012269444771)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275) # set the points to keyframes
ob.location = (334.96818145925863, -39.853028170838066, -516.9231338262522)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289) # set the points to keyframes
ob.location = (334.9593633488512, -41.802894947922006, -516.8411348594751)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304) # set the points to keyframes
ob.location = (334.9501262937265, -43.752444914962574, -516.7552321388695)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318) # set the points to keyframes
ob.location = (334.94046353672456, -45.70166345619515, -516.6654280294454)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332) # set the points to keyframes
ob.location = (334.93038859219087, -47.650535955855126, -516.5717249637854)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346) # set the points to keyframes
ob.location = (334.919887945769, -49.599047818449414, -516.474125509615)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360) # set the points to keyframes
ob.location = (334.90897511181174, -51.54718449578513, -516.3726323022315)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375) # set the points to keyframes
ob.location = (334.8976365759736, -53.49493143966939, -516.2672481120758)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389) # set the points to keyframes
ob.location = (334.88588585260004, -55.44227414245234, -516.1579757095886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403) # set the points to keyframes
ob.location = (334.87370942734196, -57.389198109998475, -516.0448182030696)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417) # set the points to keyframes
ob.location = (334.8611140573739, -59.335688895472515, -515.9277785656745)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432) # set the points to keyframes
ob.location = (334.8481064998741, -61.28173205879632, -515.806859905703)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446) # set the points to keyframes
ob.location = (334.8346732404898, -63.227313207192005, -515.6820656017413)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460) # set the points to keyframes
ob.location = (334.82082779357006, -65.17241796815316, -515.5533988972325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474) # set the points to keyframes
ob.location = (334.80656340194037, -67.11703200295923, -515.4208633734779)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488) # set the points to keyframes
ob.location = (334.79188006559707, -69.06114099991844, -515.2844625442075)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503) # set the points to keyframes
ob.location = (334.7767845417293, -71.00473071491062, -515.1442000582945)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517) # set the points to keyframes
ob.location = (334.76126331596606, -72.94778681597242, -515.0000797673274)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531) # set the points to keyframes
ob.location = (334.7453299026711, -74.89029524818461, -514.8521054553229)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545) # set the points to keyframes
ob.location = (334.7289843018443, -76.83224168634096, -514.7002810414411)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559) # set the points to keyframes
ob.location = (334.71221975630397, -78.77361207552221, -514.5446107151292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574) # set the points to keyframes
ob.location = (334.6950362660573, -80.71439236080909, -514.3850985982626)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588) # set the points to keyframes
ob.location = (334.67744058827157, -82.65456835213884, -514.2217489478601)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602) # set the points to keyframes
ob.location = (334.65943272295044, -84.59412619730743, -514.054566088512)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616) # set the points to keyframes
ob.location = (334.64100591291935, -86.53305177382379, -513.8835544799526)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630) # set the points to keyframes
ob.location = (334.62216691535286, -88.47133122948385, -513.7087187170594)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645) # set the points to keyframes
ob.location = (334.60291573025097, -90.40895064451182, -513.5300633947095)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659) # set the points to keyframes
ob.location = (334.58325235761004, -92.34589616670355, -513.3475932429242)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673) # set the points to keyframes
ob.location = (334.56317004025914, -94.282153943855, -513.1613131944393)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687) # set the points to keyframes
ob.location = (334.5217756001257, -98.15255139479444, -512.7773429356002)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716) # set the points to keyframes
ob.location = (334.48474629357224, -101.48808301397638, -512.4340113946978)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740) # set the points to keyframes
ob.location = (334.4465006959326, -104.82137645483546, -512.0794169307198)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765) # set the points to keyframes
ob.location = (334.407032050025, -108.15236211849259, -511.7135872480746)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789) # set the points to keyframes
ob.location = (334.366340355853, -111.48097114935767, -511.33655099717436)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814) # set the points to keyframes
ob.location = (334.32443237059124, -114.8071349621275, -510.94833730143364)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838) # set the points to keyframes
ob.location = (334.28131485141057, -118.13078544450099, -510.54897636541443)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863) # set the points to keyframes
ob.location = (334.23698104113646, -121.45185488960739, -510.1384989342528)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887) # set the points to keyframes
ob.location = (334.19143769695074, -124.77027626629324, -509.71693669908876)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912) # set the points to keyframes
ob.location = (334.14469157601343, -128.08598281369197, -509.284321959208)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936) # set the points to keyframes
ob.location = (334.09673592116087, -131.39890837908257, -508.8406878247568)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961) # set the points to keyframes
ob.location = (334.04757748956035, -134.70898735031787, -508.38606821674233)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985) # set the points to keyframes
ob.location = (333.9972162812155, -138.0161544531093, -507.92049752917376)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010) # set the points to keyframes
ob.location = (333.9456658104682, -141.3203451564573, -507.4440111696362)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034) # set the points to keyframes
ob.location = (333.8929193201511, -144.6214954023644, -506.9566450187168)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059) # set the points to keyframes
ob.location = (333.8389768102643, -147.9195415382635, -506.4584357678636)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083) # set the points to keyframes
ob.location = (333.78385179514225, -151.2144207224482, -505.94942071666986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108) # set the points to keyframes
ob.location = (333.727537517625, -154.50607038349912, -505.4296378404464)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132) # set the points to keyframes
ob.location = (333.6700407348799, -157.79442869328568, -504.8991257902213)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157) # set the points to keyframes
ob.location = (333.61136820407773, -161.07943436425134, -504.35792382516814)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181) # set the points to keyframes
ob.location = (333.5515131680477, -164.36102678455663, -503.8060719477493)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206) # set the points to keyframes
ob.location = (333.4904891411352, -167.63914574779258, -503.2436106334295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230) # set the points to keyframes
ob.location = (333.4282961233439, -170.91373179083925, -502.6705810333907)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255) # set the points to keyframes
ob.location = (333.3649341146702, -174.18472599115032, -502.08702483938856)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279) # set the points to keyframes
ob.location = (333.3004031151104, -177.4520700343252, -501.4929844188961)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304) # set the points to keyframes
ob.location = (333.2347166390173, -180.71570628168055, -500.8885026799603)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328) # set the points to keyframes
ob.location = (333.0998772572202, -187.23162787492683, -499.64838944667116)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377) # set the points to keyframes
ob.location = (332.98861365107587, -192.4369623019125, -498.625845823188)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416) # set the points to keyframes
ob.location = (332.8859654401567, -197.11307860967884, -497.6831123155201)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452) # set the points to keyframes
ob.location = (332.7809454616072, -201.7807998060473, -496.7192583860568)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487) # set the points to keyframes
ob.location = (332.6735739869582, -206.439967300174, -495.7344237055587)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522) # set the points to keyframes
ob.location = (332.5638645305371, -211.0904251365124, -494.728749904366)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557) # set the points to keyframes
ob.location = (332.4518103351802, -215.73202053538705, -493.70238050482754)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593) # set the points to keyframes
ob.location = (332.3374451867494, -220.36460335241995, -492.65546105644376)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628) # set the points to keyframes
ob.location = (332.22076232807376, -224.98802641638906, -491.5881386628651)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663) # set the points to keyframes
ob.location = (332.101782030677, -229.60214559680009, -490.50056225217844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699) # set the points to keyframes
ob.location = (331.98051780889, -234.20681953359951, -489.39288230662044)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734) # set the points to keyframes
ob.location = (331.85697641989464, -238.8019099074617, -488.26525093014914)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769) # set the points to keyframes
ob.location = (331.73117137804, -243.3872814397885, -487.11782171330094)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805) # set the points to keyframes
ob.location = (331.6031161976607, -247.96280155485104, -485.95074953047435)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840) # set the points to keyframes
ob.location = (331.4728176359422, -252.528340987935, -484.7641906075032)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875) # set the points to keyframes
ob.location = (331.3402959643863, -257.0837731096235, -483.5583024540837)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910) # set the points to keyframes
ob.location = (331.2055579401822, -261.6289745339425, -482.3332437286313)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946) # set the points to keyframes
ob.location = (331.06862383483895, -266.1638247805021, -481.08917417070967)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981) # set the points to keyframes
ob.location = (330.9294936483602, -270.6882062069254, -479.8262546010295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016) # set the points to keyframes
ob.location = (330.78819440943334, -275.2020042791344, -478.54464665116257)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052) # set the points to keyframes
ob.location = (330.64472611806923, -279.70510736863577, -477.244513033828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087) # set the points to keyframes
ob.location = (330.4991090457661, -284.1974067525202, -475.92601706989086)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122) # set the points to keyframes
ob.location = (330.35134994971304, -288.67879681617796, -474.5893228910766)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158) # set the points to keyframes
ob.location = (330.20146910142284, -293.1491747154402, -473.2345954399716)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193) # set the points to keyframes
ob.location = (330.0494800152446, -297.6084405792939, -471.8620000645924)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228) # set the points to keyframes
ob.location = (329.89538944834567, -302.05649737473874, -470.4717027211012)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264) # set the points to keyframes
ob.location = (329.7392109150751, -306.4932511095021, -469.06386990623423)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299) # set the points to keyframes
ob.location = (329.580964686942, -310.9186104266087, -467.6386683870144)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334) # set the points to keyframes
ob.location = (329.4206575211356, -315.33248707738255, -466.1962654034665)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369) # set the points to keyframes
ob.location = (329.2583029319867, -319.73479544844537, -464.73682846590265)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405) # set the points to keyframes
ob.location = (329.0939144338481, -324.12545283200285, -463.26052521977823)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440) # set the points to keyframes
ob.location = (328.92751229823625, -328.5043792231299, -461.7675235808356)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475) # set the points to keyframes
ob.location = (328.75909652515475, -332.8714975224857, -460.2579915323886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511) # set the points to keyframes
ob.location = (328.58869414328365, -337.22673326602677, -458.73209712532315)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546) # set the points to keyframes
ob.location = (328.41631190981207, -341.57001462500705, -457.19000827538144)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581) # set the points to keyframes
ob.location = (328.24197009624186, -345.9012726086929, -455.63189310102115)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617) # set the points to keyframes
ob.location = (328.0656687025876, -350.2204407265048, -454.0579194504126)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652) # set the points to keyframes
ob.location = (327.8874280003547, -354.5274553258755, -452.4682549014396)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687) # set the points to keyframes
ob.location = (327.70726826107057, -358.8222551192485, -450.86306709955755)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722) # set the points to keyframes
ob.location = (327.52519624190245, -363.10478158950787, -449.2425232172198)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758) # set the points to keyframes
ob.location = (327.3412187000249, -367.3749785845483, -447.60679022416423)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793) # set the points to keyframes
ob.location = (327.1553626641253, -371.63279251998995, -445.95603461712716)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828) # set the points to keyframes
ob.location = (326.96763489138175, -375.87817231160705, -444.2904226901292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864) # set the points to keyframes
ob.location = (326.7780488961471, -380.111069307756, -442.6101201966176)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899) # set the points to keyframes
ob.location = (326.5866181927486, -384.3314372893753, -440.9152922818937)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934) # set the points to keyframes
ob.location = (326.39335629553534, -388.53923226727045, -439.206103820972)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970) # set the points to keyframes
ob.location = (326.19827671887106, -392.7344127524011, -437.48271887800655)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005) # set the points to keyframes
ob.location = (326.001392977083, -396.9169394180216, -435.7453011792924)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040) # set the points to keyframes
ob.location = (325.8027185845167, -401.08677536996925, -433.99401364026414)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075) # set the points to keyframes
ob.location = (325.60226705553214, -405.2438858763758, -432.229018500639)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111) # set the points to keyframes
ob.location = (325.40004514728935, -409.3882382325257, -430.45047739198844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146) # set the points to keyframes
ob.location = (325.19607988847565, -413.5198021662848, -428.6585512025954)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181) # set the points to keyframes
ob.location = (324.99037127910196, -417.63854936509955, -426.85339994230986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217) # set the points to keyframes
ob.location = (324.78293959068833, -421.74445361113965, -425.0351829452649)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252) # set the points to keyframes
ob.location = (324.5737915803984, -425.83749078129847, -423.2040586671609)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287) # set the points to keyframes
ob.location = (324.36294751974856, -429.9176387796212, -421.36018475283777)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323) # set the points to keyframes
ob.location = (324.15041416592067, -433.98487740216143, -419.50371783355916)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358) # set the points to keyframes
ob.location = (323.9362117904275, -438.0391884721245, -417.6348138648718)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393) # set the points to keyframes
ob.location = (323.72034715044356, -442.08055570472413, -415.7536277211745)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428) # set the points to keyframes
ob.location = (323.5028337603144, -446.10896457203904, -413.8603132632905)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464) # set the points to keyframes
ob.location = (323.06290802982767, -454.12685889702914, -410.0379105236632)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534) # set the points to keyframes
ob.location = (322.7284144654259, -460.1120107890542, -407.1375587347015)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587) # set the points to keyframes
ob.location = (322.39032608512935, -466.0678533092261, -404.21139927593805)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640) # set the points to keyframes
ob.location = (322.0486901891527, -471.99437152419307, -401.2599332593023)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693) # set the points to keyframes
ob.location = (321.70355407769966, -477.891558811926, -398.2836565937009)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746) # set the points to keyframes
ob.location = (321.35495829381034, -483.7594163211441, -395.2830593093)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799) # set the points to keyframes
ob.location = (321.0029433805212, -489.59795297131546, -392.25862582781264)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852) # set the points to keyframes
ob.location = (320.6475431236977, -495.4071850472264, -389.21083482735474)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905) # set the points to keyframes
ob.location = (320.2888048235436, -501.18713599626614, -386.1401590397305)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958) # set the points to keyframes
ob.location = (319.9267622659245, -506.93783622571203, -383.047065250432)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011) # set the points to keyframes
ob.location = (319.56146275105493, -512.6593226297272, -379.93201423106757)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064) # set the points to keyframes
ob.location = (319.1929400647896, -518.3516385893605, -376.79546073936183)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117) # set the points to keyframes
ob.location = (318.8212347501685, -524.0148337022598, -373.63785345158396)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170) # set the points to keyframes
ob.location = (318.4463805930536, -529.6489633096699, -370.45963475983245)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223) # set the points to keyframes
ob.location = (318.0684248936668, -535.2540885640036, -367.2612410423221)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276) # set the points to keyframes
ob.location = (317.6873946806809, -540.8302759558409, -364.043102460669)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329) # set the points to keyframes
ob.location = (317.30333049713954, -546.377597246356, -360.80564295988984)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382) # set the points to keyframes
ob.location = (316.91627288607924, -551.8961292646029, -357.54928013325934)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436) # set the points to keyframes
ob.location = (316.5262556333655, -557.3859535020847, -354.27442569531195)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489) # set the points to keyframes
ob.location = (316.13331252486387, -562.8471560451819, -350.9814847385527)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542) # set the points to keyframes
ob.location = (315.73747734642893, -568.2798274400092, -347.6708566118898)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595) # set the points to keyframes
ob.location = (315.33879064110806, -573.6840622869846, -344.3429340422021)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648) # set the points to keyframes
ob.location = (314.93728619475587, -579.0599591732584, -340.99810387762864)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701) # set the points to keyframes
ob.location = (314.5329977932379, -584.4076206727126, -337.63674674970883)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754) # set the points to keyframes
ob.location = (314.12595246524506, -589.7271526026718, -334.25923720852734)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807) # set the points to keyframes
ob.location = (313.71618399665385, -595.0186645644782, -330.8659436551411)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860) # set the points to keyframes
ob.location = (313.30373293047523, -600.2822690650572, -327.4572286118671)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913) # set the points to keyframes
ob.location = (312.8886330525929, -605.5180819223498, -324.03344838442325)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966) # set the points to keyframes
ob.location = (312.47090463452696, -610.7262218598806, -320.59495346735906)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019) # set the points to keyframes
ob.location = (312.05058821929197, -615.906810236472, -317.1420884764836)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072) # set the points to keyframes
ob.location = (311.62771083560074, -621.059971046243, -313.675192081294)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125) # set the points to keyframes
ob.location = (311.2023062693079, -626.1858307158961, -310.1945972076907)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178) # set the points to keyframes
ob.location = (310.774401549108, -631.284518104715, -306.7006309704054)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231) # set the points to keyframes
ob.location = (310.3440304608557, -636.356164099135, -303.1936148081449)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284) # set the points to keyframes
ob.location = (309.9112200332602, -641.4009015451725, -299.673864618734)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337) # set the points to keyframes
ob.location = (309.4759972949869, -646.4188653835666, -296.14169069154474)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390) # set the points to keyframes
ob.location = (309.0383892747486, -651.4101921767783, -292.59739763992343)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443) # set the points to keyframes
ob.location = (308.59843651557094, -656.3750202441332, -289.0412848066221)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496) # set the points to keyframes
ob.location = (308.15615253180295, -661.31348918882, -285.4736461962261)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549) # set the points to keyframes
ob.location = (307.71157110931017, -666.2257403708921, -281.89477027243925)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602) # set the points to keyframes
ob.location = (307.2647192767763, -671.1119162315507, -278.30494043108575)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655) # set the points to keyframes
ob.location = (306.81562406289595, -675.9721601580009, -274.7044348649668)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708) # set the points to keyframes
ob.location = (306.36431249636007, -680.8066179024587, -271.0935266314324)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761) # set the points to keyframes
ob.location = (305.91080484869235, -685.6154335278464, -267.47248371995295)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814) # set the points to keyframes
ob.location = (305.45513490575104, -690.3987556919643, -263.8415691196911)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867) # set the points to keyframes
ob.location = (304.9973229390489, -695.1567303497433, -260.2010410897884)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920) # set the points to keyframes
ob.location = (304.5373959772878, -699.8895061589832, -256.5511528215071)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973) # set the points to keyframes
ob.location = (304.07537429198055, -704.5972317774844, -252.89215284366014)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026) # set the points to keyframes
ob.location = (303.61128491181444, -709.2800572144811, -249.22428515775468)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079) # set the points to keyframes
ob.location = (303.14515486549135, -713.9381318034906, -245.5477888325619)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132) # set the points to keyframes
ob.location = (302.6770044245168, -718.5716055537478, -241.86289867983413)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185) # set the points to keyframes
ob.location = (302.206860617589, -723.1806298259216, -238.16984484887445)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238) # set the points to keyframes
ob.location = (301.7347369590534, -727.7653546292464, -234.46885309682375)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291) # set the points to keyframes
ob.location = (301.26066723476833, -732.3259320001091, -230.7601449913758)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344) # set the points to keyframes
ob.location = (300.7846649590865, -736.8625119477438, -227.04393770806203)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397) # set the points to keyframes
ob.location = (300.30675716069527, -741.3752465085372, -223.32044416539523)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450) # set the points to keyframes
ob.location = (299.82695735393645, -745.864287043158, -219.58987336272784)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503) # set the points to keyframes
ob.location = (299.3452993246792, -750.3297835608412, -215.85243010996518)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556) # set the points to keyframes
ob.location = (298.86179658726905, -754.7718887736903, -212.10831523028085)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609) # set the points to keyframes
ob.location = (298.37646941322237, -759.1907526909399, -208.35772562768815)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662) # set the points to keyframes
ob.location = (297.8893380740519, -763.586526673259, -204.60085435461184)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715) # set the points to keyframes
ob.location = (297.4004295984596, -767.9593600541651, -200.83789074703188)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768) # set the points to keyframes
ob.location = (296.90975074360904, -772.3094048700444, -197.06902035691127)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821) # set the points to keyframes
ob.location = (296.4173352953658, -776.6368097786973, -193.29442501976823)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874) # set the points to keyframes
ob.location = (295.92319001091164, -780.9417247893577, -189.51428312496282)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927) # set the points to keyframes
ob.location = (295.4273486760976, -785.2242992355434, -185.72876948055338)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980) # set the points to keyframes
ob.location = (294.9298112909237, -789.4846810993372, -181.93805538086872)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033) # set the points to keyframes
ob.location = (294.43060488409174, -793.723019714256, -178.1423087416511)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086) # set the points to keyframes
ob.location = (293.9297564842891, -797.9394623866657, -174.3416942351999)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139) # set the points to keyframes
ob.location = (293.4272660915085, -802.1341564229315, -170.53637308765636)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192) # set the points to keyframes
ob.location = (292.9231674916191, -806.3072477779843, -166.72650348443418)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245) # set the points to keyframes
ob.location = (292.4174674417918, -810.45888375819, -162.91224036750393)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298) # set the points to keyframes
ob.location = (291.91019297072125, -814.5892089670446, -159.09373557053684)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351) # set the points to keyframes
ob.location = (291.401350835582, -818.6983680080444, -155.27113788647642)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404) # set the points to keyframes
ob.location = (290.8909613078831, -822.7865061604035, -151.4445931351101)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457) # set the points to keyframes
ob.location = (290.37904465914835, -826.8537660004661, -147.61424429821287)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510) # set the points to keyframes
ob.location = (289.86560764655223, -830.9002907802941, -143.7802313168319)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563) # set the points to keyframes
ob.location = (289.35067729877846, -834.9262217247978, -139.94269149671706)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616) # set the points to keyframes
ob.location = (288.8342603730016, -838.9317014103212, -136.10175923803385)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669) # set the points to keyframes
ob.location = (288.31637714074895, -842.9168697103405, -132.25756623807882)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722) # set the points to keyframes
ob.location = (287.79704111635874, -846.8818658226132, -128.41024169399452)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775) # set the points to keyframes
ob.location = (287.27627257134736, -850.8268296206155, -124.55991196491098)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828) # set the points to keyframes
ob.location = (286.75407826289666, -854.7518996263879, -120.70670104494772)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881) # set the points to keyframes
ob.location = (286.2304717053412, -858.657211659102, -116.85073036049873)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934) # set the points to keyframes
ob.location = (285.7054799273719, -862.5429035650819, -112.992118837804)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987) # set the points to keyframes
ob.location = (285.1791029289998, -866.409110487782, -109.13098297052137)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040) # set the points to keyframes
ob.location = (284.65136773890845, -870.255967570657, -105.26743702244168)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093) # set the points to keyframes
ob.location = (284.1222743570943, -874.0836086057268, -101.4015928247736)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146) # set the points to keyframes
ob.location = (283.59184981225917, -877.8921667092942, -97.53355991128703)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199) # set the points to keyframes
ob.location = (283.0600941043922, -881.6817743219445, -93.6634457210284)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252) # set the points to keyframes
ob.location = (282.52703426219523, -885.4525625328288, -89.79135532803357)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305) # set the points to keyframes
ob.location = (281.99267028565373, -889.2046624310981, -85.91739184675842)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358) # set the points to keyframes
ob.location = (281.457022446295, -892.9382030787515, -82.04165622936353)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411) # set the points to keyframes
ob.location = (280.9201042584682, -896.6533128620706, -78.16424726571412)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464) # set the points to keyframes
ob.location = (280.38192923650786, -900.3501201673375, -74.28526192123893)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517) # set the points to keyframes
ob.location = (279.8425041375958, -904.0287513536821, -70.40479499907127)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570) # set the points to keyframes
ob.location = (279.30184247607394, -907.6893334559518, -66.52293944412206)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623) # set the points to keyframes
ob.location = (278.75995776628406, -911.3319901304073, -62.639786322808156)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676) # set the points to keyframes
ob.location = (278.21686352257893, -914.9568470604617, -58.75542480953805)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729) # set the points to keyframes
ob.location = (277.6725665021331, -918.5640258752238, -54.86994226104077)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782) # set the points to keyframes
ob.location = (277.1270869764594, -922.1536502309544, -50.98342424339458)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835) # set the points to keyframes
ob.location = (276.5804249455505, -925.7258404053288, -47.09595457257001)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888) # set the points to keyframes
ob.location = (276.0326006809373, -929.2807180274554, -43.20761534145853)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941) # set the points to keyframes
ob.location = (275.4836209397836, -932.8184020235743, -39.31848696041561)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994) # set the points to keyframes
ob.location = (274.93349923644564, -936.3390106442084, -35.42864819104658)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047) # set the points to keyframes
ob.location = (274.38224908525444, -939.842662815598, -31.53817618674965)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100) # set the points to keyframes
ob.location = (273.82987048622454, -943.3294740853962, -27.64714649271592)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153) # set the points to keyframes
ob.location = (273.27638371086505, -946.7995613526917, -23.75563312025828)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206) # set the points to keyframes
ob.location = (272.7217955163469, -950.2530388137031, -19.863708553568603)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259) # set the points to keyframes
ob.location = (272.16611265985193, -953.6900213403674, -15.971443776746392)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312) # set the points to keyframes
ob.location = (271.6093554128893, -957.1106217774687, -12.078908321099023)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365) # set the points to keyframes
ob.location = (271.05152377546256, -960.514951618357, -8.186170278656084)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418) # set the points to keyframes
ob.location = (270.4926312619209, -963.9031237078171, -4.293296346766707)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471) # set the points to keyframes
ob.location = (269.9326913866098, -967.2752475120473, -0.40035183614060793)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524) # set the points to keyframes
ob.location = (269.37171090668926, -970.6314324972454, 3.4925992875952967)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577) # set the points to keyframes
ob.location = (268.8096898221738, -973.9717881296099, 7.385494380922507)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630) # set the points to keyframes
ob.location = (268.24665516174355, -977.2964218481872, 11.278272094726537)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683) # set the points to keyframes
ob.location = (267.682606925413, -980.6054404163061, 15.170872344903197)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736) # set the points to keyframes
ob.location = (267.1175518703385, -983.8989492458614, 19.06323630350672)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789) # set the points to keyframes
ob.location = (266.5514967537056, -987.1770551001824, 22.955306345368065)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842) # set the points to keyframes
ob.location = (265.984461847027, -990.4398613640116, 26.84702606160931)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895) # set the points to keyframes
ob.location = (265.41644715029906, -993.6874707463744, 30.738340185314698)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948) # set the points to keyframes
ob.location = (264.84746617787096, -996.9199873077309, 34.62919463207372)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10001) # set the points to keyframes
ob.location = (264.2775256869172, -1000.1375110542375, 38.51953640538066)
ob.keyframe_insert(data_path="location", index =-1)

print("importing Orion.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Orion.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Orion'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Orion']
ob.select_set(True)
ob.active_material.use_backface_culling = True
bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Orion = bpy.data.objects['Orion']
Orion.name = 'Orion'

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Orion = bpy.data.objects['Orion']
Orion.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.context.scene.frame_set(0)  # Calculating the ratio to rescale objects to see 
bpy.ops.object.select_all(action='DESELECT')  # them all in a camera
Orion = bpy.data.objects['Orion']
Orion.select_set(True)
bpy.ops.transform.resize(value=(26.0, 26.0, 26.0))
Orion.keyframe_insert(data_path="scale", index =-1)
bpy.context.scene.frame_set(5867.0)
bpy.ops.transform.resize(value=(0.038461538461538464, 0.038461538461538464, 0.038461538461538464))
Orion.keyframe_insert(data_path="scale", index =-1)

print("importing Plume.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Plume.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Plume'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Plume']
ob.select_set(True)
ob.active_material.use_backface_culling = True
bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Plume = bpy.data.objects['Plume']
Plume.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.ops.object.select_all(action='DESELECT')  # Move the constraint and rotate it
bpy.data.objects['Plume'].location.x = 0*RATIO
bpy.data.objects['Plume'].location.y = 0*RATIO
bpy.data.objects['Plume'].location.z = -3.7*RATIO
Plume = bpy.data.objects['Plume']
Plume.select_set(True)
bpy.ops.transform.rotate(value=0.0, orient_axis='X')
bpy.ops.transform.rotate(value=3.141592653589793, orient_axis='Y')
bpy.ops.transform.rotate(value=0.0, orient_axis='Z')

bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
Orion = bpy.data.objects['Orion']  # exactly the movement of the parent)
Orion.select_set(True)
Plume = bpy.data.objects['Plume']
Plume.select_set(True)
bpy.context.view_layer.objects.active = Orion
bpy.ops.object.parent_set(type='OBJECT')

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Plume = bpy.data.objects['Plume']
Plume.name = 'OrionPlume'

ops.curve.primitive_bezier_circle_add(enter_editmode=True)  # draw the trajectory
curve = context.active_object
bez_points = curve.data.splines[0].bezier_points
ops.curve.delete()
bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
ops.curve.vertex_add(location=(379441475.0, 1526.278258, 7688095.166))
ops.curve.vertex_add(location=(379441475.0, 2035.038318, 7687895.103))
ops.curve.vertex_add(location=(379441475.0, 2543.798374, 7687695.021000001))
ops.curve.vertex_add(location=(379441475.09999996, 3561.3184659999997, 7687494.805000001))
ops.curve.vertex_add(location=(379441475.09999996, 5596.35854, 7687294.157000001))
ops.curve.vertex_add(location=(379441475.4, 9666.437978, 7687091.996))
ops.curve.vertex_add(location=(379441475.7, 13736.51596, 7686888.682))
ops.curve.vertex_add(location=(379441476.2, 17806.59186, 7686684.215))
ops.curve.vertex_add(location=(379441476.8, 21876.66508, 7686478.595))
ops.curve.vertex_add(location=(379441477.5, 25946.73499, 7686271.823))
ops.curve.vertex_add(location=(379441479.40000004, 34086.86246, 7686054.8209999995))
ops.curve.vertex_add(location=(379441481.7, 42226.96935, 7685833.208))
ops.curve.vertex_add(location=(379441484.5, 50367.050740000006, 7685606.984))
ops.curve.vertex_add(location=(379441487.8, 58507.10171, 7685376.148999999))
ops.curve.vertex_add(location=(379441491.6, 66647.11737, 7685140.704))
ops.curve.vertex_add(location=(379441495.9, 74787.09277999999, 7684900.649))
ops.curve.vertex_add(location=(379441506.0, 91066.90324, 7684606.7069999995))
ops.curve.vertex_add(location=(379441518.09999996, 107346.49380000001, 7684294.323))
ops.curve.vertex_add(location=(379441532.2, 123625.8251, 7683963.5))
ops.curve.vertex_add(location=(379441548.20000005, 139904.8579, 7683614.239))
ops.curve.vertex_add(location=(379441566.2, 156183.5529, 7683246.54))
ops.curve.vertex_add(location=(379441586.20000005, 172461.8707, 7682860.405))
ops.curve.vertex_add(location=(379441608.2, 188739.7722, 7682455.835))
ops.curve.vertex_add(location=(379441632.2, 205017.2181, 7682032.833000001))
ops.curve.vertex_add(location=(379441658.1, 221294.16900000002, 7681591.4))
ops.curve.vertex_add(location=(379441686.09999996, 237570.5858, 7681131.539))
ops.curve.vertex_add(location=(379441716.0, 253846.42919999998, 7680653.252))
ops.curve.vertex_add(location=(379441747.90000004, 270121.66000000003, 7680156.54))
ops.curve.vertex_add(location=(379441781.7, 286396.239, 7679641.408000001))
ops.curve.vertex_add(location=(379441817.6, 302670.12690000003, 7679107.857))
ops.curve.vertex_add(location=(379441855.4, 318943.2846, 7678555.89))
ops.curve.vertex_add(location=(379441895.20000005, 335215.673, 7677985.510000001))
ops.curve.vertex_add(location=(379441937.0, 351487.2528, 7677396.721))
ops.curve.vertex_add(location=(379441980.8, 367757.98500000004, 7676789.526))
ops.curve.vertex_add(location=(379442026.5, 384027.83040000004, 7676163.9290000005))
ops.curve.vertex_add(location=(379442124.0, 416564.7044, 7675057.541))
ops.curve.vertex_add(location=(379442217.1, 445444.52280000004, 7673991.2979999995))
ops.curve.vertex_add(location=(379442316.40000004, 474320.95800000004, 7672867.109))
ops.curve.vertex_add(location=(379442422.0, 503193.7918, 7671684.999))
ops.curve.vertex_add(location=(379442533.79999995, 532062.8064, 7670444.995))
ops.curve.vertex_add(location=(379442651.9, 560927.7841, 7669147.125))
ops.curve.vertex_add(location=(379442776.1, 589788.5072999999, 7667791.418))
ops.curve.vertex_add(location=(379442906.59999996, 618644.7591, 7666377.908000001))
ops.curve.vertex_add(location=(379443043.3, 647496.3224000001, 7664906.626))
ops.curve.vertex_add(location=(379443186.3, 676342.9809000001, 7663377.607000001))
ops.curve.vertex_add(location=(379443335.4, 705184.5183, 7661790.887))
ops.curve.vertex_add(location=(379443490.8, 734020.7186, 7660146.504000001))
ops.curve.vertex_add(location=(379443652.3, 762851.3665, 7658444.497))
ops.curve.vertex_add(location=(379443820.1, 791676.2467, 7656684.907))
ops.curve.vertex_add(location=(379443994.0, 820495.1445, 7654867.774999999))
ops.curve.vertex_add(location=(379444174.2, 849307.8454, 7652993.147))
ops.curve.vertex_add(location=(379444360.6, 878114.1356, 7651061.067))
ops.curve.vertex_add(location=(379444553.1, 906913.8014, 7649071.581))
ops.curve.vertex_add(location=(379444751.9, 935706.6298, 7647024.739))
ops.curve.vertex_add(location=(379444956.8, 964492.4081, 7644920.589000001))
ops.curve.vertex_add(location=(379445167.9, 993270.9241, 7642759.183999999))
ops.curve.vertex_add(location=(379445385.20000005, 1022041.966, 7640540.576))
ops.curve.vertex_add(location=(379445608.59999996, 1050805.323, 7638264.819))
ops.curve.vertex_add(location=(379445838.3, 1079560.7829999998, 7635931.97))
ops.curve.vertex_add(location=(379446074.1, 1108308.138, 7633542.085))
ops.curve.vertex_add(location=(379446316.0, 1137047.176, 7631095.222))
ops.curve.vertex_add(location=(379446564.1, 1165777.689, 7628591.443))
ops.curve.vertex_add(location=(379446818.4, 1194499.469, 7626030.808999999))
ops.curve.vertex_add(location=(379447078.8, 1223212.3059999999, 7623413.383))
ops.curve.vertex_add(location=(379447345.3, 1251915.995, 7620739.228999999))
ops.curve.vertex_add(location=(379447618.0, 1280610.327, 7618008.413))
ops.curve.vertex_add(location=(379447896.8, 1309295.0969999998, 7615221.003))
ops.curve.vertex_add(location=(379448181.7, 1337970.099, 7612377.067000001))
ops.curve.vertex_add(location=(379448472.7, 1366635.1279999998, 7609476.675))
ops.curve.vertex_add(location=(379448769.90000004, 1395289.9789999998, 7606519.9))
ops.curve.vertex_add(location=(379449382.5, 1452568.335, 7600637.49))
ops.curve.vertex_add(location=(379449930.5, 1501931.164, 7595356.496))
ops.curve.vertex_add(location=(379450496.5, 1551260.87, 7589908.8209999995))
ops.curve.vertex_add(location=(379451080.59999996, 1600556.423, 7584294.875))
ops.curve.vertex_add(location=(379451682.8, 1649816.804, 7578515.0819999995))
ops.curve.vertex_add(location=(379452303.0, 1699040.998, 7572569.873000001))
ops.curve.vertex_add(location=(379452941.1, 1748227.997, 7566459.694999999))
ops.curve.vertex_add(location=(379453597.20000005, 1797376.7989999999, 7560185.003))
ops.curve.vertex_add(location=(379454271.2, 1846486.412, 7553746.266))
ops.curve.vertex_add(location=(379454963.0, 1895555.848, 7547143.962))
ops.curve.vertex_add(location=(379455672.7, 1944584.128, 7540378.581))
ops.curve.vertex_add(location=(379456400.2, 1993570.281, 7533550.625))
ops.curve.vertex_add(location=(379457145.5, 2042513.341, 7526560.603))
ops.curve.vertex_add(location=(379457908.40000004, 2091412.3530000001, 7519409.039))
ops.curve.vertex_add(location=(379458689.0, 2140266.369, 7512096.464000001))
ops.curve.vertex_add(location=(379459487.29999995, 2189074.447, 7504623.421))
ops.curve.vertex_add(location=(379460303.1, 2237835.657, 7496990.461999999))
ops.curve.vertex_add(location=(379461136.5, 2286549.0730000003, 7489198.149))
ops.curve.vertex_add(location=(379461987.4, 2335213.7800000003, 7481247.0540000005))
ops.curve.vertex_add(location=(379462855.7, 2383828.8710000003, 7473137.757999999))
ops.curve.vertex_add(location=(379463741.5, 2432393.449, 7464870.853))
ops.curve.vertex_add(location=(379464644.6, 2480906.6229999997, 7456446.938))
ops.curve.vertex_add(location=(379465565.0, 2529367.5130000003, 7447866.6219999995))
ops.curve.vertex_add(location=(379466502.7, 2577775.247, 7439130.522000001))
ops.curve.vertex_add(location=(379467457.70000005, 2626128.962, 7430239.265000001))
ops.curve.vertex_add(location=(379468429.8, 2674427.8049999997, 7421193.4860000005))
ops.curve.vertex_add(location=(379470425.3, 2770857.508, 7402740.9399999995))
ops.curve.vertex_add(location=(379472071.9, 2847891.7150000003, 7387508.226))
ops.curve.vertex_add(location=(379473591.0, 2917093.9760000003, 7373456.629000001))
ops.curve.vertex_add(location=(379475145.20000005, 2986171.997, 7359092.4690000005))
ops.curve.vertex_add(location=(379476734.2, 3055123.431, 7344417.813))
ops.curve.vertex_add(location=(379478357.8, 3123945.97, 7329434.757))
ops.curve.vertex_add(location=(379480016.1, 3192637.353, 7314145.425))
ops.curve.vertex_add(location=(379481708.6, 3261195.358, 7298551.971))
ops.curve.vertex_add(location=(379483435.40000004, 3329617.807, 7282656.572))
ops.curve.vertex_add(location=(379485196.2, 3397902.5670000003, 7266461.432))
ops.curve.vertex_add(location=(379486990.8, 3466047.5459999996, 7249968.778))
ops.curve.vertex_add(location=(379488819.1, 3534050.697, 7233180.861))
ops.curve.vertex_add(location=(379490680.9, 3601910.018, 7216099.954))
ops.curve.vertex_add(location=(379492576.0, 3669623.5470000003, 7198728.349))
ops.curve.vertex_add(location=(379494504.29999995, 3737189.371, 7181068.358))
ops.curve.vertex_add(location=(379496465.5, 3804605.616, 7163122.312000001))
ops.curve.vertex_add(location=(379498459.5, 3871870.4560000002, 7144892.559))
ops.curve.vertex_add(location=(379500486.0, 3938982.108, 7126381.4629999995))
ops.curve.vertex_add(location=(379502545.0, 4005938.831, 7107591.404))
ops.curve.vertex_add(location=(379504636.1, 4072738.93, 7088524.774))
ops.curve.vertex_add(location=(379506759.29999995, 4139380.7530000005, 7069183.981))
ops.curve.vertex_add(location=(379508914.3, 4205862.691000001, 7049571.442000001))
ops.curve.vertex_add(location=(379511101.0, 4272183.181, 7029689.586))
ops.curve.vertex_add(location=(379513319.1, 4338340.701, 7009540.854))
ops.curve.vertex_add(location=(379515568.4, 4404333.773, 6989127.693))
ops.curve.vertex_add(location=(379517848.79999995, 4470160.960999999, 6968452.559))
ops.curve.vertex_add(location=(379520160.09999996, 4535820.874, 6947517.916))
ops.curve.vertex_add(location=(379522502.0, 4601312.16, 6926326.232000001))
ops.curve.vertex_add(location=(379524874.4, 4666633.512999999, 6904879.982000001))
ops.curve.vertex_add(location=(379527277.1, 4731783.666, 6883181.645))
ops.curve.vertex_add(location=(379529709.90000004, 4796761.3950000005, 6861233.7020000005))
ops.curve.vertex_add(location=(379532172.5, 4861565.516, 6839038.638))
ops.curve.vertex_add(location=(379534664.9, 4926194.888, 6816598.939))
ops.curve.vertex_add(location=(379537186.70000005, 4990648.409, 6793917.091999999))
ops.curve.vertex_add(location=(379539737.8, 5054925.016, 6770995.5819999995))
ops.curve.vertex_add(location=(379542317.90000004, 5119023.688, 6747836.897))
ops.curve.vertex_add(location=(379544927.0, 5182943.441000001, 6724443.521))
ops.curve.vertex_add(location=(379547564.8, 5246683.333, 6700817.933999999))
ops.curve.vertex_add(location=(379550231.0, 5310242.457, 6676962.617000001))
ops.curve.vertex_add(location=(379552925.5, 5373619.947, 6652880.044000001))
ops.curve.vertex_add(location=(379555648.2, 5436814.972, 6628572.686000001))
ops.curve.vertex_add(location=(379558398.70000005, 5499826.739, 6604043.007))
ops.curve.vertex_add(location=(379561176.90000004, 5562654.492, 6579293.467999999))
ops.curve.vertex_add(location=(379563982.59999996, 5625297.511, 6554326.522))
ops.curve.vertex_add(location=(379566815.59999996, 5687755.112, 6529144.613000001))
ops.curve.vertex_add(location=(379569675.70000005, 5750026.643999999, 6503800.181))
ops.curve.vertex_add(location=(379572562.7, 5812111.493000001, 6478245.654))
ops.curve.vertex_add(location=(379575476.4, 5874009.077, 6452483.454999999))
ops.curve.vertex_add(location=(379578416.6, 5935718.850000001, 6426515.995))
ops.curve.vertex_add(location=(379581383.09999996, 5997240.2979999995, 6400345.675))
ops.curve.vertex_add(location=(379584375.79999995, 6058572.937, 6373974.887))
ops.curve.vertex_add(location=(379587394.3, 6119716.319, 6347406.012))
ops.curve.vertex_add(location=(379590438.6, 6180670.025, 6320641.4180000005))
ops.curve.vertex_add(location=(379593508.4, 6241433.667, 6293683.463))
ops.curve.vertex_add(location=(379596603.59999996, 6302006.888, 6266534.492))
ops.curve.vertex_add(location=(379599723.9, 6362389.361, 6239196.8379999995))
ops.curve.vertex_add(location=(379602869.2, 6422580.787, 6211672.819))
ops.curve.vertex_add(location=(379606039.2, 6482580.897, 6183964.743000001))
ops.curve.vertex_add(location=(379609233.8, 6542389.449999999, 6156074.902000001))
ops.curve.vertex_add(location=(379612452.8, 6602006.231000001, 6128005.573000001))
ops.curve.vertex_add(location=(379618963.3, 6720663.762, 6071387.495))
ops.curve.vertex_add(location=(379623913.5, 6809238.557, 6028414.931))
ops.curve.vertex_add(location=(379628916.90000004, 6897379.600000001, 5985060.437))
ops.curve.vertex_add(location=(379633972.8, 6985086.67, 5941331.429))
ops.curve.vertex_add(location=(379639080.5, 7072359.669000001, 5897235.246))
ops.curve.vertex_add(location=(379644239.40000004, 7159198.614, 5852779.14))
ops.curve.vertex_add(location=(379649448.90000004, 7245603.637, 5807970.279999999))
ops.curve.vertex_add(location=(379654708.5, 7331574.979, 5762815.75))
ops.curve.vertex_add(location=(379660017.5, 7417112.987000001, 5717322.546))
ops.curve.vertex_add(location=(379665375.40000004, 7502218.1110000005, 5671497.575999999))
ops.curve.vertex_add(location=(379670781.5, 7586890.897, 5625347.659))
ops.curve.vertex_add(location=(379676235.3, 7671131.987, 5578879.525))
ops.curve.vertex_add(location=(379681736.2, 7754942.115, 5532099.814))
ops.curve.vertex_add(location=(379687283.70000005, 7838322.100000001, 5485015.073))
ops.curve.vertex_add(location=(379692877.09999996, 7921272.847, 5437631.76))
ops.curve.vertex_add(location=(379698516.0, 8003795.34, 5389956.240999999))
ops.curve.vertex_add(location=(379704199.8, 8085890.641, 5341994.79))
ops.curve.vertex_add(location=(379709927.90000004, 8167559.887, 5293753.587))
ops.curve.vertex_add(location=(379715699.8, 8248804.284, 5245238.725000001))
ops.curve.vertex_add(location=(379721515.0, 8329625.106, 5196456.199))
ops.curve.vertex_add(location=(379727373.0, 8410023.693, 5147411.919))
ops.curve.vertex_add(location=(379733273.2, 8490001.445, 5098111.697000001))
ops.curve.vertex_add(location=(379739215.09999996, 8569559.821, 5048561.257999999))
ops.curve.vertex_add(location=(379745198.2, 8648700.339000002, 4998766.235))
ops.curve.vertex_add(location=(379751222.1, 8727424.565, 4948732.171))
ops.curve.vertex_add(location=(379757286.29999995, 8805734.121, 4898464.518))
ops.curve.vertex_add(location=(379763390.20000005, 8883630.671999998, 4847968.641000001))
ops.curve.vertex_add(location=(379769533.3, 8961115.932, 4797249.813))
ops.curve.vertex_add(location=(379775715.29999995, 9038191.658, 4746313.221))
ops.curve.vertex_add(location=(379781935.6, 9114859.646000002, 4695163.965))
ops.curve.vertex_add(location=(379788193.8, 9191121.730999999, 4643807.057))
ops.curve.vertex_add(location=(379794489.40000004, 9266979.784, 4592247.424000001))
ops.curve.vertex_add(location=(379800822.0, 9342435.712000001, 4540489.907000001))
ops.curve.vertex_add(location=(379807191.1, 9417491.452, 4488539.262999999))
ops.curve.vertex_add(location=(379813596.29999995, 9492148.97, 4436400.166999999))
ops.curve.vertex_add(location=(379820037.20000005, 9566410.263, 4384077.211))
ops.curve.vertex_add(location=(379826513.4, 9640277.352, 4331574.903))
ops.curve.vertex_add(location=(379833024.3, 9713752.284, 4278897.673))
ops.curve.vertex_add(location=(379839569.7, 9786837.125, 4226049.8719999995))
ops.curve.vertex_add(location=(379846149.09999996, 9859533.967, 4173035.769))
ops.curve.vertex_add(location=(379852762.09999996, 9931844.918000001, 4119859.558))
ops.curve.vertex_add(location=(379859408.3, 10003772.1, 4066525.356))
ops.curve.vertex_add(location=(379866087.3, 10075317.67, 4013037.2040000004))
ops.curve.vertex_add(location=(379872798.8, 10146483.76, 3959399.068))
ops.curve.vertex_add(location=(379879542.29999995, 10217272.569999998, 3905614.8400000003))
ops.curve.vertex_add(location=(379886317.5, 10287686.26, 3851708.3419999997))
ops.curve.vertex_add(location=(379893124.0, 10357727.03, 3797663.321))
ops.curve.vertex_add(location=(379899961.5, 10427397.08, 3743483.455))
ops.curve.vertex_add(location=(379906829.6, 10496698.63, 3689172.355))
ops.curve.vertex_add(location=(379913727.9, 10565633.889999999, 3634733.559))
ops.curve.vertex_add(location=(379920656.1, 10634205.08, 3580170.542))
ops.curve.vertex_add(location=(379927613.8, 10702414.440000001, 3525486.71))
ops.curve.vertex_add(location=(379934600.8, 10770264.19, 3470685.4039999996))
ops.curve.vertex_add(location=(379941616.6, 10837756.58, 3415769.903))
ops.curve.vertex_add(location=(379948661.0, 10904893.83, 3360743.421))
ops.curve.vertex_add(location=(379955733.59999996, 10971678.190000001, 3305609.109))
ops.curve.vertex_add(location=(379962834.2, 11038111.9, 3250370.06))
ops.curve.vertex_add(location=(379969962.3, 11104197.18, 3195029.305))
ops.curve.vertex_add(location=(379977117.7, 11169936.29, 3139589.816))
ops.curve.vertex_add(location=(379984300.09999996, 11235331.45, 3084054.5069999998))
ops.curve.vertex_add(location=(379991509.2, 11300384.899999999, 3028426.235))
ops.curve.vertex_add(location=(379998744.59999996, 11365098.85, 2972707.8019999997))
ops.curve.vertex_add(location=(380006006.2, 11429475.549999999, 2916901.954))
ops.curve.vertex_add(location=(380013293.5, 11493517.200000001, 2861011.382))
ops.curve.vertex_add(location=(380020606.4, 11557226.02, 2805038.7260000003))
ops.curve.vertex_add(location=(380027944.4, 11620604.219999999, 2748986.5730000003))
ops.curve.vertex_add(location=(380035307.5, 11683653.99, 2692857.458))
ops.curve.vertex_add(location=(380042695.3, 11746377.54, 2636653.866))
ops.curve.vertex_add(location=(380050107.4, 11808777.05, 2580378.2339999997))
ops.curve.vertex_add(location=(380057543.8, 11870854.7, 2524032.948))
ops.curve.vertex_add(location=(380065004.0, 11932612.649999999, 2467620.349))
ops.curve.vertex_add(location=(380072487.90000004, 11994053.08, 2411142.73))
ops.curve.vertex_add(location=(380079995.1, 12055178.13, 2354602.338))
ops.curve.vertex_add(location=(380087525.5, 12115989.94, 2298001.375))
ops.curve.vertex_add(location=(380095078.8, 12176490.66, 2241341.9990000003))
ops.curve.vertex_add(location=(380102654.7, 12236682.4, 2184646.326))
ops.curve.vertex_add(location=(380110253.09999996, 12296567.28, 2127896.427))
ops.curve.vertex_add(location=(380117873.59999996, 12356147.39, 2071094.3339999998))
ops.curve.vertex_add(location=(380125516.1, 12415424.84, 2014242.036))
ops.curve.vertex_add(location=(380133180.3, 12474401.700000001, 1957341.482))
ops.curve.vertex_add(location=(380140866.0, 12533080.03, 1900394.5839999998))
ops.curve.vertex_add(location=(380148572.90000004, 12591461.9, 1843403.2119999998))
ops.curve.vertex_add(location=(380156300.9, 12649549.360000001, 1786369.201))
ops.curve.vertex_add(location=(380164049.79999995, 12707344.42, 1729294.348))
ops.curve.vertex_add(location=(380171819.20000005, 12764849.120000001, 1672180.413))
ops.curve.vertex_add(location=(380179609.1, 12822065.459999999, 1615039.1199999999))
ops.curve.vertex_add(location=(380187419.1, 12878995.440000001, 1557862.16))
ops.curve.vertex_add(location=(380195249.20000005, 12935641.040000001, 1500651.188))
ops.curve.vertex_add(location=(380203099.0, 12992004.23, 1443407.825))
ops.curve.vertex_add(location=(380210968.5, 13048086.97, 1386133.661))
ops.curve.vertex_add(location=(380218857.29999995, 13103891.2, 1328830.2510000002))
ops.curve.vertex_add(location=(380226765.4, 13159418.86, 1271499.121))
ops.curve.vertex_add(location=(380234692.5, 13214671.860000001, 1214141.7650000001))
ops.curve.vertex_add(location=(380242638.4, 13269652.1, 1156759.645))
ops.curve.vertex_add(location=(380250602.9, 13324361.48, 1099354.196))
ops.curve.vertex_add(location=(380258585.9, 13378801.87, 1041926.821))
ops.curve.vertex_add(location=(380266587.2, 13432975.15, 984478.8955))
ops.curve.vertex_add(location=(380274606.6, 13486883.15, 927011.7672))
ops.curve.vertex_add(location=(380282643.90000004, 13540527.73, 869526.7558))
ops.curve.vertex_add(location=(380290699.0, 13593910.690000001, 812025.1541))
ops.curve.vertex_add(location=(380298771.59999996, 13647033.86, 754508.2284))
ops.curve.vertex_add(location=(380306861.7, 13699899.020000001, 696977.2191))
ops.curve.vertex_add(location=(380314969.0, 13752507.97, 639433.3411))
ops.curve.vertex_add(location=(380323093.40000004, 13804862.47, 581877.7844))
ops.curve.vertex_add(location=(380331234.7, 13856964.27, 524311.7146))
ops.curve.vertex_add(location=(380339392.70000005, 13908815.13, 466736.2735))
ops.curve.vertex_add(location=(380347567.4, 13960416.76, 409152.57910000003))
ops.curve.vertex_add(location=(380355758.5, 14011770.89, 351561.7267))
ops.curve.vertex_add(location=(380363965.90000004, 14062879.209999999, 293964.789))
ops.curve.vertex_add(location=(380372189.5, 14113743.42, 236362.81650000002))
ops.curve.vertex_add(location=(380380429.0, 14164365.190000001, 178756.8382))
ops.curve.vertex_add(location=(380388684.40000004, 14214746.17, 121147.8618))
ops.curve.vertex_add(location=(380396955.5, 14264888.03, 63536.87436))
ops.curve.vertex_add(location=(380405242.09999996, 14314792.39, 5924.842419))
ops.curve.vertex_add(location=(380413544.1, 14364460.870000001, -51687.287390000005))
ops.curve.vertex_add(location=(380421861.5, 14413895.09, -109298.58799999999))
ops.curve.vertex_add(location=(380430193.90000004, 14463096.64, -166908.1515))
ops.curve.vertex_add(location=(380438541.3, 14512067.1, -224515.0887))
ops.curve.vertex_add(location=(380446903.6, 14560808.03, -282118.52900000004))
ops.curve.vertex_add(location=(380455280.7, 14609321.01, -339717.6196))
ops.curve.vertex_add(location=(380463672.29999995, 14657607.57, -397311.5257))
ops.curve.vertex_add(location=(380472078.4, 14705669.23, -454899.42939999996))
ops.curve.vertex_add(location=(380480498.8, 14753507.530000001, -512480.53030000004))
ops.curve.vertex_add(location=(380488933.4, 14801123.95, -570054.0441))
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False
curve.name='Orion_trajectory'
ops.object.mode_set(mode='OBJECT')
bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_depth = 130.0/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth", index =-1)
#Orion = bpy.data.objects['Orion_trajectory']
#Orion.select_set(True)
bpy.ops.transform.resize(value=(RATIO, RATIO, RATIO))
bpy.context.scene.frame_set(5867)
bpy.context.object.data.bevel_depth = 5/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth",index=-1) #give some thickness to the trajectory

bpy.context.object.data.bevel_factor_mapping_end = 'SEGMENTS'
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_factor_end = 0 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_factor_end = 3.115281384729673e-05 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_factor_end = 6.230602372838087e-05 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1.0)
bpy.context.object.data.bevel_factor_end = 0.00012140143075645377 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1.0)
bpy.context.object.data.bevel_factor_end = 0.00023793086839636222 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2.0)
bpy.context.object.data.bevel_factor_end = 0.0004701510084540781 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4.0)
bpy.context.object.data.bevel_factor_end = 0.000702374334414315 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6.0)
bpy.context.object.data.bevel_factor_end = 0.000934600830313895 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8.0)
bpy.context.object.data.bevel_factor_end = 0.0011668304803156748 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(10.0)
bpy.context.object.data.bevel_factor_end = 0.00139906326436014 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(12.0)
bpy.context.object.data.bevel_factor_end = 0.0018630947084988295 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(16.0)
bpy.context.object.data.bevel_factor_end = 0.002327132062747834 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(20.0)
bpy.context.object.data.bevel_factor_end = 0.0027911751986038247 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(24.0)
bpy.context.object.data.bevel_factor_end = 0.003255223986276316 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(28.0)
bpy.context.object.data.bevel_factor_end = 0.003719278296043974 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(32.0)
bpy.context.object.data.bevel_factor_end = 0.004183337996928731 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(36.0)
bpy.context.object.data.bevel_factor_end = 0.0051111973208502375 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(44.0)
bpy.context.object.data.bevel_factor_end = 0.00603906376060381 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(52.0)
bpy.context.object.data.bevel_factor_end = 0.00696693627464137 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(60.0)
bpy.context.object.data.bevel_factor_end = 0.007894813824625866 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(68.0)
bpy.context.object.data.bevel_factor_end = 0.008822695380569259 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(76.0)
bpy.context.object.data.bevel_factor_end = 0.009750579899319538 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(84.0)
bpy.context.object.data.bevel_factor_end = 0.01067846635600972 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(92.0)
bpy.context.object.data.bevel_factor_end = 0.011606353711402657 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(100.0)
bpy.context.object.data.bevel_factor_end = 0.012534240914183318 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(108.0)
bpy.context.object.data.bevel_factor_end = 0.01346212695416242 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(116.0)
bpy.context.object.data.bevel_factor_end = 0.01439001077334076 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(124.0)
bpy.context.object.data.bevel_factor_end = 0.015317891350340458 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(132.0)
bpy.context.object.data.bevel_factor_end = 0.016245767634992008 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(140.0)
bpy.context.object.data.bevel_factor_end = 0.017173638613726224 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(148.0)
bpy.context.object.data.bevel_factor_end = 0.018101503231788454 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(156.0)
bpy.context.object.data.bevel_factor_end = 0.01902936047176237 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(164.0)
bpy.context.object.data.bevel_factor_end = 0.019957209289946123 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(172.0)
bpy.context.object.data.bevel_factor_end = 0.02088504866137866 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(180.0)
bpy.context.object.data.bevel_factor_end = 0.021812877531244734 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(188.0)
bpy.context.object.data.bevel_factor_end = 0.023668077123177835 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(204.0)
bpy.context.object.data.bevel_factor_end = 0.02531492858409996 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(218.0)
bpy.context.object.data.bevel_factor_end = 0.026961713710045104 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(233.0)
bpy.context.object.data.bevel_factor_end = 0.028608426756986133 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(247.0)
bpy.context.object.data.bevel_factor_end = 0.03025506195369785 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(261.0)
bpy.context.object.data.bevel_factor_end = 0.03190161356981806 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(275.0)
bpy.context.object.data.bevel_factor_end = 0.03354807580289446 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(289.0)
bpy.context.object.data.bevel_factor_end = 0.03519444296656722 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(304.0)
bpy.context.object.data.bevel_factor_end = 0.03684070929139897 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(318.0)
bpy.context.object.data.bevel_factor_end = 0.03848686908848522 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(332.0)
bpy.context.object.data.bevel_factor_end = 0.040132916574402586 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(346.0)
bpy.context.object.data.bevel_factor_end = 0.041778846084643106 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(360.0)
bpy.context.object.data.bevel_factor_end = 0.04342465186363495 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(375.0)
bpy.context.object.data.bevel_factor_end = 0.04507032826640079 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(389.0)
bpy.context.object.data.bevel_factor_end = 0.04671586954416103 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(403.0)
bpy.context.object.data.bevel_factor_end = 0.04836127006810407 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(417.0)
bpy.context.object.data.bevel_factor_end = 0.05000652414461019 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(432.0)
bpy.context.object.data.bevel_factor_end = 0.05165162606584508 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(446.0)
bpy.context.object.data.bevel_factor_end = 0.05329657025279567 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(460.0)
bpy.context.object.data.bevel_factor_end = 0.05494135098733249 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(474.0)
bpy.context.object.data.bevel_factor_end = 0.056585962667013004 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(488.0)
bpy.context.object.data.bevel_factor_end = 0.05823039966667877 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(503.0)
bpy.context.object.data.bevel_factor_end = 0.05987465635482082 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(517.0)
bpy.context.object.data.bevel_factor_end = 0.06151872713413623 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(531.0)
bpy.context.object.data.bevel_factor_end = 0.06316260645460278 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(545.0)
bpy.context.object.data.bevel_factor_end = 0.06480628860685145 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(559.0)
bpy.context.object.data.bevel_factor_end = 0.06644976812661554 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(574.0)
bpy.context.object.data.bevel_factor_end = 0.06809303949567203 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(588.0)
bpy.context.object.data.bevel_factor_end = 0.06973609700859147 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(602.0)
bpy.context.object.data.bevel_factor_end = 0.07137893527508611 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(616.0)
bpy.context.object.data.bevel_factor_end = 0.07302154870849804 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(630.0)
bpy.context.object.data.bevel_factor_end = 0.0746639318185211 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(645.0)
bpy.context.object.data.bevel_factor_end = 0.0763060790973431 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(659.0)
bpy.context.object.data.bevel_factor_end = 0.07794798506913139 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(673.0)
bpy.context.object.data.bevel_factor_end = 0.07958964428555168 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(687.0)
bpy.context.object.data.bevel_factor_end = 0.08287101511223763 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(716.0)
bpy.context.object.data.bevel_factor_end = 0.08570018906451321 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(740.0)
bpy.context.object.data.bevel_factor_end = 0.08852852461474538 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(765.0)
bpy.context.object.data.bevel_factor_end = 0.09135599416365066 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(789.0)
bpy.context.object.data.bevel_factor_end = 0.09418257043947188 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(814.0)
bpy.context.object.data.bevel_factor_end = 0.09700822613288694 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(838.0)
bpy.context.object.data.bevel_factor_end = 0.09983293406588993 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(863.0)
bpy.context.object.data.bevel_factor_end = 0.1026566673341568 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(887.0)
bpy.context.object.data.bevel_factor_end = 0.1054793991916221 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(912.0)
bpy.context.object.data.bevel_factor_end = 0.10830110288365596 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(936.0)
bpy.context.object.data.bevel_factor_end = 0.11112175206520937 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(961.0)
bpy.context.object.data.bevel_factor_end = 0.11394052817008281 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(985.0)
bpy.context.object.data.bevel_factor_end = 0.1167581782742407 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1010.0)
bpy.context.object.data.bevel_factor_end = 0.11957467637811514 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1034.0)
bpy.context.object.data.bevel_factor_end = 0.12238999689616423 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1059.0)
bpy.context.object.data.bevel_factor_end = 0.12520411421087105 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1083.0)
bpy.context.object.data.bevel_factor_end = 0.1280170029277572 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1108.0)
bpy.context.object.data.bevel_factor_end = 0.13082863787585408 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1132.0)
bpy.context.object.data.bevel_factor_end = 0.13363899402958332 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1157.0)
bpy.context.object.data.bevel_factor_end = 0.13644804652793477 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1181.0)
bpy.context.object.data.bevel_factor_end = 0.13925577096783773 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1206.0)
bpy.context.object.data.bevel_factor_end = 0.1420621427060763 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1230.0)
bpy.context.object.data.bevel_factor_end = 0.14486713761507647 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1255.0)
bpy.context.object.data.bevel_factor_end = 0.14767073172822673 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1279.0)
bpy.context.object.data.bevel_factor_end = 0.1504729012712339 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1304.0)
bpy.context.object.data.bevel_factor_end = 0.1532736225079486 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1328.0)
bpy.context.object.data.bevel_factor_end = 0.15886954579758195 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1377.0)
bpy.context.object.data.bevel_factor_end = 0.163345337576093 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1416.0)
bpy.context.object.data.bevel_factor_end = 0.16737024568693182 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1452.0)
bpy.context.object.data.bevel_factor_end = 0.17139184843607436 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1487.0)
bpy.context.object.data.bevel_factor_end = 0.17541007984360688 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1522.0)
bpy.context.object.data.bevel_factor_end = 0.17942487485251188 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1557.0)
bpy.context.object.data.bevel_factor_end = 0.1834361700441156 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1593.0)
bpy.context.object.data.bevel_factor_end = 0.18744390194600602 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1628.0)
bpy.context.object.data.bevel_factor_end = 0.19144800898715575 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1663.0)
bpy.context.object.data.bevel_factor_end = 0.19544843012333002 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1699.0)
bpy.context.object.data.bevel_factor_end = 0.19944510532981016 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1734.0)
bpy.context.object.data.bevel_factor_end = 0.20343797580311576 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1769.0)
bpy.context.object.data.bevel_factor_end = 0.20742698367753282 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1805.0)
bpy.context.object.data.bevel_factor_end = 0.21141207191674566 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1840.0)
bpy.context.object.data.bevel_factor_end = 0.2153931849388971 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1875.0)
bpy.context.object.data.bevel_factor_end = 0.2193702675943467 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1910.0)
bpy.context.object.data.bevel_factor_end = 0.22334326630535611 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1946.0)
bpy.context.object.data.bevel_factor_end = 0.22731212815143045 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(1981.0)
bpy.context.object.data.bevel_factor_end = 0.23127680162234454 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2016.0)
bpy.context.object.data.bevel_factor_end = 0.23523723572080085 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2052.0)
bpy.context.object.data.bevel_factor_end = 0.23919338107379795 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2087.0)
bpy.context.object.data.bevel_factor_end = 0.2431451888374494 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2122.0)
bpy.context.object.data.bevel_factor_end = 0.24709261167039293 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2158.0)
bpy.context.object.data.bevel_factor_end = 0.25103560274209125 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2193.0)
bpy.context.object.data.bevel_factor_end = 0.25497411652136626 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2228.0)
bpy.context.object.data.bevel_factor_end = 0.2589081086224647 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2264.0)
bpy.context.object.data.bevel_factor_end = 0.26283753560649165 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2299.0)
bpy.context.object.data.bevel_factor_end = 0.2667623547073798 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2334.0)
bpy.context.object.data.bevel_factor_end = 0.2706825247332305 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2369.0)
bpy.context.object.data.bevel_factor_end = 0.2745980051584154 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2405.0)
bpy.context.object.data.bevel_factor_end = 0.27850875656362944 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2440.0)
bpy.context.object.data.bevel_factor_end = 0.2824147402297167 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2475.0)
bpy.context.object.data.bevel_factor_end = 0.28631591916998156 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2511.0)
bpy.context.object.data.bevel_factor_end = 0.2902122564741585 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2546.0)
bpy.context.object.data.bevel_factor_end = 0.29410371681099806 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2581.0)
bpy.context.object.data.bevel_factor_end = 0.2979902654476062 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2617.0)
bpy.context.object.data.bevel_factor_end = 0.30187186918399306 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2652.0)
bpy.context.object.data.bevel_factor_end = 0.3057484953292808 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2687.0)
bpy.context.object.data.bevel_factor_end = 0.30962011186794486 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2722.0)
bpy.context.object.data.bevel_factor_end = 0.31348668839207905 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2758.0)
bpy.context.object.data.bevel_factor_end = 0.31734819526733166 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2793.0)
bpy.context.object.data.bevel_factor_end = 0.3212046031812345 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2828.0)
bpy.context.object.data.bevel_factor_end = 0.32505588438408306 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2864.0)
bpy.context.object.data.bevel_factor_end = 0.3289020118258381 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2899.0)
bpy.context.object.data.bevel_factor_end = 0.33274295941092197 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2934.0)
bpy.context.object.data.bevel_factor_end = 0.3365776277087167 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(2970.0)
bpy.context.object.data.bevel_factor_end = 0.3404070560516673 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3005.0)
bpy.context.object.data.bevel_factor_end = 0.3442312208559534 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3040.0)
bpy.context.object.data.bevel_factor_end = 0.34805009957503713 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3075.0)
bpy.context.object.data.bevel_factor_end = 0.3518636704520444 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3111.0)
bpy.context.object.data.bevel_factor_end = 0.35567191265384696 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3146.0)
bpy.context.object.data.bevel_factor_end = 0.3594748056145865 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3181.0)
bpy.context.object.data.bevel_factor_end = 0.3632723305128939 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3217.0)
bpy.context.object.data.bevel_factor_end = 0.36706446851623536 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3252.0)
bpy.context.object.data.bevel_factor_end = 0.37085120215253814 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3287.0)
bpy.context.object.data.bevel_factor_end = 0.37463251419081867 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3323.0)
bpy.context.object.data.bevel_factor_end = 0.37840838867712756 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3358.0)
bpy.context.object.data.bevel_factor_end = 0.3821788098562997 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3393.0)
bpy.context.object.data.bevel_factor_end = 0.3859437632998437 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3428.0)
bpy.context.object.data.bevel_factor_end = 0.3897032349745174 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3464.0)
bpy.context.object.data.bevel_factor_end = 0.3972044423813241 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3534.0)
bpy.context.object.data.bevel_factor_end = 0.40282164366126444 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3587.0)
bpy.context.object.data.bevel_factor_end = 0.40842635840719327 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3640.0)
bpy.context.object.data.bevel_factor_end = 0.41401855269781496 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3693.0)
bpy.context.object.data.bevel_factor_end = 0.41959819586103925 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3746.0)
bpy.context.object.data.bevel_factor_end = 0.42516526069494837 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3799.0)
bpy.context.object.data.bevel_factor_end = 0.43071972315160695 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3852.0)
bpy.context.object.data.bevel_factor_end = 0.4362615624676258 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3905.0)
bpy.context.object.data.bevel_factor_end = 0.44179076024737457 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(3958.0)
bpy.context.object.data.bevel_factor_end = 0.44730730162253496 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4011.0)
bpy.context.object.data.bevel_factor_end = 0.4528111737588727 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4064.0)
bpy.context.object.data.bevel_factor_end = 0.45830236719678685 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4117.0)
bpy.context.object.data.bevel_factor_end = 0.463780874807318 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4170.0)
bpy.context.object.data.bevel_factor_end = 0.46924669225205184 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4223.0)
bpy.context.object.data.bevel_factor_end = 0.4746998170168461 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4276.0)
bpy.context.object.data.bevel_factor_end = 0.48014024988311377 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4329.0)
bpy.context.object.data.bevel_factor_end = 0.4855679933008141 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4382.0)
bpy.context.object.data.bevel_factor_end = 0.4909830520376252 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4436.0)
bpy.context.object.data.bevel_factor_end = 0.49638543310935174 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4489.0)
bpy.context.object.data.bevel_factor_end = 0.5017751457933795 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4542.0)
bpy.context.object.data.bevel_factor_end = 0.507152201230702 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4595.0)
bpy.context.object.data.bevel_factor_end = 0.5125166122378113 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4648.0)
bpy.context.object.data.bevel_factor_end = 0.5178683937201064 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4701.0)
bpy.context.object.data.bevel_factor_end = 0.5232075625467005 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4754.0)
bpy.context.object.data.bevel_factor_end = 0.5285341374051626 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4807.0)
bpy.context.object.data.bevel_factor_end = 0.5338481385663698 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4860.0)
bpy.context.object.data.bevel_factor_end = 0.5391495872009773 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4913.0)
bpy.context.object.data.bevel_factor_end = 0.5444385066378117 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(4966.0)
bpy.context.object.data.bevel_factor_end = 0.5497149223601605 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5019.0)
bpy.context.object.data.bevel_factor_end = 0.55497886002773 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5072.0)
bpy.context.object.data.bevel_factor_end = 0.5602303474777512 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5125.0)
bpy.context.object.data.bevel_factor_end = 0.5654694134163555 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5178.0)
bpy.context.object.data.bevel_factor_end = 0.5706960882954014 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5231.0)
bpy.context.object.data.bevel_factor_end = 0.575910403257009 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5284.0)
bpy.context.object.data.bevel_factor_end = 0.5811123908792012 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5337.0)
bpy.context.object.data.bevel_factor_end = 0.5863020849794612 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5390.0)
bpy.context.object.data.bevel_factor_end = 0.5914795203898345 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5443.0)
bpy.context.object.data.bevel_factor_end = 0.5966447321017205 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5496.0)
bpy.context.object.data.bevel_factor_end = 0.601797757489468 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5549.0)
bpy.context.object.data.bevel_factor_end = 0.6069386339395706 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5602.0)
bpy.context.object.data.bevel_factor_end = 0.6120673998580224 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5655.0)
bpy.context.object.data.bevel_factor_end = 0.617184094300107 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5708.0)
bpy.context.object.data.bevel_factor_end = 0.6222887579563091 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5761.0)
bpy.context.object.data.bevel_factor_end = 0.6273814308743675 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5814.0)
bpy.context.object.data.bevel_factor_end = 0.6324621554334012 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5867.0)
bpy.context.object.data.bevel_factor_end = 0.6375302822020793 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5920.0)
bpy.context.object.data.bevel_factor_end = 0.6425865417272899 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(5973.0)
bpy.context.object.data.bevel_factor_end = 0.647630977660713 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6026.0)
bpy.context.object.data.bevel_factor_end = 0.6526636342039983 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6079.0)
bpy.context.object.data.bevel_factor_end = 0.657684555463789 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6132.0)
bpy.context.object.data.bevel_factor_end = 0.6626937865023181 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6185.0)
bpy.context.object.data.bevel_factor_end = 0.6676913731496201 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6238.0)
bpy.context.object.data.bevel_factor_end = 0.6726773614924041 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6291.0)
bpy.context.object.data.bevel_factor_end = 0.6776517977665691 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6344.0)
bpy.context.object.data.bevel_factor_end = 0.6826147285944538 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6397.0)
bpy.context.object.data.bevel_factor_end = 0.6875662013234375 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6450.0)
bpy.context.object.data.bevel_factor_end = 0.6925062639751071 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6503.0)
bpy.context.object.data.bevel_factor_end = 0.6974349626815259 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6556.0)
bpy.context.object.data.bevel_factor_end = 0.7023523470382704 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6609.0)
bpy.context.object.data.bevel_factor_end = 0.7072584647622587 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6662.0)
bpy.context.object.data.bevel_factor_end = 0.7121533647750753 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6715.0)
bpy.context.object.data.bevel_factor_end = 0.7170370945284466 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6768.0)
bpy.context.object.data.bevel_factor_end = 0.7219097050248809 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6821.0)
bpy.context.object.data.bevel_factor_end = 0.7267712435773434 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6874.0)
bpy.context.object.data.bevel_factor_end = 0.7316217605810421 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6927.0)
bpy.context.object.data.bevel_factor_end = 0.7364613044453091 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(6980.0)
bpy.context.object.data.bevel_factor_end = 0.7412899255857522 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7033.0)
bpy.context.object.data.bevel_factor_end = 0.7461076736597652 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7086.0)
bpy.context.object.data.bevel_factor_end = 0.7509145973656969 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7139.0)
bpy.context.object.data.bevel_factor_end = 0.7557107478799634 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7192.0)
bpy.context.object.data.bevel_factor_end = 0.7604961733110905 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7245.0)
bpy.context.object.data.bevel_factor_end = 0.7652709250523476 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7298.0)
bpy.context.object.data.bevel_factor_end = 0.770035051688377 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7351.0)
bpy.context.object.data.bevel_factor_end = 0.7747886037214917 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7404.0)
bpy.context.object.data.bevel_factor_end = 0.779531631423015 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7457.0)
bpy.context.object.data.bevel_factor_end = 0.7842634054789158 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7510.0)
bpy.context.object.data.bevel_factor_end = 0.7889847525509273 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7563.0)
bpy.context.object.data.bevel_factor_end = 0.7936957210130368 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7616.0)
bpy.context.object.data.bevel_factor_end = 0.7983963620931028 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7669.0)
bpy.context.object.data.bevel_factor_end = 0.8030867247002333 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7722.0)
bpy.context.object.data.bevel_factor_end = 0.8077668581493652 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7775.0)
bpy.context.object.data.bevel_factor_end = 0.8124368121372931 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7828.0)
bpy.context.object.data.bevel_factor_end = 0.817096636874952 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7881.0)
bpy.context.object.data.bevel_factor_end = 0.8217463808275485 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7934.0)
bpy.context.object.data.bevel_factor_end = 0.8263860929781565 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(7987.0)
bpy.context.object.data.bevel_factor_end = 0.8310154223995202 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8040.0)
bpy.context.object.data.bevel_factor_end = 0.8356348168411571 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8093.0)
bpy.context.object.data.bevel_factor_end = 0.8402443261477743 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8146.0)
bpy.context.object.data.bevel_factor_end = 0.8448439979329833 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8199.0)
bpy.context.object.data.bevel_factor_end = 0.8494338818939058 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8252.0)
bpy.context.object.data.bevel_factor_end = 0.8540140251663014 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8305.0)
bpy.context.object.data.bevel_factor_end = 0.8585844773509504 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8358.0)
bpy.context.object.data.bevel_factor_end = 0.8631452855617529 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8411.0)
bpy.context.object.data.bevel_factor_end = 0.8676964974779353 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8464.0)
bpy.context.object.data.bevel_factor_end = 0.8722381609918096 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8517.0)
bpy.context.object.data.bevel_factor_end = 0.8767703238307 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8570.0)
bpy.context.object.data.bevel_factor_end = 0.8812930338177503 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8623.0)
bpy.context.object.data.bevel_factor_end = 0.8858063371338792 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8676.0)
bpy.context.object.data.bevel_factor_end = 0.8903102814563225 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8729.0)
bpy.context.object.data.bevel_factor_end = 0.8948049130246551 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8782.0)
bpy.context.object.data.bevel_factor_end = 0.8992902783816138 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8835.0)
bpy.context.object.data.bevel_factor_end = 0.9037664241947149 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8888.0)
bpy.context.object.data.bevel_factor_end = 0.9082333964442169 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8941.0)
bpy.context.object.data.bevel_factor_end = 0.9126912410430529 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(8994.0)
bpy.context.object.data.bevel_factor_end = 0.917140003222614 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9047.0)
bpy.context.object.data.bevel_factor_end = 0.9215797288687327 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9100.0)
bpy.context.object.data.bevel_factor_end = 0.9260104634577759 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9153.0)
bpy.context.object.data.bevel_factor_end = 0.930432251693924 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9206.0)
bpy.context.object.data.bevel_factor_end = 0.9348451382412456 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9259.0)
bpy.context.object.data.bevel_factor_end = 0.9392491684156133 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9312.0)
bpy.context.object.data.bevel_factor_end = 0.9436443854472151 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9365.0)
bpy.context.object.data.bevel_factor_end = 0.9480308339037503 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9418.0)
bpy.context.object.data.bevel_factor_end = 0.9524085581287124 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9471.0)
bpy.context.object.data.bevel_factor_end = 0.9567776008535298 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9524.0)
bpy.context.object.data.bevel_factor_end = 0.9611380056684219 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9577.0)
bpy.context.object.data.bevel_factor_end = 0.9654898170258654 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9630.0)
bpy.context.object.data.bevel_factor_end = 0.9698330760222063 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9683.0)
bpy.context.object.data.bevel_factor_end = 0.9741678260980067 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9736.0)
bpy.context.object.data.bevel_factor_end = 0.9784941095776235 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9789.0)
bpy.context.object.data.bevel_factor_end = 0.9828119697307863 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9842.0)
bpy.context.object.data.bevel_factor_end = 0.987121446969053 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9895.0)
bpy.context.object.data.bevel_factor_end = 0.9914225834314245 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(9948.0)
bpy.context.object.data.bevel_factor_end = 0.9957154209499519 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
bpy.context.scene.frame_set(10001.0)
bpy.context.object.data.bevel_factor_end = 1 
bpy.context.object.data.keyframe_insert(data_path="bevel_factor_end", index=-1)
traj = bpy.data.objects['Orion_trajectory']
bpy.context.scene.frame_set(0)
traj.location = ((-379441475.0+traj.location.x)*RATIO, (-1526.278258+traj.location.y)*RATIO, (-7662095.166+traj.location.z)*RATIO) 
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
traj.location = (traj.location.x-6.757167284376919e-06, traj.location.y-0.1375111819480721, traj.location.z--2.439339357351855e-05)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
traj.location = (traj.location.x-0.0, traj.location.y-0.1375111728934607, traj.location.z--4.378647929570434e-05)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2.0)
traj.location = (traj.location.x-2.0271520043024793e-05, traj.location.y-0.27502229781099485, traj.location.z--0.00014602250269035721)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4.0)
traj.location = (traj.location.x-2.0271520043024793e-05, traj.location.y-0.27502219942656014, traj.location.z--0.00022393270421616762)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6.0)
traj.location = (traj.location.x-3.3785861887736246e-05, traj.location.y-0.27502205874222374, traj.location.z--0.00030184290574197803)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8.0)
traj.location = (traj.location.x-4.0543040086049587e-05, traj.location.y-0.27502187764999464, traj.location.z--0.0003797531073814753)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10.0)
traj.location = (traj.location.x-4.7300207370426506e-05, traj.location.y-0.2750216539875776, traj.location.z--0.00045759573708892276)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12.0)
traj.location = (traj.location.x-0.00012838628390454687, traj.location.y-0.5500424734643226, traj.location.z--0.0011488545073916612)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16.0)
traj.location = (traj.location.x-0.00015541497123194858, traj.location.y-0.5500410828381752, traj.location.z--0.0014604277417902267)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20.0)
traj.location = (traj.location.x-0.00018920083675766364, traj.location.y-0.55003935975913, traj.location.z--0.001772000976302479)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24.0)
traj.location = (traj.location.x-0.00022298669864539988, traj.location.y-0.5500373042271853, traj.location.z--0.0020835742108147315)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28.0)
traj.location = (traj.location.x-0.00025677256417111494, traj.location.y-0.5500349182694952, traj.location.z--0.002395079873622308)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32.0)
traj.location = (traj.location.x-0.0002905584260588512, traj.location.y-0.5500321985074708, traj.location.z--0.0027065855363161972)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36.0)
traj.location = (traj.location.x-0.0006824744486948475, traj.location.y-1.1000549126473027, traj.location.z--0.006347823204578162)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44.0)
traj.location = (traj.location.x-0.0008176178962457925, traj.location.y-1.1000400536244737, traj.location.z--0.007593980999104133)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52.0)
traj.location = (traj.location.x-0.0009527613547106739, traj.location.y-1.1000225349784554, traj.location.z--0.008839936078629762)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60.0)
traj.location = (traj.location.x-0.0010811476422531996, traj.location.y-1.100002364817863, traj.location.z--0.010085823586223341)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68.0)
traj.location = (traj.location.x-0.0012162910861661658, traj.location.y-1.0999795390883857, traj.location.z--0.01133171109381692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76.0)
traj.location = (traj.location.x-0.0013514345519070048, traj.location.y-1.099954051032853, traj.location.z--0.012577463458114835)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84.0)
traj.location = (traj.location.x-0.001486577995819971, traj.location.y-1.0999259209227858, traj.location.z--0.013823148250594386)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92.0)
traj.location = (traj.location.x-0.0016217214542848524, traj.location.y-1.0998951352438322, traj.location.z--0.015068630327959909)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100.0)
traj.location = (traj.location.x-0.0017501077381893992, traj.location.y-1.09986168723883, traj.location.z--0.016314044833393382)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108.0)
traj.location = (traj.location.x-0.0018920083603006788, traj.location.y-1.099825597179283, traj.location.z--0.0175592566239402)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116.0)
traj.location = (traj.location.x-0.0020203946478432044, traj.location.y-1.09978685155086, traj.location.z--0.018804333270850293)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124.0)
traj.location = (traj.location.x-0.002155538102670107, traj.location.y-1.099745457110732, traj.location.z--0.02004934234605571)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132.0)
traj.location = (traj.location.x-0.0022839243792986963, traj.location.y-1.0997014138588774, traj.location.z--0.02129401356262406)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140.0)
traj.location = (traj.location.x-0.0024258250123239122, traj.location.y-1.0996547150381595, traj.location.z--0.022538617207487732)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148.0)
traj.location = (traj.location.x-0.0025542112889525015, traj.location.y-1.0996053741628913, traj.location.z--0.023783018137237377)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156.0)
traj.location = (traj.location.x-0.0026893547510553617, traj.location.y-1.0995533912330906, traj.location.z--0.025027216351531933)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164.0)
traj.location = (traj.location.x-0.002824498194968328, traj.location.y-1.0994987527344122, traj.location.z--0.026271144279348846)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172.0)
traj.location = (traj.location.x-0.002959641657071188, traj.location.y-1.0994414789383669, traj.location.z--0.027514869491596983)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180.0)
traj.location = (traj.location.x-0.003088027937337756, traj.location.y-1.0993815563306057, traj.location.z--0.028758324417140102)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188.0)
traj.location = (traj.location.x-0.006588243410078576, traj.location.y-2.198572776619798, traj.location.z--0.0612462027499987)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204.0)
traj.location = (traj.location.x-0.00629092780945939, traj.location.y-1.9514592129521624, traj.location.z--0.05853353575810161)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218.0)
traj.location = (traj.location.x-0.006709872519422788, traj.location.y-1.9512306042844152, traj.location.z--0.062449047067389074)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233.0)
traj.location = (traj.location.x-0.007135574400308542, traj.location.y-1.9509872514657616, traj.location.z--0.06636286908337752)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247.0)
traj.location = (traj.location.x-0.007554519106633961, traj.location.y-1.9507291815249275, traj.location.z--0.07027486666288496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261.0)
traj.location = (traj.location.x-0.007980220994795673, traj.location.y-1.95045640121905, traj.location.z--0.07418497223386566)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275.0)
traj.location = (traj.location.x-0.008392408530198736, traj.location.y-1.9501689173053123, traj.location.z--0.07809311822495602)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289.0)
traj.location = (traj.location.x-0.008818110407446511, traj.location.y-1.9498667770839404, traj.location.z--0.0819989667770642)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304.0)
traj.location = (traj.location.x-0.009237055124685867, traj.location.y-1.9495499670405678, traj.location.z--0.0859027206056453)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318.0)
traj.location = (traj.location.x-0.009662757001933642, traj.location.y-1.9492185412325753, traj.location.z--0.08980410942410799)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332.0)
traj.location = (traj.location.x-0.010074944533698726, traj.location.y-1.948872499659977, traj.location.z--0.09370306565995179)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346.0)
traj.location = (traj.location.x-0.010500646421860438, traj.location.y-1.9485118625942874, traj.location.z--0.09759945417044946)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360.0)
traj.location = (traj.location.x-0.010912833957263501, traj.location.y-1.948136677335718, traj.location.z--0.10149320738344159)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375.0)
traj.location = (traj.location.x-0.011338535838149255, traj.location.y-1.9477469438842547, traj.location.z--0.10538419015574618)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389.0)
traj.location = (traj.location.x-0.011750723373552319, traj.location.y-1.9473427027829544, traj.location.z--0.10927240248713588)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403.0)
traj.location = (traj.location.x-0.012176425258076051, traj.location.y-1.9469239675461338, traj.location.z--0.11315750651908729)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417.0)
traj.location = (traj.location.x-0.01259536996803945, traj.location.y-1.9464907854740403, traj.location.z--0.11703963739500978)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432.0)
traj.location = (traj.location.x-0.013007557499804534, traj.location.y-1.9460431633238073, traj.location.z--0.12091865997149398)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446.0)
traj.location = (traj.location.x-0.013433259384328267, traj.location.y-1.9455811483956822, traj.location.z--0.1247943039617212)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460.0)
traj.location = (traj.location.x-0.01384544691973133, traj.location.y-1.9451047609611507, traj.location.z--0.1286667045088734)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474.0)
traj.location = (traj.location.x-0.014264391629694728, traj.location.y-1.9446140348060794, traj.location.z--0.13253552375454092)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488.0)
traj.location = (traj.location.x-0.014683336343296105, traj.location.y-1.9441089969592014, traj.location.z--0.1364008292704284)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503.0)
traj.location = (traj.location.x-0.015095523867785232, traj.location.y-1.9435897149921857, traj.location.z--0.14026248591301282)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517.0)
traj.location = (traj.location.x-0.015521225763222901, traj.location.y-1.9430561010617993, traj.location.z--0.14412029096706647)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531.0)
traj.location = (traj.location.x-0.015933413294987986, traj.location.y-1.9425084322121933, traj.location.z--0.14797431200452138)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545.0)
traj.location = (traj.location.x-0.01634560082675307, traj.location.y-1.94194643815635, traj.location.z--0.15182441388174084)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559.0)
traj.location = (traj.location.x-0.016764545540354447, traj.location.y-1.9413703891812446, traj.location.z--0.15567032631190614)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574.0)
traj.location = (traj.location.x-0.017183490246679867, traj.location.y-1.940780285286877, traj.location.z--0.15951211686660827)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588.0)
traj.location = (traj.location.x-0.01759567778572091, traj.location.y-1.9401759913297525, traj.location.z--0.16334965040255156)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602.0)
traj.location = (traj.location.x-0.018007865321123973, traj.location.y-1.9395578451685935, traj.location.z--0.16718285934803134)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616.0)
traj.location = (traj.location.x-0.01842681003108737, traj.location.y-1.938925576516354, traj.location.z--0.1710116085594109)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630.0)
traj.location = (traj.location.x-0.018838997566490434, traj.location.y-1.9382794556600658, traj.location.z--0.17483576289328084)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645.0)
traj.location = (traj.location.x-0.019251185101893498, traj.location.y-1.9376194150279673, traj.location.z--0.17865532234986858)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659.0)
traj.location = (traj.location.x-0.01966337264093454, traj.location.y-1.9369455221917349, traj.location.z--0.18247015178531)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673.0)
traj.location = (traj.location.x-0.020082317350897938, traj.location.y-1.9362577771514395, traj.location.z--0.18628004848483215)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687.0)
traj.location = (traj.location.x-0.04139444013344473, traj.location.y-3.87039745093945, traj.location.z--0.3839702588390992)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716.0)
traj.location = (traj.location.x-0.037029306553449715, traj.location.y-3.3355316191819355, traj.location.z--0.34333154090245444)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740.0)
traj.location = (traj.location.x-0.03824559763961588, traj.location.y-3.33329344085908, traj.location.z--0.35459446397794636)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765.0)
traj.location = (traj.location.x-0.03946864590761834, traj.location.y-3.330985663657131, traj.location.z--0.3658296826452556)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789.0)
traj.location = (traj.location.x-0.04069169417198282, traj.location.y-3.3286090308650813, traj.location.z--0.3770362509002325)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814.0)
traj.location = (traj.location.x-0.04190798526178696, traj.location.y-3.3261638127698347, traj.location.z--0.38821369574071696)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838.0)
traj.location = (traj.location.x-0.04311751918066875, traj.location.y-3.3236504823734805, traj.location.z--0.39936093601920675)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863.0)
traj.location = (traj.location.x-0.044333810274110874, traj.location.y-3.321069445106403, traj.location.z--0.41047743116160973)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887.0)
traj.location = (traj.location.x-0.045543344185716705, traj.location.y-3.3184213766858477, traj.location.z--0.42156223516406044)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912.0)
traj.location = (traj.location.x-0.04674612093731412, traj.location.y-3.315706547398733, traj.location.z--0.43261473988076204)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936.0)
traj.location = (traj.location.x-0.04795565485255793, traj.location.y-3.3129255653906, traj.location.z--0.4436341344512016)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961.0)
traj.location = (traj.location.x-0.04915843160051736, traj.location.y-3.310078971235299, traj.location.z--0.4546196080144682)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985.0)
traj.location = (traj.location.x-0.050361208344838815, traj.location.y-3.307167102791425, traj.location.z--0.46557068756857234)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010.0)
traj.location = (traj.location.x-0.05155047074731556, traj.location.y-3.304190703348013, traj.location.z--0.4764863595375459)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034.0)
traj.location = (traj.location.x-0.05274649031707668, traj.location.y-3.301150245907081, traj.location.z--0.4873661509193994)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059.0)
traj.location = (traj.location.x-0.0539425098868378, traj.location.y-3.2980461358991136, traj.location.z--0.4982092508532219)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083.0)
traj.location = (traj.location.x-0.05512501512203016, traj.location.y-3.2948791841847083, traj.location.z--0.5090150511937281)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108.0)
traj.location = (traj.location.x-0.05631427751723095, traj.location.y-3.2916496610509114, traj.location.z--0.5197828762234735)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132.0)
traj.location = (traj.location.x-0.05749678274514736, traj.location.y-3.2883583097865596, traj.location.z--0.5305120502250702)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157.0)
traj.location = (traj.location.x-0.05867253080214141, traj.location.y-3.28500567096566, traj.location.z--0.5412019650531761)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181.0)
traj.location = (traj.location.x-0.05985503603005782, traj.location.y-3.2815924203052873, traj.location.z--0.5518518774188692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206.0)
traj.location = (traj.location.x-0.061024026912491536, traj.location.y-3.278118963235954, traj.location.z--0.5624613143197621)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230.0)
traj.location = (traj.location.x-0.062193017791287275, traj.location.y-3.274586043046668, traj.location.z--0.573029600038808)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255.0)
traj.location = (traj.location.x-0.063362008673721, traj.location.y-3.2709942003110655, traj.location.z--0.5835561940021421)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279.0)
traj.location = (traj.location.x-0.06453099955979269, traj.location.y-3.2673440431748872, traj.location.z--0.5940404204924334)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304.0)
traj.location = (traj.location.x-0.06568647609310574, traj.location.y-3.2636362473553504, traj.location.z--0.6044817389358172)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328.0)
traj.location = (traj.location.x-0.13483938179706456, traj.location.y-6.515921593246276, traj.location.z--1.2401132332891507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377.0)
traj.location = (traj.location.x-0.11126360614434816, traj.location.y-5.205334426985672, traj.location.z--1.022543623483159)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416.0)
traj.location = (traj.location.x-0.102648210919142, traj.location.y-4.6761163077663355, traj.location.z--0.9427335076679242)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452.0)
traj.location = (traj.location.x-0.10501997854953515, traj.location.y-4.667721196368461, traj.location.z--0.9638539294632551)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487.0)
traj.location = (traj.location.x-0.10737147464897134, traj.location.y-4.659167494126706, traj.location.z--0.9848346804981247)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522.0)
traj.location = (traj.location.x-0.10970945642111474, traj.location.y-4.650457836338404, traj.location.z--1.0056738011926996)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557.0)
traj.location = (traj.location.x-0.11205419535690453, traj.location.y-4.641595398874642, traj.location.z--1.0263693995384529)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593.0)
traj.location = (traj.location.x-0.11436514843080658, traj.location.y-4.6325828170328975, traj.location.z--1.0469194483837896)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628.0)
traj.location = (traj.location.x-0.116682858675631, traj.location.y-4.623423063969113, traj.location.z--1.0673223935786496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663.0)
traj.location = (traj.location.x-0.1189802973967744, traj.location.y-4.6141191804110235, traj.location.z--1.0875764106866654)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699.0)
traj.location = (traj.location.x-0.12126422178698704, traj.location.y-4.604673936799429, traj.location.z--1.1076799455580044)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734.0)
traj.location = (traj.location.x-0.12354138899536338, traj.location.y-4.595090373862178, traj.location.z--1.1276313764712995)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769.0)
traj.location = (traj.location.x-0.12580504185461905, traj.location.y-4.585371532326803, traj.location.z--1.1474292168481952)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805.0)
traj.location = (traj.location.x-0.12805518037930597, traj.location.y-4.575520115062545, traj.location.z--1.1670721828265869)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840.0)
traj.location = (traj.location.x-0.1302985617185186, traj.location.y-4.565539433083956, traj.location.z--1.1865589229711304)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875.0)
traj.location = (traj.location.x-0.13252167155587813, traj.location.y-4.555432121688511, traj.location.z--1.2058881534195507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910.0)
traj.location = (traj.location.x-0.1347380242041254, traj.location.y-4.545201424318975, traj.location.z--1.2250587254523566)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946.0)
traj.location = (traj.location.x-0.13693410534324357, traj.location.y-4.5348502465596425, traj.location.z--1.244069557921648)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981.0)
traj.location = (traj.location.x-0.13913018647872377, traj.location.y-4.524381426423247, traj.location.z--1.2629195696801503)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016.0)
traj.location = (traj.location.x-0.14129923892687657, traj.location.y-4.513798072209056, traj.location.z--1.2816079498669524)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052.0)
traj.location = (traj.location.x-0.14346829136411543, traj.location.y-4.503103089501337, traj.location.z--1.3001336173345521)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087.0)
traj.location = (traj.location.x-0.14561707230313914, traj.location.y-4.492299383884415, traj.location.z--1.3184959639371527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122.0)
traj.location = (traj.location.x-0.1477590960530506, traj.location.y-4.481390063657784, traj.location.z--1.3366941788142412)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158.0)
traj.location = (traj.location.x-0.149880848290195, traj.location.y-4.470377899262246, traj.location.z--1.3547274511050205)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193.0)
traj.location = (traj.location.x-0.1519890861782187, traj.location.y-4.459265863853716, traj.location.z--1.3725953753792055)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228.0)
traj.location = (traj.location.x-0.15409056689895806, traj.location.y-4.448056795444813, traj.location.z--1.39029734349117)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264.0)
traj.location = (traj.location.x-0.15617853327057674, traj.location.y-4.436753734763386, traj.location.z--1.4078328148669925)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299.0)
traj.location = (traj.location.x-0.15824622813306632, traj.location.y-4.425359317106597, traj.location.z--1.4252015192198542)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334.0)
traj.location = (traj.location.x-0.16030716580644366, traj.location.y-4.41387665077383, traj.location.z--1.442402983547879)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369.0)
traj.location = (traj.location.x-0.16235458914889023, traj.location.y-4.402308371062816, traj.location.z--1.4594369375638507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405.0)
traj.location = (traj.location.x-0.16438849813857814, traj.location.y-4.390657383557482, traj.location.z--1.4763032461244165)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440.0)
traj.location = (traj.location.x-0.166402135611861, traj.location.y-4.378926391127038, traj.location.z--1.4930016389426441)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475.0)
traj.location = (traj.location.x-0.1684157730815059, traj.location.y-4.367118299355809, traj.location.z--1.5095320484469994)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511.0)
traj.location = (traj.location.x-0.17040238187109935, traj.location.y-4.355235743541073, traj.location.z--1.5258944070654366)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546.0)
traj.location = (traj.location.x-0.17238223347158055, traj.location.y-4.343281358980278, traj.location.z--1.542088849941706)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581.0)
traj.location = (traj.location.x-0.17434181357020861, traj.location.y-4.331257983685873, traj.location.z--1.558115174360296)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617.0)
traj.location = (traj.location.x-0.17630139365428477, traj.location.y-4.319168117811898, traj.location.z--1.5739736506085364)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652.0)
traj.location = (traj.location.x-0.1782407022328698, traj.location.y-4.307014599370689, traj.location.z--1.589664548973019)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687.0)
traj.location = (traj.location.x-0.18015973928413587, traj.location.y-4.294799793372988, traj.location.z--1.605187801882039)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722.0)
traj.location = (traj.location.x-0.18207201916811755, traj.location.y-4.28252647025937, traj.location.z--1.6205438823377563)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758.0)
traj.location = (traj.location.x-0.1839775418775389, traj.location.y-4.270196995040408, traj.location.z--1.6357329930555693)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793.0)
traj.location = (traj.location.x-0.18585603589963284, traj.location.y-4.257813935441675, traj.location.z--1.6507556070370697)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828.0)
traj.location = (traj.location.x-0.18772777274352848, traj.location.y-4.245379791617097, traj.location.z--1.66561192699794)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864.0)
traj.location = (traj.location.x-0.18958599523466546, traj.location.y-4.232896996148952, traj.location.z--1.6803024935116468)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899.0)
traj.location = (traj.location.x-0.19143070339850965, traj.location.y-4.22036798161929, traj.location.z--1.6948279147238736)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934.0)
traj.location = (traj.location.x-0.19326189721323317, traj.location.y-4.207794977895162, traj.location.z--1.7091884609217232)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970.0)
traj.location = (traj.location.x-0.1950795766642841, traj.location.y-4.195180485130663, traj.location.z--1.7233849429654242)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005.0)
traj.location = (traj.location.x-0.19688374178804224, traj.location.y-4.1825266656205144, traj.location.z--1.7374176987141254)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040.0)
traj.location = (traj.location.x-0.1986743925663177, traj.location.y-4.1698359519476185, traj.location.z--1.751287539028283)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075.0)
traj.location = (traj.location.x-0.20045152898455854, traj.location.y-4.157110506406582, traj.location.z--1.7649951396251709)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111.0)
traj.location = (traj.location.x-0.20222190824279096, traj.location.y-4.144352356149852, traj.location.z--1.7785411086505292)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146.0)
traj.location = (traj.location.x-0.20396525881369598, traj.location.y-4.131563933759139, traj.location.z--1.7919261893930525)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181.0)
traj.location = (traj.location.x-0.20570860937368707, traj.location.y-4.118747198814731, traj.location.z--1.8051512602855269)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217.0)
traj.location = (traj.location.x-0.20743168841363513, traj.location.y-4.105904246040097, traj.location.z--1.8182169970449422)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252.0)
traj.location = (traj.location.x-0.2091480102899368, traj.location.y-4.093037170158823, traj.location.z--1.8311242781040278)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287.0)
traj.location = (traj.location.x-0.2108440606498334, traj.location.y-4.08014799832273, traj.location.z--1.843873914323126)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323.0)
traj.location = (traj.location.x-0.21253335382789373, traj.location.y-4.067238622540231, traj.location.z--1.8564669192786027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358.0)
traj.location = (traj.location.x-0.214202375493187, traj.location.y-4.0543110699630915, traj.location.z--1.8689039686873912)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393.0)
traj.location = (traj.location.x-0.21586463998391991, traj.location.y-4.041367232599612, traj.location.z--1.8811861436972777)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428.0)
traj.location = (traj.location.x-0.21751339012917015, traj.location.y-4.0284088673149085, traj.location.z--1.8933144578840029)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464.0)
traj.location = (traj.location.x-0.43992573048672057, traj.location.y-8.017894324990095, traj.location.z--3.8224027396273073)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534.0)
traj.location = (traj.location.x-0.33449356440178235, traj.location.y-5.985151892025044, traj.location.z--2.9003517889617)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587.0)
traj.location = (traj.location.x-0.33808838029654, traj.location.y-5.955842520171927, traj.location.z--2.9261594587634363)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640.0)
traj.location = (traj.location.x-0.3416358959766512, traj.location.y-5.926518214966961, traj.location.z--2.951466016635777)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693.0)
traj.location = (traj.location.x-0.34513611145303, traj.location.y-5.897187287732947, traj.location.z--2.976276665601347)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746.0)
traj.location = (traj.location.x-0.3485957838893228, traj.location.y-5.867857509218084, traj.location.z--3.0005972844009534)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799.0)
traj.location = (traj.location.x-0.35201491328916745, traj.location.y-5.838536650171363, traj.location.z--3.0244334814873355)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852.0)
traj.location = (traj.location.x-0.3554002568234864, traj.location.y-5.809232075910927, traj.location.z--3.0477910004578916)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905.0)
traj.location = (traj.location.x-0.35873830015407293, traj.location.y-5.779950949039744, traj.location.z--3.070675787624225)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958.0)
traj.location = (traj.location.x-0.36204255761913373, traj.location.y-5.750700229445897, traj.location.z--3.093093789298507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011.0)
traj.location = (traj.location.x-0.36529951486954815, traj.location.y-5.721486404015138, traj.location.z--3.1150510193644436)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064.0)
traj.location = (traj.location.x-0.3685226862653508, traj.location.y-5.6923159596333335, traj.location.z--3.1365534917057403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117.0)
traj.location = (traj.location.x-0.3717053146210674, traj.location.y-5.663195112899302, traj.location.z--3.1576072877778643)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170.0)
traj.location = (traj.location.x-0.3748541571148962, traj.location.y-5.6341296074100455, traj.location.z--3.1782186917515105)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223.0)
traj.location = (traj.location.x-0.37795569938680273, traj.location.y-5.605125254333757, traj.location.z--3.198393717510328)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276.0)
traj.location = (traj.location.x-0.38103021298593376, traj.location.y-5.576187391837266, traj.location.z--3.218138581653136)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329.0)
traj.location = (traj.location.x-0.38406418354134075, traj.location.y-5.547321290515129, traj.location.z--3.2374595007791527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382.0)
traj.location = (traj.location.x-0.3870576110602997, traj.location.y-5.518532018246901, traj.location.z--3.256362826630493)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436.0)
traj.location = (traj.location.x-0.3900172527137329, traj.location.y-5.489824237481798, traj.location.z--3.274854437947397)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489.0)
traj.location = (traj.location.x-0.3929431085016404, traj.location.y-5.461202543097215, traj.location.z--3.292940956759253)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542.0)
traj.location = (traj.location.x-0.3958351784349361, traj.location.y-5.432671394827253, traj.location.z--3.310628126662891)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595.0)
traj.location = (traj.location.x-0.3986867053208698, traj.location.y-5.4042348469754415, traj.location.z--3.3279225696877006)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648.0)
traj.location = (traj.location.x-0.40150444635219174, traj.location.y-5.375896886273836, traj.location.z--3.344830164573466)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701.0)
traj.location = (traj.location.x-0.40428840151798795, traj.location.y-5.347661499454148, traj.location.z--3.3613571279198027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754.0)
traj.location = (traj.location.x-0.4070453279928188, traj.location.y-5.319531929959226, traj.location.z--3.377509541181496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807.0)
traj.location = (traj.location.x-0.40976846859120997, traj.location.y-5.29151196180635, traj.location.z--3.393293553386229)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860.0)
traj.location = (traj.location.x-0.41245106617861893, traj.location.y-5.263604500579049, traj.location.z--3.4087150432740145)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913.0)
traj.location = (traj.location.x-0.4150998778823123, traj.location.y-5.235812857292558, traj.location.z--3.4237802274438423)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966.0)
traj.location = (traj.location.x-0.4177284180659626, traj.location.y-5.208139937530859, traj.location.z--3.4384949170641903)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019.0)
traj.location = (traj.location.x-0.42031641523499275, traj.location.y-5.180588376591345, traj.location.z--3.4528649908754687)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072.0)
traj.location = (traj.location.x-0.4228773836912296, traj.location.y-5.153160809771066, traj.location.z--3.466896395189565)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125.0)
traj.location = (traj.location.x-0.4254045662928547, traj.location.y-5.125859669653096, traj.location.z--3.4805948736033088)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178.0)
traj.location = (traj.location.x-0.42790472019987646, traj.location.y-5.098687388818803, traj.location.z--3.493966237285292)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231.0)
traj.location = (traj.location.x-0.4303710882522864, traj.location.y-5.07164599442001, traj.location.z--3.507016162260527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284.0)
traj.location = (traj.location.x-0.4328104275955411, traj.location.y-5.044737446037516, traj.location.z--3.5197501894108996)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337.0)
traj.location = (traj.location.x-0.4352227382732963, traj.location.y-5.0179638383941665, traj.location.z--3.5321739271892625)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390.0)
traj.location = (traj.location.x-0.4376080202382582, traj.location.y-4.99132679321167, traj.location.z--3.5442930516213096)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443.0)
traj.location = (traj.location.x-0.43995275917768595, traj.location.y-4.964828067354915, traj.location.z--3.556112833301313)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496.0)
traj.location = (traj.location.x-0.44228398376799305, traj.location.y-4.938468944686747, traj.location.z--3.5676386103959885)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549.0)
traj.location = (traj.location.x-0.44458142249277444, traj.location.y-4.912251182072168, traj.location.z--3.5788759237868817)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602.0)
traj.location = (traj.location.x-0.4468518325338664, traj.location.y-4.886175860658568, traj.location.z--3.5898298413534917)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655.0)
traj.location = (traj.location.x-0.449095213880355, traj.location.y-4.860243926450153, traj.location.z--3.6005055661189544)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708.0)
traj.location = (traj.location.x-0.45131156653587823, traj.location.y-4.834457744457836, traj.location.z--3.610908233534417)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761.0)
traj.location = (traj.location.x-0.45350764766772045, traj.location.y-4.808815625387751, traj.location.z--3.621042911479435)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814.0)
traj.location = (traj.location.x-0.4556699429413129, traj.location.y-4.7833221641178625, traj.location.z--3.6309146002618604)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867.0)
traj.location = (traj.location.x-0.4578119667021383, traj.location.y-4.757974657778959, traj.location.z--3.6405280299026685)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920.0)
traj.location = (traj.location.x-0.4599269617610844, traj.location.y-4.732775809239911, traj.location.z--3.649888268281302)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973.0)
traj.location = (traj.location.x-0.4620216853072634, traj.location.y-4.707725618501172, traj.location.z--3.6589999778469746)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026.0)
traj.location = (traj.location.x-0.464089380166115, traj.location.y-4.682825436996723, traj.location.z--3.6678676859054633)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079.0)
traj.location = (traj.location.x-0.4661300463230873, traj.location.y-4.658074589009516, traj.location.z--3.6764963251927725)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132.0)
traj.location = (traj.location.x-0.4681504409745685, traj.location.y-4.633473750257167, traj.location.z--3.6848901527277746)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185.0)
traj.location = (traj.location.x-0.4701438069278083, traj.location.y-4.609024272173883, traj.location.z--3.693053830959684)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238.0)
traj.location = (traj.location.x-0.4721236585355655, traj.location.y-4.584724803324775, traj.location.z--3.7009917520506974)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291.0)
traj.location = (traj.location.x-0.4740697242850729, traj.location.y-4.560577370862688, traj.location.z--3.708708105447954)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344.0)
traj.location = (traj.location.x-0.47600227568182163, traj.location.y-4.536579947634664, traj.location.z--3.7162072833137643)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397.0)
traj.location = (traj.location.x-0.477907798391243, traj.location.y-4.512734560793433, traj.location.z--3.7234935426668017)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450.0)
traj.location = (traj.location.x-0.4797998067588196, traj.location.y-4.489040534620813, traj.location.z--3.730570802667387)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503.0)
traj.location = (traj.location.x-0.48165802925723256, traj.location.y-4.465496517683164, traj.location.z--3.7374432527626595)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556.0)
traj.location = (traj.location.x-0.4835027374101628, traj.location.y-4.442105212849128, traj.location.z--3.744114879684332)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609.0)
traj.location = (traj.location.x-0.485327174046688, traj.location.y-4.41886391724961, traj.location.z--3.7505896025926972)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662.0)
traj.location = (traj.location.x-0.48713133917044615, traj.location.y-4.395773982319042, traj.location.z--3.756871273076314)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715.0)
traj.location = (traj.location.x-0.488908475592325, traj.location.y-4.372833380906172, traj.location.z--3.7629636075799624)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768.0)
traj.location = (traj.location.x-0.4906788548505574, traj.location.y-4.350044815879301, traj.location.z--3.768870390120611)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821.0)
traj.location = (traj.location.x-0.4924154482432641, traj.location.y-4.327404908652852, traj.location.z--3.7745953371430403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874.0)
traj.location = (traj.location.x-0.4941452844541345, traj.location.y-4.304915010660466, traj.location.z--3.7801418948054106)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927.0)
traj.location = (traj.location.x-0.49584133481403114, traj.location.y-4.282574446185663, traj.location.z--3.785513644409434)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980.0)
traj.location = (traj.location.x-0.49753738517392776, traj.location.y-4.260381863793782, traj.location.z--3.7907140996846636)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033.0)
traj.location = (traj.location.x-0.49920640683194506, traj.location.y-4.238338614918803, traj.location.z--3.7957466392176116)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086.0)
traj.location = (traj.location.x-0.500848399802635, traj.location.y-4.216442672409698, traj.location.z--3.800614506451211)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139.0)
traj.location = (traj.location.x-0.5024903927806008, traj.location.y-4.194694036265787, traj.location.z--3.8053211475435376)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192.0)
traj.location = (traj.location.x-0.504098599889403, traj.location.y-4.173091355052861, traj.location.z--3.8098696032221824)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245.0)
traj.location = (traj.location.x-0.5057000498272828, traj.location.y-4.151635980205697, traj.location.z--3.814263116930249)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298.0)
traj.location = (traj.location.x-0.5072744710705592, traj.location.y-4.130325208854515, traj.location.z--3.8185047969670904)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351.0)
traj.location = (traj.location.x-0.5088421351392753, traj.location.y-4.109159040999884, traj.location.z--3.822597684060412)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404.0)
traj.location = (traj.location.x-0.5103895276988624, traj.location.y-4.088138152359079, traj.location.z--3.8265447513663275)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457.0)
traj.location = (traj.location.x-0.5119166487347684, traj.location.y-4.0672598400625475, traj.location.z--3.8303488368972296)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510.0)
traj.location = (traj.location.x-0.5134370125961141, traj.location.y-4.046524779828019, traj.location.z--3.8340129813809654)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563.0)
traj.location = (traj.location.x-0.5149303477737703, traj.location.y-4.025930944503671, traj.location.z--3.8375398201148414)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616.0)
traj.location = (traj.location.x-0.5164169257768663, traj.location.y-4.005479685523483, traj.location.z--3.8409322586832104)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669.0)
traj.location = (traj.location.x-0.5178832322526432, traj.location.y-3.985168300019268, traj.location.z--3.844192999955027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722.0)
traj.location = (traj.location.x-0.5193360243902134, traj.location.y-3.964996112272729, traj.location.z--3.8473245440843016)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775.0)
traj.location = (traj.location.x-0.5207685450113786, traj.location.y-3.944963798002277, traj.location.z--3.8503297290835405)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828.0)
traj.location = (traj.location.x-0.5221943084507075, traj.location.y-3.9250700057723407, traj.location.z--3.85321091996326)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881.0)
traj.location = (traj.location.x-0.5236065575554676, traj.location.y-3.905312032714164, traj.location.z--3.855970684448991)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934.0)
traj.location = (traj.location.x-0.5249917779692623, traj.location.y-3.885691905979911, traj.location.z--3.85861152269473)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987.0)
traj.location = (traj.location.x-0.5263769983721431, traj.location.y-3.86620692270003, traj.location.z--3.861135867282627)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040.0)
traj.location = (traj.location.x-0.5277351900913345, traj.location.y-3.84685708287509, traj.location.z--3.8635459480796897)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093.0)
traj.location = (traj.location.x-0.5290933818141639, traj.location.y-3.827641035069746, traj.location.z--3.865844197668082)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146.0)
traj.location = (traj.location.x-0.5304245448351139, traj.location.y-3.8085581035674068, traj.location.z--3.868032913486573)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199.0)
traj.location = (traj.location.x-0.5317557078669779, traj.location.y-3.789607612650343, traj.location.z--3.870114190258633)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252.0)
traj.location = (traj.location.x-0.5330598421969626, traj.location.y-3.770788210884234, traj.location.z--3.8720903929948207)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305.0)
traj.location = (traj.location.x-0.5343639765414991, traj.location.y-3.7520998982693072, traj.location.z--3.8739634812751547)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358.0)
traj.location = (traj.location.x-0.5356478393587167, traj.location.y-3.733540647653399, traj.location.z--3.875735617394895)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411.0)
traj.location = (traj.location.x-0.5369181878268137, traj.location.y-3.7151097833191216, traj.location.z--3.877408963649401)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464.0)
traj.location = (traj.location.x-0.5381750219603418, traj.location.y-3.6968073052669297, traj.location.z--3.878985344475197)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517.0)
traj.location = (traj.location.x-0.5394250989120337, traj.location.y-3.6786311863445462, traj.location.z--3.880466922167656)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570.0)
traj.location = (traj.location.x-0.5406616615218809, traj.location.y-3.6605821022697, traj.location.z--3.8818555549492118)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623.0)
traj.location = (traj.location.x-0.5418847097898833, traj.location.y-3.6426566744555657, traj.location.z--3.883153121313903)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676.0)
traj.location = (traj.location.x-0.5430942437051272, traj.location.y-3.6248569300543068, traj.location.z--3.8843615132701075)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729.0)
traj.location = (traj.location.x-0.5442970204458106, traj.location.y-3.6071788147621646, traj.location.z--3.8854825484972793)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782.0)
traj.location = (traj.location.x-0.545479525673727, traj.location.y-3.5896243557306207, traj.location.z--3.886518017646189)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835.0)
traj.location = (traj.location.x-0.5466620309089194, traj.location.y-3.5721901743743274, traj.location.z--3.887469670824572)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888.0)
traj.location = (traj.location.x-0.5478242646131548, traj.location.y-3.5548776221265825, traj.location.z--3.8883392311114804)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941.0)
traj.location = (traj.location.x-0.5489797411537438, traj.location.y-3.5376839961189717, traj.location.z--3.889128381042916)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994.0)
traj.location = (traj.location.x-0.5501217033379362, traj.location.y-3.520608620634107, traj.location.z--3.8898387693690353)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047.0)
traj.location = (traj.location.x-0.5512501511911978, traj.location.y-3.5036521713896036, traj.location.z--3.890472004296928)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100.0)
traj.location = (traj.location.x-0.5523785990299075, traj.location.y-3.4868112697981815, traj.location.z--3.8910296940337297)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153.0)
traj.location = (traj.location.x-0.5534867753594881, traj.location.y-3.4700872672955256, traj.location.z--3.891513372457638)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206.0)
traj.location = (traj.location.x-0.5545881945181463, traj.location.y-3.4534774610114027, traj.location.z--3.891924566689678)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259.0)
traj.location = (traj.location.x-0.5556828564949683, traj.location.y-3.4369825266642238, traj.location.z--3.892264776822211)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312.0)
traj.location = (traj.location.x-0.5567572469626612, traj.location.y-3.4206004371013705, traj.location.z--3.8925354556473692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365.0)
traj.location = (traj.location.x-0.557831637426716, traj.location.y-3.4043298408882947, traj.location.z--3.8927380424429394)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418.0)
traj.location = (traj.location.x-0.5588925135416503, traj.location.y-3.388172089460113, traj.location.z--3.892873931889377)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471.0)
traj.location = (traj.location.x-0.5599398753111018, traj.location.y-3.372123804230114, traj.location.z--3.8929445106260987)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524.0)
traj.location = (traj.location.x-0.5609804799205449, traj.location.y-3.3561849851981833, traj.location.z--3.8929511237359047)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577.0)
traj.location = (traj.location.x-0.5620210845154361, traj.location.y-3.3403556323644352, traj.location.z--3.8928950933272106)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630.0)
traj.location = (traj.location.x-0.5630346604302758, traj.location.y-3.3246337185772745, traj.location.z--3.89277771380403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683.0)
traj.location = (traj.location.x-0.5640482363305637, traj.location.y-3.3090185681189723, traj.location.z--3.8926002501766597)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736.0)
traj.location = (traj.location.x-0.5650550550744811, traj.location.y-3.2935088295553214, traj.location.z--3.8923639586035232)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789.0)
traj.location = (traj.location.x-0.5660551166329242, traj.location.y-3.278105854320984, traj.location.z--3.892070041861345)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842.0)
traj.location = (traj.location.x-0.5670349066786002, traj.location.y-3.262806263829134, traj.location.z--3.8917197162412442)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895.0)
traj.location = (traj.location.x-0.5680146967279143, traj.location.y-3.247609382362839, traj.location.z--3.8913141237053885)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948.0)
traj.location = (traj.location.x-0.5689809724281076, traj.location.y-3.232516561356533, traj.location.z--3.890854446759022)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10001.0)
traj.location = (traj.location.x-0.5699404909537407, traj.location.y-3.217523746506572, traj.location.z--3.890341773306943)
traj.keyframe_insert(data_path="location", index =-1)

col = bpy.data.materials.new('Orion_trajectory')  # Changing the
col.roughness = 1  # color and some texture for the trajectories
col.specular_intensity = 0
col.use_backface_culling = True
bpy.context.object.data.twist_smooth = 0.5
bpy.data.materials['Orion_trajectory'].use_nodes = True
col.node_tree.nodes.new('ShaderNodeEmission')
node_1 = col.node_tree.nodes["Material Output"]
node_2 = col.node_tree.nodes["Emission"]
col.node_tree.links.new(node_1.inputs["Surface"], node_2.outputs[0])
bpy.data.materials['Orion_trajectory'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 1, 0, 1)
mesh=bpy.context.object.data
mesh.materials.clear()
mesh.materials.append(col)

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Orion_trajectory = bpy.data.objects['Orion_trajectory']
Orion_trajectory.name = 'OrionTraj'

print("importing Sphere.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Sphere.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Sphere'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Sphere']
ob.select_set(True)
ob.active_material.use_backface_culling = True
for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Sphere':
        material.name = 'OrionSphere1'
bpy.data.materials['OrionSphere1'].node_tree.nodes["Emission"].inputs[0].default_value = (0.330000, 0, 1, 1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Sphere = bpy.data.objects['Sphere']
Sphere.select_set(True)
bpy.ops.transform.resize(value=(RATIO*3000, RATIO*3000, RATIO*3000))

bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
Orion = bpy.data.objects['Orion']  # exactly the movement of the parent)
Orion.select_set(True)
Sphere = bpy.data.objects['Sphere']
Sphere.select_set(True)
bpy.context.view_layer.objects.active = Sphere
bpy.ops.object.constraint_add(type='COPY_LOCATION')
bpy.context.object.constraints["Copy Location"].target = bpy.data.objects['Orion']

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Sphere = bpy.data.objects['Sphere']
Sphere.name = 'OrionSphere1'

print("importing Cone.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Cone.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Cone'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Cone']
ob.select_set(True)
ob.active_material.use_backface_culling = True
for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Cone':
        material.name = 'OrionCone'
bpy.data.materials['OrionCone'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 1, 0, 1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.ops.object.select_all(action='DESELECT')  # Move the constraint and rotate it
bpy.data.objects['Cone'].location.x = 0*RATIO
bpy.data.objects['Cone'].location.y = 0*RATIO
bpy.data.objects['Cone'].location.z = 3*RATIO
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.ops.transform.rotate(value=0.0, orient_axis='X')
bpy.ops.transform.rotate(value=0.0, orient_axis='Y')
bpy.ops.transform.rotate(value=0.0, orient_axis='Z')

bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
Orion = bpy.data.objects['Orion']  # exactly the movement of the parent)
Orion.select_set(True)
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.context.view_layer.objects.active = Orion
bpy.ops.object.parent_set(type='OBJECT')

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Cone = bpy.data.objects['Cone']
Cone.name = 'OrionCone'

print("importing Point.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Point.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Point'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Point']
ob.select_set(True)
ob.active_material.use_backface_culling = True
for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Point':
        material.name = 'OrionPoint'
bpy.data.materials['OrionPoint'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0.800000, 1)

ob = bpy.data.objects['Point']
bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (0.9593631124662352, 131.29577512087266, -8.22459988762688)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (0.9593631124662352, 131.26139732484506, -8.224595630608007)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0) # set the points to keyframes
ob.location = (0.9593631124662352, 131.22701952908776, -8.224590089726405)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (0.9593563552989508, 131.1582639389246, -8.224575494233306)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1) # set the points to keyframes
ob.location = (0.9593563552989508, 131.0207527660311, -8.22453170775401)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2) # set the points to keyframes
ob.location = (0.9593360837789078, 130.74573046822013, -8.22438568525132)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4) # set the points to keyframes
ob.location = (0.9593158122588648, 130.47070826879357, -8.224161752547104)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6) # set the points to keyframes
ob.location = (0.959282026396977, 130.19568621005135, -8.223859909641362)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8) # set the points to keyframes
ob.location = (0.959241483356891, 129.92066433240134, -8.22348015653398)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10) # set the points to keyframes
ob.location = (0.9591941831495205, 129.64564267841376, -8.223022560796892)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12) # set the points to keyframes
ob.location = (0.959065796865616, 129.09560020494945, -8.2218737062895)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16) # set the points to keyframes
ob.location = (0.958910381894384, 128.54555912211129, -8.22041327854771)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20) # set the points to keyframes
ob.location = (0.9587211810576264, 127.99551976235215, -8.218641277571407)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24) # set the points to keyframes
ob.location = (0.958498194358981, 127.44548245812496, -8.216557703360593)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28) # set the points to keyframes
ob.location = (0.9582414217948099, 126.89544753985547, -8.21416262348697)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32) # set the points to keyframes
ob.location = (0.957950863368751, 126.34541534134799, -8.211456037950654)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36) # set the points to keyframes
ob.location = (0.9572683889200562, 125.24536042870069, -8.205108214746076)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44) # set the points to keyframes
ob.location = (0.9564507710238104, 124.14532037507621, -8.197514233746972)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52) # set the points to keyframes
ob.location = (0.9554980096690997, 123.04529784009776, -8.188674297668342)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60) # set the points to keyframes
ob.location = (0.9544168620268465, 121.94529547527989, -8.178588474082119)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68) # set the points to keyframes
ob.location = (0.9532005709406803, 120.84531593619151, -8.167256762988302)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76) # set the points to keyframes
ob.location = (0.9518491363887733, 119.74536188515866, -8.154679299530187)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84) # set the points to keyframes
ob.location = (0.9503625583929534, 118.64543596423587, -8.140856151279593)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92) # set the points to keyframes
ob.location = (0.9487408369386685, 117.54554082899205, -8.125787520951633)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100) # set the points to keyframes
ob.location = (0.9469907292004791, 116.4456791417532, -8.10947347611824)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108) # set the points to keyframes
ob.location = (0.9450987208401784, 115.34585354457393, -8.0919142194943)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116) # set the points to keyframes
ob.location = (0.9430783261923352, 114.24606669302307, -8.073109886223449)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124) # set the points to keyframes
ob.location = (0.9409227880896651, 113.14632123591234, -8.053060543877393)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132) # set the points to keyframes
ob.location = (0.9386388637103664, 112.04661982205346, -8.03176653031477)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140) # set the points to keyframes
ob.location = (0.9362130386980425, 110.9469651070153, -8.009227913107281)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148) # set the points to keyframes
ob.location = (0.93365882740909, 109.84735973285241, -7.985444894970044)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156) # set the points to keyframes
ob.location = (0.9309694726580346, 108.74780634161931, -7.960417678618512)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164) # set the points to keyframes
ob.location = (0.9281449744630663, 107.6483075888849, -7.934146534339163)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172) # set the points to keyframes
ob.location = (0.9251853328059951, 106.54886610994654, -7.906631664847566)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180) # set the points to keyframes
ob.location = (0.9220973048686574, 105.44948455361593, -7.877873340430426)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188) # set the points to keyframes
ob.location = (0.9155090614585788, 103.25091177699613, -7.8166271376804275)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204) # set the points to keyframes
ob.location = (0.9092181336491194, 101.29945256404397, -7.758093601922326)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218) # set the points to keyframes
ob.location = (0.9025082611296966, 99.34822195975956, -7.695644554854937)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233) # set the points to keyframes
ob.location = (0.8953726867293881, 97.3972347082938, -7.629281685771559)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247) # set the points to keyframes
ob.location = (0.8878181676227541, 95.44650552676887, -7.559006819108674)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261) # set the points to keyframes
ob.location = (0.8798379466279584, 93.49604912554982, -7.484821846874809)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275) # set the points to keyframes
ob.location = (0.8714455380977597, 91.54588020824451, -7.406728728649853)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289) # set the points to keyframes
ob.location = (0.8626274276903132, 89.59601343116057, -7.3247297618727885)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304) # set the points to keyframes
ob.location = (0.8533903725656273, 87.64646346411999, -7.238827041267143)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318) # set the points to keyframes
ob.location = (0.8437276155636937, 85.69724492288742, -7.149022931843035)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332) # set the points to keyframes
ob.location = (0.833652671029995, 83.74837242322744, -7.055319866183083)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346) # set the points to keyframes
ob.location = (0.8231520246081345, 81.79986056063316, -6.957720412012634)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360) # set the points to keyframes
ob.location = (0.812239190650871, 79.85172388329744, -6.856227204629192)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375) # set the points to keyframes
ob.location = (0.8009006548127218, 77.90397693941318, -6.750843014473446)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389) # set the points to keyframes
ob.location = (0.7891499314391694, 75.95663423663024, -6.64157061198631)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403) # set the points to keyframes
ob.location = (0.7769735061810934, 74.0097102690841, -6.528413105467223)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417) # set the points to keyframes
ob.location = (0.764378136213054, 72.06321948361006, -6.411373468072213)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432) # set the points to keyframes
ob.location = (0.7513705787132494, 70.11717632028625, -6.290454808100719)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446) # set the points to keyframes
ob.location = (0.7379373193289211, 68.17159517189057, -6.165660504138998)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460) # set the points to keyframes
ob.location = (0.7240918724091898, 66.22649041092942, -6.036993799630125)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474) # set the points to keyframes
ob.location = (0.7098274807794951, 64.28187637612334, -5.904458275875584)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488) # set the points to keyframes
ob.location = (0.695144144436199, 62.337767379164134, -5.768057446605155)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503) # set the points to keyframes
ob.location = (0.6800486205684138, 60.39417766417195, -5.6277949606921425)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517) # set the points to keyframes
ob.location = (0.6645273948051909, 58.45112156311015, -5.483674669725076)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531) # set the points to keyframes
ob.location = (0.6485939815102029, 56.508613130897956, -5.335700357720555)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545) # set the points to keyframes
ob.location = (0.6322483806834498, 54.566666692741606, -5.183875943838814)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559) # set the points to keyframes
ob.location = (0.6154838351430953, 52.62529630356036, -5.028205617526908)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574) # set the points to keyframes
ob.location = (0.5983003448964155, 50.684516018273484, -4.868693500660299)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588) # set the points to keyframes
ob.location = (0.5807046671106946, 48.74434002694373, -4.705343850257748)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602) # set the points to keyframes
ob.location = (0.5626968017895706, 46.80478218177514, -4.5381609909097165)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616) # set the points to keyframes
ob.location = (0.5442699917584832, 44.865856605258784, -4.367149382350306)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630) # set the points to keyframes
ob.location = (0.5254309941919928, 42.92757714959872, -4.192313619457025)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645) # set the points to keyframes
ob.location = (0.5061798090900993, 40.98995773457075, -4.013658297107156)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659) # set the points to keyframes
ob.location = (0.48651643644916476, 39.053012212379016, -3.831188145321846)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673) # set the points to keyframes
ob.location = (0.4664341190982668, 37.11675443522758, -3.644908096837014)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687) # set the points to keyframes
ob.location = (0.4250396789648221, 33.24635698428813, -3.260937837997915)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716) # set the points to keyframes
ob.location = (0.3880103724113724, 29.91082536510619, -2.9176062970954604)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740) # set the points to keyframes
ob.location = (0.3497647747717565, 26.57753192424711, -2.563011833117514)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765) # set the points to keyframes
ob.location = (0.31029612886413815, 23.24654626058998, -2.1971821504722584)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789) # set the points to keyframes
ob.location = (0.26960443469215534, 19.9179372297249, -1.8201458995720259)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814) # set the points to keyframes
ob.location = (0.22769644943036838, 16.591773416955064, -1.431932203831309)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838) # set the points to keyframes
ob.location = (0.18457893024969962, 13.268122934581584, -1.0325712678121022)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863) # set the points to keyframes
ob.location = (0.14024511997558875, 9.94705348947518, -0.6220938366504924)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887) # set the points to keyframes
ob.location = (0.09470177578987204, 6.628632112789333, -0.200531601486432)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912) # set the points to keyframes
ob.location = (0.04795565485255793, 3.3129255653906, 0.23208313839433004)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936) # set the points to keyframes
ob.location = (0.0, 0.0, 0.6757172728455316)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961) # set the points to keyframes
ob.location = (-0.04915843160051736, -3.310078971235299, 1.1303368808599998)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985) # set the points to keyframes
ob.location = (-0.09951963994535618, -6.617246074026724, 1.5959075684285722)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010) # set the points to keyframes
ob.location = (-0.15107011069267173, -9.921436777374737, 2.072393927966118)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034) # set the points to keyframes
ob.location = (-0.2038166010097484, -13.222587023281818, 2.5597600788855175)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059) # set the points to keyframes
ob.location = (-0.2577591108965862, -16.52063315918093, 3.0579693297387394)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083) # set the points to keyframes
ob.location = (-0.31288412601861637, -19.81551234336564, 3.5669843809324675)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108) # set the points to keyframes
ob.location = (-0.3691984035358473, -23.10716200441655, 4.086767257155941)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132) # set the points to keyframes
ob.location = (-0.4266951862809947, -26.39552031420311, 4.617279307381011)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157) # set the points to keyframes
ob.location = (-0.4853677170831361, -29.68052598516877, 5.158481272434187)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181) # set the points to keyframes
ob.location = (-0.5452227531131939, -32.96211840547406, 5.7103331498530565)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206) # set the points to keyframes
ob.location = (-0.6062467800256854, -36.24023736871001, 6.272794464172819)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230) # set the points to keyframes
ob.location = (-0.6684397978169727, -39.51482341175668, 6.845824064211627)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255) # set the points to keyframes
ob.location = (-0.7318018064906937, -42.785817612067746, 7.429380258213769)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279) # set the points to keyframes
ob.location = (-0.7963328060504864, -46.05316165524263, 8.023420678706202)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304) # set the points to keyframes
ob.location = (-0.8620192821435921, -49.31679790259798, 8.62790241764202)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328) # set the points to keyframes
ob.location = (-0.9968586639406567, -55.83271949584426, 9.86801565093117)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377) # set the points to keyframes
ob.location = (-1.1081222700850049, -61.03805392282993, 10.89055927441433)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416) # set the points to keyframes
ob.location = (-1.2107704810041469, -65.71417023059627, 11.833292782082253)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452) # set the points to keyframes
ob.location = (-1.315790459553682, -70.38189142696473, 12.797146711545508)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487) # set the points to keyframes
ob.location = (-1.4231619342026534, -75.04105892109143, 13.781981392043633)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522) # set the points to keyframes
ob.location = (-1.532871390623768, -79.69151675742984, 14.787655193236333)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557) # set the points to keyframes
ob.location = (-1.6449255859806726, -84.33311215630448, 15.814024592774786)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593) # set the points to keyframes
ob.location = (-1.7592907344114792, -88.96569497333738, 16.860944041158575)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628) # set the points to keyframes
ob.location = (-1.8759735930871102, -93.58911803730649, 17.928266434737225)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663) # set the points to keyframes
ob.location = (-1.9949538904838846, -98.20323721771751, 19.01584284542389)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699) # set the points to keyframes
ob.location = (-2.1162181122708716, -102.80791115451694, 20.123522790981895)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734) # set the points to keyframes
ob.location = (-2.239759501266235, -107.40300152837912, 21.251154167453194)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769) # set the points to keyframes
ob.location = (-2.365564543120854, -111.98837306070592, 22.39858338430139)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805) # set the points to keyframes
ob.location = (-2.49361972350016, -116.56389317576847, 23.565655567127976)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840) # set the points to keyframes
ob.location = (-2.6239182852186786, -121.12943260885243, 24.752214490099107)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875) # set the points to keyframes
ob.location = (-2.7564399567745568, -125.68486473054094, 25.958102643518657)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910) # set the points to keyframes
ob.location = (-2.891177980978682, -130.2300661548599, 27.183161368971014)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946) # set the points to keyframes
ob.location = (-3.0281120863219257, -134.76491640141955, 28.427230926892662)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981) # set the points to keyframes
ob.location = (-3.1672422728006495, -139.2892978278428, 29.690150496572812)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016) # set the points to keyframes
ob.location = (-3.308541511727526, -143.80309590005186, 30.971758446439765)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052) # set the points to keyframes
ob.location = (-3.4520098030916415, -148.3061989895532, 32.27189206377432)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087) # set the points to keyframes
ob.location = (-3.5976268753947807, -152.7984983734376, 33.59038802771147)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122) # set the points to keyframes
ob.location = (-3.7453859714478313, -157.2798884370954, 34.92708220652571)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158) # set the points to keyframes
ob.location = (-3.8952668197380262, -161.75026633635764, 36.28180965763073)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193) # set the points to keyframes
ob.location = (-4.047255905916245, -166.20953220021136, 37.65440503300994)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228) # set the points to keyframes
ob.location = (-4.201346472815203, -170.65758899565617, 39.04470237650111)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264) # set the points to keyframes
ob.location = (-4.35752500608578, -175.09434273041956, 40.4525351913681)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299) # set the points to keyframes
ob.location = (-4.515771234218846, -179.51970204752615, 41.87773671058795)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334) # set the points to keyframes
ob.location = (-4.67607840002529, -183.93357869829998, 43.32013969413583)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369) # set the points to keyframes
ob.location = (-4.83843298917418, -188.3358870693628, 44.77957663169968)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405) # set the points to keyframes
ob.location = (-5.002821487312758, -192.72654445292028, 46.2558798778241)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440) # set the points to keyframes
ob.location = (-5.169223622924619, -197.10547084404732, 47.748881516766744)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475) # set the points to keyframes
ob.location = (-5.337639396006125, -201.47258914340313, 49.25841356521374)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511) # set the points to keyframes
ob.location = (-5.508041777877224, -205.8278248869442, 50.78430797227918)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546) # set the points to keyframes
ob.location = (-5.680424011348805, -210.17110624592448, 52.326396822220886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581) # set the points to keyframes
ob.location = (-5.8547658249190135, -214.50236422961035, 53.88451199658118)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617) # set the points to keyframes
ob.location = (-6.031067218573298, -218.82153234742225, 55.45848564718972)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652) # set the points to keyframes
ob.location = (-6.209307920806168, -223.12854694679294, 57.04815019616274)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687) # set the points to keyframes
ob.location = (-6.389467660090304, -227.42334674016593, 58.653337998044776)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722) # set the points to keyframes
ob.location = (-6.5715396792584215, -231.7058732104253, 60.27388188038253)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758) # set the points to keyframes
ob.location = (-6.75551722113596, -235.9760702054657, 61.9096148734381)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793) # set the points to keyframes
ob.location = (-6.941373257035593, -240.23388414090738, 63.56037048047517)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828) # set the points to keyframes
ob.location = (-7.129101029779122, -244.47926393252447, 65.22598240747311)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864) # set the points to keyframes
ob.location = (-7.318687025013787, -248.71216092867343, 66.90628490098476)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899) # set the points to keyframes
ob.location = (-7.510117728412297, -252.93252891029272, 68.60111281570863)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934) # set the points to keyframes
ob.location = (-7.70337962562553, -257.1403238881879, 70.31030127663036)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970) # set the points to keyframes
ob.location = (-7.898459202289814, -261.33550437331854, 72.03368621959578)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005) # set the points to keyframes
ob.location = (-8.095342944077856, -265.51803103893906, 73.7711039183099)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040) # set the points to keyframes
ob.location = (-8.294017336644174, -269.6878669908867, 75.52239145733819)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075) # set the points to keyframes
ob.location = (-8.494468865628733, -273.84497749729326, 77.28738659696336)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111) # set the points to keyframes
ob.location = (-8.696690773871524, -277.9893298534431, 79.06592770561389)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146) # set the points to keyframes
ob.location = (-8.90065603268522, -282.12089378720225, 80.85785389500694)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181) # set the points to keyframes
ob.location = (-9.106364642058907, -286.239640986017, 82.66300515529247)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217) # set the points to keyframes
ob.location = (-9.313796330472542, -290.3455452320571, 84.48122215233741)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252) # set the points to keyframes
ob.location = (-9.522944340762479, -294.4385824022159, 86.31234643044144)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287) # set the points to keyframes
ob.location = (-9.733788401412312, -298.51873040053863, 88.15622034476456)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323) # set the points to keyframes
ob.location = (-9.946321755240206, -302.58596902307886, 90.01268726404317)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358) # set the points to keyframes
ob.location = (-10.160524130733393, -306.64028009304195, 91.88159123273056)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393) # set the points to keyframes
ob.location = (-10.376388770717313, -310.68164732564156, 93.76277737642783)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428) # set the points to keyframes
ob.location = (-10.593902160846483, -314.7100561929565, 95.65609183431184)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464) # set the points to keyframes
ob.location = (-11.033827891333203, -322.72795051794657, 99.47849457393914)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534) # set the points to keyframes
ob.location = (-11.368321455734986, -328.7131024099716, 102.37884636290084)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587) # set the points to keyframes
ob.location = (-11.706409836031526, -334.66894493014354, 105.30500582166428)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640) # set the points to keyframes
ob.location = (-12.048045732008177, -340.5954631451105, 108.25647183830006)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693) # set the points to keyframes
ob.location = (-12.393181843461207, -346.49265043284345, 111.2327485039014)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746) # set the points to keyframes
ob.location = (-12.74177762735053, -352.36050794206153, 114.23334578830236)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799) # set the points to keyframes
ob.location = (-13.093792540639697, -358.1990445922329, 117.2577792697897)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852) # set the points to keyframes
ob.location = (-13.449192797463184, -364.0082766681438, 120.30557027024759)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905) # set the points to keyframes
ob.location = (-13.807931097617256, -369.78822761718357, 123.37624605787181)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958) # set the points to keyframes
ob.location = (-14.16997365523639, -375.53892784662946, 126.46933984717032)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011) # set the points to keyframes
ob.location = (-14.535273170105938, -381.2604142506446, 129.58439086653476)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064) # set the points to keyframes
ob.location = (-14.90379585637129, -386.95273021027793, 132.7209443582405)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117) # set the points to keyframes
ob.location = (-15.275501170992356, -392.61592532317724, 135.87855164601837)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170) # set the points to keyframes
ob.location = (-15.650355328107253, -398.2500549305873, 139.05677033776988)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223) # set the points to keyframes
ob.location = (-16.028311027494055, -403.85518018492104, 142.2551640552802)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276) # set the points to keyframes
ob.location = (-16.40934124047999, -409.4313675767583, 145.47330263693334)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329) # set the points to keyframes
ob.location = (-16.79340542402133, -414.97868886727343, 148.7107621377125)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382) # set the points to keyframes
ob.location = (-17.18046303508163, -420.49722088552033, 151.967124964343)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436) # set the points to keyframes
ob.location = (-17.570480287795363, -425.98704512300213, 155.24197940229038)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489) # set the points to keyframes
ob.location = (-17.963423396297003, -431.44824766609935, 158.53492035904964)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542) # set the points to keyframes
ob.location = (-18.35925857473194, -436.8809190609266, 161.84554848571253)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595) # set the points to keyframes
ob.location = (-18.75794528005281, -442.28515390790204, 165.17347105540023)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648) # set the points to keyframes
ob.location = (-19.159449726405, -447.6610507941759, 168.5183012199737)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701) # set the points to keyframes
ob.location = (-19.56373812792299, -453.00871229363, 171.8796583478935)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754) # set the points to keyframes
ob.location = (-19.970783455915807, -458.32824422358925, 175.257167889075)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807) # set the points to keyframes
ob.location = (-20.380551924507017, -463.6197561853956, 178.65046144246122)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860) # set the points to keyframes
ob.location = (-20.793002990685636, -468.88336068597465, 182.05917648573524)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913) # set the points to keyframes
ob.location = (-21.20810286856795, -474.1191735432672, 185.48295671317908)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966) # set the points to keyframes
ob.location = (-21.62583128663391, -479.32731348079807, 188.92145163024327)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019) # set the points to keyframes
ob.location = (-22.046147701868904, -484.5079018573894, 192.37431662111874)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072) # set the points to keyframes
ob.location = (-22.469025085560133, -489.6610626671605, 195.8412130163083)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125) # set the points to keyframes
ob.location = (-22.894429651852988, -494.7869223368136, 199.3218078899116)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178) # set the points to keyframes
ob.location = (-23.322334372052865, -499.8856097256324, 202.8157741271969)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231) # set the points to keyframes
ob.location = (-23.75270546030515, -504.9572557200524, 206.32279028945743)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284) # set the points to keyframes
ob.location = (-24.185515887900692, -510.0019931660899, 209.84254047886833)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337) # set the points to keyframes
ob.location = (-24.62073862617399, -515.019957004484, 213.3747144060576)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390) # set the points to keyframes
ob.location = (-25.058346646412247, -520.0112837976958, 216.9190074576789)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443) # set the points to keyframes
ob.location = (-25.498299405589933, -524.9761118650506, 220.47512029098021)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496) # set the points to keyframes
ob.location = (-25.940583389357926, -529.9145808097373, 224.0427589013762)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549) # set the points to keyframes
ob.location = (-26.3851648118507, -534.8268319918095, 227.62163482516308)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602) # set the points to keyframes
ob.location = (-26.832016644384566, -539.7130078524681, 231.21146466651658)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655) # set the points to keyframes
ob.location = (-27.28111185826492, -544.5732517789183, 234.81197023263553)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708) # set the points to keyframes
ob.location = (-27.7324234248008, -549.4077095233761, 238.42287846616995)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761) # set the points to keyframes
ob.location = (-28.18593107246852, -554.2165251487638, 242.04392137764938)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814) # set the points to keyframes
ob.location = (-28.641601015409833, -558.9998473128817, 245.67483597791124)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867) # set the points to keyframes
ob.location = (-29.09941298211197, -563.7578219706606, 249.3153640078139)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920) # set the points to keyframes
ob.location = (-29.559339943873056, -568.4905977799006, 252.9652522760952)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973) # set the points to keyframes
ob.location = (-30.02136162918032, -573.1983233984017, 256.6242522539422)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026) # set the points to keyframes
ob.location = (-30.485451009346434, -577.8811488353986, 260.2921199398477)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079) # set the points to keyframes
ob.location = (-30.95158105566952, -582.539223424408, 263.9686162650404)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132) # set the points to keyframes
ob.location = (-31.41973149664409, -587.1726971746652, 267.6535064177682)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185) # set the points to keyframes
ob.location = (-31.8898753035719, -591.7817214468391, 271.3465602487279)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238) # set the points to keyframes
ob.location = (-32.361998962107464, -596.3664462501638, 275.0475520007786)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291) # set the points to keyframes
ob.location = (-32.83606868639254, -600.9270236210266, 278.75626010622653)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344) # set the points to keyframes
ob.location = (-33.31207096207436, -605.4636035686613, 282.47246738954027)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397) # set the points to keyframes
ob.location = (-33.7899787604656, -609.9763381294547, 286.1959609322071)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450) # set the points to keyframes
ob.location = (-34.26977856722442, -614.4653786640754, 289.9265317348745)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503) # set the points to keyframes
ob.location = (-34.75143659648165, -618.9308751817587, 293.6639749876372)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556) # set the points to keyframes
ob.location = (-35.234939333891816, -623.3729803946078, 297.40808986732145)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609) # set the points to keyframes
ob.location = (-35.720266507938504, -627.7918443118574, 301.1586794699142)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662) # set the points to keyframes
ob.location = (-36.20739784710895, -632.1876182941764, 304.9155507429905)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715) # set the points to keyframes
ob.location = (-36.696306322701275, -636.5604516750825, 308.6785143505705)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768) # set the points to keyframes
ob.location = (-37.18698517755183, -640.9104964909618, 312.44738474069106)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821) # set the points to keyframes
ob.location = (-37.6794006257951, -645.2379013996147, 316.22198007783413)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874) # set the points to keyframes
ob.location = (-38.17354591024923, -649.5428164102752, 320.00212197263954)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927) # set the points to keyframes
ob.location = (-38.66938724506326, -653.8253908564609, 323.787635617049)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980) # set the points to keyframes
ob.location = (-39.16692463023719, -658.0857727202547, 327.5783497167336)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033) # set the points to keyframes
ob.location = (-39.666131037069135, -662.3241113351735, 331.3740963559512)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086) # set the points to keyframes
ob.location = (-40.16697943687177, -666.5405540075831, 335.17471086240243)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139) # set the points to keyframes
ob.location = (-40.66946982965237, -670.735248043849, 338.98003200994594)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192) # set the points to keyframes
ob.location = (-41.173568429541774, -674.9083393989017, 342.7899016131681)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245) # set the points to keyframes
ob.location = (-41.67926847936906, -679.0599753791075, 346.60416473009843)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298) # set the points to keyframes
ob.location = (-42.186542950439616, -683.1903005879619, 350.4226695270655)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351) # set the points to keyframes
ob.location = (-42.69538508557889, -687.2994596289618, 354.2452672111259)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404) # set the points to keyframes
ob.location = (-43.205774613277754, -691.3875977813209, 358.07181196249223)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457) # set the points to keyframes
ob.location = (-43.71769126201252, -695.4548576213836, 361.90216079938943)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510) # set the points to keyframes
ob.location = (-44.231128274608636, -699.5013824012115, 365.7361737807704)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563) # set the points to keyframes
ob.location = (-44.74605862238241, -703.5273133457151, 369.5737136008853)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616) # set the points to keyframes
ob.location = (-45.26247554815927, -707.5327930312387, 373.4146458595685)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669) # set the points to keyframes
ob.location = (-45.780358780411916, -711.517961331258, 377.2588388595235)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722) # set the points to keyframes
ob.location = (-46.29969480480213, -715.4829574435307, 381.1061634036078)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775) # set the points to keyframes
ob.location = (-46.82046334981351, -719.4279212415329, 384.9564931326913)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828) # set the points to keyframes
ob.location = (-47.342657658264216, -723.3529912473052, 388.80970405265464)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881) # set the points to keyframes
ob.location = (-47.86626421581968, -727.2583032800194, 392.6656747371036)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934) # set the points to keyframes
ob.location = (-48.391255993788945, -731.1439951859993, 396.5242862597983)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987) # set the points to keyframes
ob.location = (-48.91763299216109, -735.0102021086993, 400.385422127081)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040) # set the points to keyframes
ob.location = (-49.44536818225242, -738.8570591915745, 404.24896807516063)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093) # set the points to keyframes
ob.location = (-49.97446156406659, -742.6847002266443, 408.1148122728287)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146) # set the points to keyframes
ob.location = (-50.5048861089017, -746.4932583302116, 411.98284518631533)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199) # set the points to keyframes
ob.location = (-51.03664181676868, -750.2828659428619, 415.85295937657395)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252) # set the points to keyframes
ob.location = (-51.56970165896564, -754.0536541537463, 419.7250497695687)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305) # set the points to keyframes
ob.location = (-52.10406563550714, -757.8057540520156, 423.5990132508439)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358) # set the points to keyframes
ob.location = (-52.63971347486586, -761.539294699669, 427.4747488682388)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411) # set the points to keyframes
ob.location = (-53.17663166269267, -765.2544044829881, 431.35215783188823)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464) # set the points to keyframes
ob.location = (-53.71480668465301, -768.951211788255, 435.2311431763634)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517) # set the points to keyframes
ob.location = (-54.25423178356505, -772.6298429745996, 439.11161009853106)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570) # set the points to keyframes
ob.location = (-54.79489344508693, -776.2904250768693, 442.99346565348026)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623) # set the points to keyframes
ob.location = (-55.33677815487681, -779.9330817513248, 446.87661877479417)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676) # set the points to keyframes
ob.location = (-55.87987239858194, -783.557938681379, 450.7609802880643)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729) # set the points to keyframes
ob.location = (-56.42416941902775, -787.1651174961412, 454.64646283656157)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782) # set the points to keyframes
ob.location = (-56.969648944701476, -790.7547418518718, 458.5329808542077)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835) # set the points to keyframes
ob.location = (-57.516310975610395, -794.3269320262461, 462.4204505250323)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888) # set the points to keyframes
ob.location = (-58.06413524022355, -797.8818096483728, 466.3087897561438)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941) # set the points to keyframes
ob.location = (-58.613114981377294, -801.4194936444917, 470.1979181371867)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994) # set the points to keyframes
ob.location = (-59.16323668471523, -804.9401022651259, 474.08775690655574)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047) # set the points to keyframes
ob.location = (-59.71448683590643, -808.4437544365155, 477.9782289108527)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100) # set the points to keyframes
ob.location = (-60.266865434936335, -811.9305657063137, 481.8692586048864)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153) # set the points to keyframes
ob.location = (-60.82035221029582, -815.4006529736091, 485.76077197734406)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206) # set the points to keyframes
ob.location = (-61.37494040481397, -818.8541304346206, 489.65269654403374)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259) # set the points to keyframes
ob.location = (-61.93062326130894, -822.2911129612849, 493.5449613208559)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312) # set the points to keyframes
ob.location = (-62.4873805082716, -825.7117133983861, 497.4374967765033)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365) # set the points to keyframes
ob.location = (-63.045212145698315, -829.1160432392744, 501.3302348189462)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418) # set the points to keyframes
ob.location = (-63.604104659239965, -832.5042153287345, 505.22310875083565)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471) # set the points to keyframes
ob.location = (-64.16404453455107, -835.8763391329646, 509.11605326146173)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524) # set the points to keyframes
ob.location = (-64.72502501447161, -839.2325241181629, 513.0090043851976)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577) # set the points to keyframes
ob.location = (-65.28704609898705, -842.5728797505274, 516.9018994785248)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630) # set the points to keyframes
ob.location = (-65.85008075941732, -845.8975134691045, 520.7946771923289)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683) # set the points to keyframes
ob.location = (-66.41412899574789, -849.2065320372235, 524.6872774425055)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736) # set the points to keyframes
ob.location = (-66.97918405082237, -852.5000408667788, 528.579641401109)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789) # set the points to keyframes
ob.location = (-67.54523916745529, -855.7781467210998, 532.4717114429704)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842) # set the points to keyframes
ob.location = (-68.1122740741339, -859.0409529849289, 536.3634311592116)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895) # set the points to keyframes
ob.location = (-68.68028877086181, -862.2885623672919, 540.254745282917)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948) # set the points to keyframes
ob.location = (-69.24926974328991, -865.5210789286484, 544.1455997296761)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10001) # set the points to keyframes
ob.location = (-69.81921023424366, -868.7386026751549, 548.035941502983)
ob.keyframe_insert(data_path="location", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Point = bpy.data.objects['Point']
Point.select_set(True)
bpy.ops.transform.resize(value=(RATIO*3, RATIO*3, RATIO*3))

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Point = bpy.data.objects['Point']
Point.name = 'OrionPoint'

ob = bpy.data.objects['Orion']
ob.rotation_mode = 'QUATERNION' # Set all the points and the attitude for each satellite
bpy.context.scene.frame_set(0)
ob.location = (0.0, 0.0, 1.7568649093983595)
ob.keyframe_insert(data_path="location", index =-1)

ob.location = (0.0, 0.0, 1.7568649093983595)
ob.keyframe_insert(data_path="location", index =-1)

ob.location = (0.0, 0.0, 1.7568649093983595)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
ob.location = (0.0, 0.0, 1.7163218730275958)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
ob.location = (0.0, 0.0, 1.7028075275707124)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2.0)
ob.location = (0.0, 0.0, 1.689293182113829)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4.0)
ob.location = (0.0, 0.0, 1.6757788366569457)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6.0)
ob.location = (0.0, 0.0, 1.6622644911999487)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8.0)
ob.location = (0.0, 0.0, 1.6487501457430653)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10.0)
ob.location = (0.0, 0.0, 1.6352358002860683)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12.0)
ob.location = (0.0, 0.0, 1.6217214548292986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16.0)
ob.location = (0.0, 0.0, 1.6082071093723016)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20.0)
ob.location = (0.0, 0.0, 1.5946927639154183)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24.0)
ob.location = (0.0, 0.0, 1.5811784184584212)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28.0)
ob.location = (0.0, 0.0, 1.5676640730016516)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32.0)
ob.location = (0.0, 0.0, 1.5541497275446545)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36.0)
ob.location = (0.0, 0.0, 1.5406353820877712)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44.0)
ob.location = (0.0, 0.0, 1.5271210366307741)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52.0)
ob.location = (0.0, 0.0, 1.5136066911740045)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60.0)
ob.location = (0.0, 0.0, 1.5000923457171211)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68.0)
ob.location = (0.0, 0.0, 1.486578000260124)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76.0)
ob.location = (0.0, 0.0, 1.4730636548032408)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84.0)
ob.location = (0.0, 0.0, 1.4595493093462437)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92.0)
ob.location = (0.0, 0.0, 1.446034963889474)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100.0)
ob.location = (0.0, 0.0, 1.432520618432477)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108.0)
ob.location = (0.0, 0.0, 1.41900627297548)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116.0)
ob.location = (0.0, 0.0, 1.4054919275187103)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124.0)
ob.location = (0.0, 0.0, 1.3919775820617133)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132.0)
ob.location = (0.0, 0.0, 1.37846323660483)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140.0)
ob.location = (0.0, 0.0, 1.3649488911479466)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148.0)
ob.location = (0.0, 0.0, 1.3514345456910632)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156.0)
ob.location = (0.0, 0.0, 1.3379202002340662)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164.0)
ob.location = (0.0, 0.0, 1.3244058547771829)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172.0)
ob.location = (0.0, 0.0, 1.3108915093202995)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180.0)
ob.location = (0.0, 0.0, 1.2973771638634162)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188.0)
ob.location = (0.0, 0.0, 1.2838628184065328)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204.0)
ob.location = (0.0, 0.0, 1.2703484729495358)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218.0)
ob.location = (0.0, 0.0, 1.2568341274926524)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233.0)
ob.location = (0.0, 0.0, 1.2433197820356554)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247.0)
ob.location = (0.0, 0.0, 1.229805436578772)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261.0)
ob.location = (0.0, 0.0, 1.2162910911218887)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275.0)
ob.location = (0.0, 0.0, 1.2027767456650054)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289.0)
ob.location = (0.0, 0.0, 1.1892624002082357)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304.0)
ob.location = (0.0, 0.0, 1.175748054751125)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318.0)
ob.location = (0.0, 0.0, 1.1622337092943553)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332.0)
ob.location = (0.0, 0.0, 1.1487193638373583)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346.0)
ob.location = (0.0, 0.0, 1.135205018380475)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360.0)
ob.location = (0.0, 0.0, 1.121690672923478)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375.0)
ob.location = (0.0, 0.0, 1.1081763274665946)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389.0)
ob.location = (0.0, 0.0, 1.0946619820097112)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403.0)
ob.location = (0.0, 0.0, 1.0811476365528279)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417.0)
ob.location = (0.0, 0.0, 1.0676332910958308)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432.0)
ob.location = (0.0, 0.0, 1.0541189456389475)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446.0)
ob.location = (0.0, 0.0, 1.0406046001820641)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460.0)
ob.location = (0.0, 0.0, 1.0270902547252945)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474.0)
ob.location = (0.0, 0.0, 1.0135759092681838)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488.0)
ob.location = (0.0, 0.0, 1.000061563811414)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503.0)
ob.location = (0.0, 0.0, 0.9865472183545307)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517.0)
ob.location = (0.0, 0.0, 0.9730328728975337)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531.0)
ob.location = (0.0, 0.0, 0.9595185274406504)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545.0)
ob.location = (0.0, 0.0, 0.9460041819836533)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559.0)
ob.location = (0.0, 0.0, 0.93248983652677)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574.0)
ob.location = (0.0, 0.0, 0.9189754910697729)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588.0)
ob.location = (0.0, 0.0, 0.9054611456128896)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602.0)
ob.location = (0.0, 0.0, 0.8919468001560062)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616.0)
ob.location = (0.0, 0.0, 0.8784324546991229)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630.0)
ob.location = (0.0, 0.0, 0.8649181092422396)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645.0)
ob.location = (0.0, 0.0, 0.8514037637854699)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659.0)
ob.location = (0.0, 0.0, 0.8378894183283592)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673.0)
ob.location = (0.0, 0.0, 0.8243750728714758)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687.0)
ob.location = (0.0, 0.0, 0.8108607274145925)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716.0)
ob.location = (0.0, 0.0, 0.7973463819577091)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740.0)
ob.location = (0.0, 0.0, 0.7838320365008258)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765.0)
ob.location = (0.0, 0.0, 0.7703176910438856)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789.0)
ob.location = (0.0, 0.0, 0.7568033455868886)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814.0)
ob.location = (0.0, 0.0, 0.743289000130062)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838.0)
ob.location = (0.0, 0.0, 0.7297746546731219)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863.0)
ob.location = (0.0, 0.0, 0.7162603092161817)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887.0)
ob.location = (0.0, 0.0, 0.7027459637592983)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912.0)
ob.location = (0.0, 0.0, 0.689231618302415)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936.0)
ob.location = (0.0, 0.0, 0.6757172728455316)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961.0)
ob.location = (0.0, 0.0, 0.6689601001170331)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985.0)
ob.location = (0.0, 0.0, 0.6622029273886483)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010.0)
ob.location = (0.0, 0.0, 0.6554457546601498)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034.0)
ob.location = (0.0, 0.0, 0.6486885819317649)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059.0)
ob.location = (0.0, 0.0, 0.6419314092032096)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083.0)
ob.location = (0.0, 0.0, 0.6351742364747679)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108.0)
ob.location = (0.0, 0.0, 0.6284170637463831)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132.0)
ob.location = (0.0, 0.0, 0.6216598910179414)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157.0)
ob.location = (0.0, 0.0, 0.6149027182893292)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181.0)
ob.location = (0.0, 0.0, 0.6081455455609444)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206.0)
ob.location = (0.0, 0.0, 0.6013883728325027)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230.0)
ob.location = (0.0, 0.0, 0.5946312001040042)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255.0)
ob.location = (0.0, 0.0, 0.5878740273756762)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279.0)
ob.location = (0.0, 0.0, 0.5811168546472345)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304.0)
ob.location = (0.0, 0.0, 0.5743596819186791)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328.0)
ob.location = (0.0, 0.0, 0.5676025091901806)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377.0)
ob.location = (0.0, 0.0, 0.5608453364617958)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416.0)
ob.location = (0.0, 0.0, 0.5540881637333541)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452.0)
ob.location = (0.0, 0.0, 0.5473309910049124)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487.0)
ob.location = (0.0, 0.0, 0.5405738182764139)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522.0)
ob.location = (0.0, 0.0, 0.5338166455479723)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557.0)
ob.location = (0.0, 0.0, 0.5270594728194169)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593.0)
ob.location = (0.0, 0.0, 0.5203023000910321)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628.0)
ob.location = (0.0, 0.0, 0.5135451273625335)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663.0)
ob.location = (0.0, 0.0, 0.5067879546340919)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699.0)
ob.location = (0.0, 0.0, 0.5000307819056502)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734.0)
ob.location = (0.0, 0.0, 0.49327360917726537)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769.0)
ob.location = (0.0, 0.0, 0.48651643644871)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805.0)
ob.location = (0.0, 0.0, 0.479759263720382)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840.0)
ob.location = (0.0, 0.0, 0.47300209099182666)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875.0)
ob.location = (0.0, 0.0, 0.46624491826344183)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910.0)
ob.location = (0.0, 0.0, 0.459487745535057)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946.0)
ob.location = (0.0, 0.0, 0.45273057280650164)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981.0)
ob.location = (0.0, 0.0, 0.44597340007805997)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016.0)
ob.location = (0.0, 0.0, 0.43921622734956145)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052.0)
ob.location = (0.0, 0.0, 0.4324590546211198)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087.0)
ob.location = (0.0, 0.0, 0.42570188189273495)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122.0)
ob.location = (0.0, 0.0, 0.41894470916423643)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158.0)
ob.location = (0.0, 0.0, 0.4121875364357379)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193.0)
ob.location = (0.0, 0.0, 0.40543036370729624)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228.0)
ob.location = (0.0, 0.0, 0.3986731909789114)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264.0)
ob.location = (0.0, 0.0, 0.39191601825046973)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299.0)
ob.location = (0.0, 0.0, 0.3851588455219712)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334.0)
ob.location = (0.0, 0.0, 0.3784016727935864)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369.0)
ob.location = (0.0, 0.0, 0.371644500065031)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405.0)
ob.location = (0.0, 0.0, 0.3648873273366462)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440.0)
ob.location = (0.0, 0.0, 0.3581301546081477)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475.0)
ob.location = (0.0, 0.0, 0.351372981879706)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511.0)
ob.location = (0.0, 0.0, 0.34461580915115064)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546.0)
ob.location = (0.0, 0.0, 0.3378586364227658)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581.0)
ob.location = (0.0, 0.0, 0.3311014636942673)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617.0)
ob.location = (0.0, 0.0, 0.3243442909658256)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652.0)
ob.location = (0.0, 0.0, 0.31758711823738395)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687.0)
ob.location = (0.0, 0.0, 0.3108299455089991)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722.0)
ob.location = (0.0, 0.0, 0.3040727727805006)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758.0)
ob.location = (0.0, 0.0, 0.29731560005211577)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793.0)
ob.location = (0.0, 0.0, 0.2905584273235604)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828.0)
ob.location = (0.0, 0.0, 0.2838012545950619)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864.0)
ob.location = (0.0, 0.0, 0.2770440818666202)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899.0)
ob.location = (0.0, 0.0, 0.27028690913817854)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934.0)
ob.location = (0.0, 0.0, 0.26690832277398613)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970.0)
ob.location = (0.0, 0.0, 0.26352973640973687)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005.0)
ob.location = (0.0, 0.0, 0.2601511500454876)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040.0)
ob.location = (0.0, 0.0, 0.25677256368135204)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075.0)
ob.location = (0.0, 0.0, 0.25339397731704594)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111.0)
ob.location = (0.0, 0.0, 0.25001539095291037)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146.0)
ob.location = (0.0, 0.0, 0.24663680458860426)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181.0)
ob.location = (0.0, 0.0, 0.24325821822441185)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217.0)
ob.location = (0.0, 0.0, 0.2398796318601626)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252.0)
ob.location = (0.0, 0.0, 0.23650104549597017)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287.0)
ob.location = (0.0, 0.0, 0.23312245913166407)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323.0)
ob.location = (0.0, 0.0, 0.2297438727675285)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358.0)
ob.location = (0.0, 0.0, 0.22636528640333609)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393.0)
ob.location = (0.0, 0.0, 0.22298670003908683)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428.0)
ob.location = (0.0, 0.0, 0.21960811367478072)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464.0)
ob.location = (0.0, 0.0, 0.2162295273105883)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534.0)
ob.location = (0.0, 0.0, 0.21285094094633905)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587.0)
ob.location = (0.0, 0.0, 0.2094723545820898)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640.0)
ob.location = (0.0, 0.0, 0.20609376821789738)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693.0)
ob.location = (0.0, 0.0, 0.20271518185364812)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746.0)
ob.location = (0.0, 0.0, 0.1993365954894557)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799.0)
ob.location = (0.0, 0.0, 0.1959580091251496)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852.0)
ob.location = (0.0, 0.0, 0.1925794227609572)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905.0)
ob.location = (0.0, 0.0, 0.18920083639676477)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958.0)
ob.location = (0.0, 0.0, 0.1858222500325155)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011.0)
ob.location = (0.0, 0.0, 0.1824436636683231)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064.0)
ob.location = (0.0, 0.0, 0.17906507730407384)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117.0)
ob.location = (0.0, 0.0, 0.17568649093982458)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170.0)
ob.location = (0.0, 0.0, 0.17230790457557532)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223.0)
ob.location = (0.0, 0.0, 0.1689293182113829)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276.0)
ob.location = (0.0, 0.0, 0.1655507318470768)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329.0)
ob.location = (0.0, 0.0, 0.1621721454828844)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382.0)
ob.location = (0.0, 0.0, 0.15879355911874882)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436.0)
ob.location = (0.0, 0.0, 0.1554149727545564)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489.0)
ob.location = (0.0, 0.0, 0.1520363863902503)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542.0)
ob.location = (0.0, 0.0, 0.1486578000259442)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595.0)
ob.location = (0.0, 0.0, 0.14527921366186547)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648.0)
ob.location = (0.0, 0.0, 0.14190062729750252)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701.0)
ob.location = (0.0, 0.0, 0.13852204093336695)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754.0)
ob.location = (0.0, 0.0, 0.1351434545691177)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807.0)
ob.location = (0.0, 0.0, 0.13176486820486843)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860.0)
ob.location = (0.0, 0.0, 0.12838628184067602)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913.0)
ob.location = (0.0, 0.0, 0.12500769547642676)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966.0)
ob.location = (0.0, 0.0, 0.1216291091121775)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019.0)
ob.location = (0.0, 0.0, 0.11825052274792824)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072.0)
ob.location = (0.0, 0.0, 0.11487193638373583)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125.0)
ob.location = (0.0, 0.0, 0.11149335001954341)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178.0)
ob.location = (0.0, 0.0, 0.108114763655351)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231.0)
ob.location = (0.0, 0.0, 0.10473617729098805)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284.0)
ob.location = (0.0, 0.0, 0.10135759092685248)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337.0)
ob.location = (0.0, 0.0, 0.09797900456260322)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390.0)
ob.location = (0.0, 0.0, 0.09460041819841081)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443.0)
ob.location = (0.0, 0.0, 0.09122183183416155)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496.0)
ob.location = (0.0, 0.0, 0.08784324546985545)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549.0)
ob.location = (0.0, 0.0, 0.08446465910566303)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602.0)
ob.location = (0.0, 0.0, 0.08108607274141377)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655.0)
ob.location = (0.0, 0.0, 0.07770748637722136)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708.0)
ob.location = (0.0, 0.0, 0.07432890001302894)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761.0)
ob.location = (0.0, 0.0, 0.07095031364877968)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814.0)
ob.location = (0.0, 0.0, 0.06757172728453043)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867.0)
ob.location = (0.0, 0.0, 0.06554457546599224)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920.0)
ob.location = (0.0, 0.0, 0.06351742364745405)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973.0)
ob.location = (0.0, 0.0, 0.061490271828915866)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026.0)
ob.location = (0.0, 0.0, 0.0594631200104061)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079.0)
ob.location = (0.0, 0.0, 0.057435968191867914)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132.0)
ob.location = (0.0, 0.0, 0.055408816373301306)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185.0)
ob.location = (0.0, 0.0, 0.05338166455479154)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238.0)
ob.location = (0.0, 0.0, 0.05135451273622493)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291.0)
ob.location = (0.0, 0.0, 0.04932736091771517)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344.0)
ob.location = (0.0, 0.0, 0.047300209099233825)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397.0)
ob.location = (0.0, 0.0, 0.04527305728069564)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450.0)
ob.location = (0.0, 0.0, 0.04324590546210061)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503.0)
ob.location = (0.0, 0.0, 0.041218753643619266)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556.0)
ob.location = (0.0, 0.0, 0.03919160182508108)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609.0)
ob.location = (0.0, 0.0, 0.03716445000648605)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662.0)
ob.location = (0.0, 0.0, 0.035137298187976285)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715.0)
ob.location = (0.0, 0.0, 0.033110146369381255)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768.0)
ob.location = (0.0, 0.0, 0.03108299455089991)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821.0)
ob.location = (0.0, 0.0, 0.029055842732390147)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874.0)
ob.location = (0.0, 0.0, 0.02702869091382354)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927.0)
ob.location = (0.0, 0.0, 0.025001539095313774)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980.0)
ob.location = (0.0, 0.0, 0.022974387276775587)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033.0)
ob.location = (0.0, 0.0, 0.020947235458180558)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086.0)
ob.location = (0.0, 0.0, 0.01892008363964237)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139.0)
ob.location = (0.0, 0.0, 0.016892931821132606)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192.0)
ob.location = (0.0, 0.0, 0.01486578000259442)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245.0)
ob.location = (0.0, 0.0, 0.012838628184056233)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298.0)
ob.location = (0.0, 0.0, 0.010811476365546469)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351.0)
ob.location = (0.0, 0.0, 0.00878432454697986)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404.0)
ob.location = (0.0, 0.0, 0.006757172728498517)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457.0)
ob.location = (0.0, 0.0, 0.0060814554556145595)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510.0)
ob.location = (0.0, 0.0, 0.005405738182787445)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563.0)
ob.location = (0.0, 0.0, 0.004730020909875066)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616.0)
ob.location = (0.0, 0.0, 0.004054303637076373)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669.0)
ob.location = (0.0, 0.0, 0.0033785863642492586)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722.0)
ob.location = (0.0, 0.0, 0.002702869091365301)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775.0)
ob.location = (0.0, 0.0, 0.0020271518185097648)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828.0)
ob.location = (0.0, 0.0, 0.0013514345456968613)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881.0)
ob.location = (0.0, 0.0, 0.0006757172728555361)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948.0)
ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

ob.location = (0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0)
ob.rotation_quaternion = (1.0, 0.0, 0.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8.0)
ob.rotation_quaternion = (0.632, 0.316, 0.0, 0.707)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(10.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(12.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(16.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(20.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(24.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(28.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(32.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(36.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(44.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(52.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(60.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(68.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(76.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(84.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(92.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(100.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(108.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(116.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(124.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(132.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(140.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(148.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(156.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(164.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(172.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(180.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(188.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(204.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(218.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(233.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(247.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(261.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(275.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(289.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(304.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(318.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(332.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(346.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(360.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(375.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(389.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(403.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(417.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(432.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(446.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(460.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(474.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(488.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(503.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(517.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(531.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(545.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(559.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(574.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(588.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(602.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(616.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(630.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(645.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(659.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(673.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(687.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(716.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(740.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(765.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(789.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(814.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(838.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(863.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(887.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(912.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(936.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(961.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(985.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1010.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1034.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1059.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1083.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1108.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1132.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1157.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1181.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1206.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1230.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1255.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1279.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1304.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1328.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1377.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1416.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1452.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1487.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1522.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1557.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1593.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1628.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1663.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1699.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1734.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1769.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1805.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1840.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1875.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1910.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1946.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(1981.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2016.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2052.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2087.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2122.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2158.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2193.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2228.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2264.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2299.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2334.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2369.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2405.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2440.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2475.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2511.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2546.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2581.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2617.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2652.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2687.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2722.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2758.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2793.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2828.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2864.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2899.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2934.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(2970.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3005.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3040.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3075.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3111.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3146.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3181.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3217.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3252.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3287.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3323.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3358.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3393.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3428.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3464.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3534.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3587.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3640.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3693.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3746.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3799.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3852.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3905.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(3958.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4011.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4064.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4117.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4170.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4223.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4276.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4329.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4382.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4436.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4489.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4542.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4595.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4648.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4701.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4754.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4807.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4860.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4913.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(4966.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5019.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5072.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5125.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5178.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5231.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5284.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5337.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5390.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5443.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5496.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5549.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5602.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5655.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5708.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5761.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5814.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5867.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5920.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(5973.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6026.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6079.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6132.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6185.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6238.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6291.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6344.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6397.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6450.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6503.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6556.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6609.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6662.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6715.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6768.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6821.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6874.0)
ob.rotation_quaternion = (-0.853, 0.189, -0.24, 0.424)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6927.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(6980.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7033.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7086.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7139.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7192.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7245.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7298.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7351.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7404.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7457.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7510.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7563.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7616.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7669.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7722.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7775.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7828.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7881.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7934.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(7987.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8040.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8093.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8146.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8199.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8252.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8305.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8358.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8411.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8464.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8517.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8570.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8623.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8676.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8729.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8782.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8835.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8888.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8941.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(8994.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9047.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9100.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9153.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9206.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9259.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9312.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9365.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9418.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9471.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9524.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9577.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9630.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9683.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9736.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9789.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9842.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9895.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(9948.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.context.scene.frame_set(10001.0)
ob.rotation_quaternion = (0.0, 0.0, 1.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

print("importing Gateway.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Gateway.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Gateway'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Gateway']
ob.select_set(True)
ob.active_material.use_backface_culling = True
bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Gateway = bpy.data.objects['Gateway']
Gateway.name = 'Gateway'

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Gateway = bpy.data.objects['Gateway']
Gateway.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.context.scene.frame_set(0)  # Calculating the ratio to rescale objects to see 
bpy.ops.object.select_all(action='DESELECT')  # them all in a camera
Gateway = bpy.data.objects['Gateway']
Gateway.select_set(True)
bpy.ops.transform.resize(value=(26.0, 26.0, 26.0))
Gateway.keyframe_insert(data_path="scale", index =-1)
bpy.context.scene.frame_set(5867.0)
bpy.ops.transform.resize(value=(0.038461538461538464, 0.038461538461538464, 0.038461538461538464))
Gateway.keyframe_insert(data_path="scale", index =-1)

ops.curve.primitive_bezier_circle_add(enter_editmode=True)  # draw the trajectory
curve = context.active_object
bez_points = curve.data.splines[0].bezier_points
ops.curve.delete()
bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
ops.curve.vertex_add(location=(379441475.0, 1526.278258, 7662095.166))
ops.curve.vertex_add(location=(379441475.0, 2035.038318, 7662095.103))
ops.curve.vertex_add(location=(379441475.0, 2543.798374, 7662095.021))
ops.curve.vertex_add(location=(379441475.09999996, 3561.3184659999997, 7662094.805))
ops.curve.vertex_add(location=(379441475.09999996, 5596.35854, 7662094.157000001))
ops.curve.vertex_add(location=(379441475.4, 9666.437978, 7662091.996))
ops.curve.vertex_add(location=(379441475.7, 13736.51596, 7662088.682))
ops.curve.vertex_add(location=(379441476.2, 17806.59186, 7662084.215))
ops.curve.vertex_add(location=(379441476.8, 21876.66508, 7662078.595))
ops.curve.vertex_add(location=(379441477.5, 25946.73499, 7662071.823))
ops.curve.vertex_add(location=(379441479.40000004, 34086.86246, 7662054.8209999995))
ops.curve.vertex_add(location=(379441481.7, 42226.96935, 7662033.208))
ops.curve.vertex_add(location=(379441484.5, 50367.050740000006, 7662006.983999999))
ops.curve.vertex_add(location=(379441487.8, 58507.10171, 7661976.149))
ops.curve.vertex_add(location=(379441491.6, 66647.11737, 7661940.704))
ops.curve.vertex_add(location=(379441495.9, 74787.09277999999, 7661900.649))
ops.curve.vertex_add(location=(379441506.0, 91066.90324, 7661806.7069999995))
ops.curve.vertex_add(location=(379441518.09999996, 107346.49380000001, 7661694.323))
ops.curve.vertex_add(location=(379441532.2, 123625.8251, 7661563.5))
ops.curve.vertex_add(location=(379441548.20000005, 139904.8579, 7661414.239))
ops.curve.vertex_add(location=(379441566.2, 156183.5529, 7661246.54))
ops.curve.vertex_add(location=(379441586.20000005, 172461.8707, 7661060.405))
ops.curve.vertex_add(location=(379441608.2, 188739.7722, 7660855.835))
ops.curve.vertex_add(location=(379441632.2, 205017.2181, 7660632.833))
ops.curve.vertex_add(location=(379441658.1, 221294.16900000002, 7660391.4))
ops.curve.vertex_add(location=(379441686.09999996, 237570.5858, 7660131.539))
ops.curve.vertex_add(location=(379441716.0, 253846.42919999998, 7659853.252))
ops.curve.vertex_add(location=(379441747.90000004, 270121.66000000003, 7659556.54))
ops.curve.vertex_add(location=(379441781.7, 286396.239, 7659241.408))
ops.curve.vertex_add(location=(379441817.6, 302670.12690000003, 7658907.857))
ops.curve.vertex_add(location=(379441855.4, 318943.2846, 7658555.89))
ops.curve.vertex_add(location=(379441895.20000005, 335215.673, 7658185.510000001))
ops.curve.vertex_add(location=(379441937.0, 351487.2528, 7657796.721))
ops.curve.vertex_add(location=(379441980.8, 367757.98500000004, 7657389.526))
ops.curve.vertex_add(location=(379442026.5, 384027.83040000004, 7656963.929))
ops.curve.vertex_add(location=(379442124.0, 416564.7044, 7656057.541))
ops.curve.vertex_add(location=(379442217.1, 445444.52280000004, 7655191.2979999995))
ops.curve.vertex_add(location=(379442316.40000004, 474320.95800000004, 7654267.109))
ops.curve.vertex_add(location=(379442422.0, 503193.7918, 7653284.999000001))
ops.curve.vertex_add(location=(379442533.79999995, 532062.8064, 7652244.995))
ops.curve.vertex_add(location=(379442651.9, 560927.7841, 7651147.125))
ops.curve.vertex_add(location=(379442776.1, 589788.5072999999, 7649991.418))
ops.curve.vertex_add(location=(379442906.59999996, 618644.7591, 7648777.908))
ops.curve.vertex_add(location=(379443043.3, 647496.3224000001, 7647506.626))
ops.curve.vertex_add(location=(379443186.3, 676342.9809000001, 7646177.607))
ops.curve.vertex_add(location=(379443335.4, 705184.5183, 7644790.887))
ops.curve.vertex_add(location=(379443490.8, 734020.7186, 7643346.504))
ops.curve.vertex_add(location=(379443652.3, 762851.3665, 7641844.497))
ops.curve.vertex_add(location=(379443820.1, 791676.2467, 7640284.907000001))
ops.curve.vertex_add(location=(379443994.0, 820495.1445, 7638667.774999999))
ops.curve.vertex_add(location=(379444174.2, 849307.8454, 7636993.147))
ops.curve.vertex_add(location=(379444360.6, 878114.1356, 7635261.067000001))
ops.curve.vertex_add(location=(379444553.1, 906913.8014, 7633471.581))
ops.curve.vertex_add(location=(379444751.9, 935706.6298, 7631624.739))
ops.curve.vertex_add(location=(379444956.8, 964492.4081, 7629720.589))
ops.curve.vertex_add(location=(379445167.9, 993270.9241, 7627759.183999999))
ops.curve.vertex_add(location=(379445385.20000005, 1022041.966, 7625740.576))
ops.curve.vertex_add(location=(379445608.59999996, 1050805.323, 7623664.818999999))
ops.curve.vertex_add(location=(379445838.3, 1079560.7829999998, 7621531.97))
ops.curve.vertex_add(location=(379446074.1, 1108308.138, 7619342.085))
ops.curve.vertex_add(location=(379446316.0, 1137047.176, 7617095.222))
ops.curve.vertex_add(location=(379446564.1, 1165777.689, 7614791.443))
ops.curve.vertex_add(location=(379446818.4, 1194499.469, 7612430.809))
ops.curve.vertex_add(location=(379447078.8, 1223212.3059999999, 7610013.383))
ops.curve.vertex_add(location=(379447345.3, 1251915.995, 7607539.229))
ops.curve.vertex_add(location=(379447618.0, 1280610.327, 7605008.413))
ops.curve.vertex_add(location=(379447896.8, 1309295.0969999998, 7602421.0030000005))
ops.curve.vertex_add(location=(379448181.7, 1337970.099, 7599777.067))
ops.curve.vertex_add(location=(379448472.7, 1366635.1279999998, 7597076.675))
ops.curve.vertex_add(location=(379448769.90000004, 1395289.9789999998, 7594319.9))
ops.curve.vertex_add(location=(379449382.5, 1452568.335, 7588637.49))
ops.curve.vertex_add(location=(379449930.5, 1501931.164, 7583556.496))
ops.curve.vertex_add(location=(379450496.5, 1551260.87, 7578308.8209999995))
ops.curve.vertex_add(location=(379451080.59999996, 1600556.423, 7572894.875))
ops.curve.vertex_add(location=(379451682.8, 1649816.804, 7567315.082))
ops.curve.vertex_add(location=(379452303.0, 1699040.998, 7561569.873000001))
ops.curve.vertex_add(location=(379452941.1, 1748227.997, 7555659.695))
ops.curve.vertex_add(location=(379453597.20000005, 1797376.7989999999, 7549585.0030000005))
ops.curve.vertex_add(location=(379454271.2, 1846486.412, 7543346.266))
ops.curve.vertex_add(location=(379454963.0, 1895555.848, 7536943.962))
ops.curve.vertex_add(location=(379455672.7, 1944584.128, 7530378.581))
ops.curve.vertex_add(location=(379456400.2, 1993570.281, 7523650.625))
ops.curve.vertex_add(location=(379457145.5, 2042513.341, 7516760.603))
ops.curve.vertex_add(location=(379457908.40000004, 2091412.3530000001, 7509709.039))
ops.curve.vertex_add(location=(379458689.0, 2140266.369, 7502496.464))
ops.curve.vertex_add(location=(379459487.29999995, 2189074.447, 7495123.421))
ops.curve.vertex_add(location=(379460303.1, 2237835.657, 7487590.462))
ops.curve.vertex_add(location=(379461136.5, 2286549.0730000003, 7479898.148999999))
ops.curve.vertex_add(location=(379461987.4, 2335213.7800000003, 7472047.054))
ops.curve.vertex_add(location=(379462855.7, 2383828.8710000003, 7464037.758))
ops.curve.vertex_add(location=(379463741.5, 2432393.449, 7455870.853))
ops.curve.vertex_add(location=(379464644.6, 2480906.6229999997, 7447546.938))
ops.curve.vertex_add(location=(379465565.0, 2529367.5130000003, 7439066.622))
ops.curve.vertex_add(location=(379466502.7, 2577775.247, 7430430.522))
ops.curve.vertex_add(location=(379467457.70000005, 2626128.962, 7421639.265))
ops.curve.vertex_add(location=(379468429.8, 2674427.8049999997, 7412693.4860000005))
ops.curve.vertex_add(location=(379470425.3, 2770857.508, 7394340.94))
ops.curve.vertex_add(location=(379472071.9, 2847891.7150000003, 7379208.226))
ops.curve.vertex_add(location=(379473591.0, 2917093.9760000003, 7365256.629000001))
ops.curve.vertex_add(location=(379475145.20000005, 2986171.997, 7350992.469))
ops.curve.vertex_add(location=(379476734.2, 3055123.431, 7336417.813))
ops.curve.vertex_add(location=(379478357.8, 3123945.97, 7321534.757))
ops.curve.vertex_add(location=(379480016.1, 3192637.353, 7306345.425000001))
ops.curve.vertex_add(location=(379481708.6, 3261195.358, 7290851.971))
ops.curve.vertex_add(location=(379483435.40000004, 3329617.807, 7275056.572000001))
ops.curve.vertex_add(location=(379485196.2, 3397902.5670000003, 7258961.432))
ops.curve.vertex_add(location=(379486990.8, 3466047.5459999996, 7242568.778))
ops.curve.vertex_add(location=(379488819.1, 3534050.697, 7225880.861))
ops.curve.vertex_add(location=(379490680.9, 3601910.018, 7208899.954000001))
ops.curve.vertex_add(location=(379492576.0, 3669623.5470000003, 7191628.348999999))
ops.curve.vertex_add(location=(379494504.29999995, 3737189.371, 7174068.358))
ops.curve.vertex_add(location=(379496465.5, 3804605.616, 7156222.312))
ops.curve.vertex_add(location=(379498459.5, 3871870.4560000002, 7138092.558999999))
ops.curve.vertex_add(location=(379500486.0, 3938982.108, 7119681.4629999995))
ops.curve.vertex_add(location=(379502545.0, 4005938.831, 7100991.404))
ops.curve.vertex_add(location=(379504636.1, 4072738.93, 7082024.774))
ops.curve.vertex_add(location=(379506759.29999995, 4139380.7530000005, 7062783.981))
ops.curve.vertex_add(location=(379508914.3, 4205862.691000001, 7043271.442))
ops.curve.vertex_add(location=(379511101.0, 4272183.181, 7023489.586))
ops.curve.vertex_add(location=(379513319.1, 4338340.701, 7003440.854))
ops.curve.vertex_add(location=(379515568.4, 4404333.773, 6983127.693))
ops.curve.vertex_add(location=(379517848.79999995, 4470160.960999999, 6962552.558999999))
ops.curve.vertex_add(location=(379520160.09999996, 4535820.874, 6941717.915999999))
ops.curve.vertex_add(location=(379522502.0, 4601312.16, 6920626.232))
ops.curve.vertex_add(location=(379524874.4, 4666633.512999999, 6899279.982))
ops.curve.vertex_add(location=(379527277.1, 4731783.666, 6877681.645))
ops.curve.vertex_add(location=(379529709.90000004, 4796761.3950000005, 6855833.702))
ops.curve.vertex_add(location=(379532172.5, 4861565.516, 6833738.638))
ops.curve.vertex_add(location=(379534664.9, 4926194.888, 6811398.938999999))
ops.curve.vertex_add(location=(379537186.70000005, 4990648.409, 6788817.092))
ops.curve.vertex_add(location=(379539737.8, 5054925.016, 6765995.5819999995))
ops.curve.vertex_add(location=(379542317.90000004, 5119023.688, 6742936.897))
ops.curve.vertex_add(location=(379544927.0, 5182943.441000001, 6719643.521))
ops.curve.vertex_add(location=(379547564.8, 5246683.333, 6696117.933999999))
ops.curve.vertex_add(location=(379550231.0, 5310242.457, 6672362.617))
ops.curve.vertex_add(location=(379552925.5, 5373619.947, 6648380.044000001))
ops.curve.vertex_add(location=(379555648.2, 5436814.972, 6624172.686))
ops.curve.vertex_add(location=(379558398.70000005, 5499826.739, 6599743.007))
ops.curve.vertex_add(location=(379561176.90000004, 5562654.492, 6575093.468))
ops.curve.vertex_add(location=(379563982.59999996, 5625297.511, 6550226.522))
ops.curve.vertex_add(location=(379566815.59999996, 5687755.112, 6525144.613000001))
ops.curve.vertex_add(location=(379569675.70000005, 5750026.643999999, 6499850.181))
ops.curve.vertex_add(location=(379572562.7, 5812111.493000001, 6474345.654))
ops.curve.vertex_add(location=(379575476.4, 5874009.077, 6448633.455))
ops.curve.vertex_add(location=(379578416.6, 5935718.850000001, 6422715.994999999))
ops.curve.vertex_add(location=(379581383.09999996, 5997240.2979999995, 6396595.675))
ops.curve.vertex_add(location=(379584375.79999995, 6058572.937, 6370274.886999999))
ops.curve.vertex_add(location=(379587394.3, 6119716.319, 6343756.012))
ops.curve.vertex_add(location=(379590438.6, 6180670.025, 6317041.418))
ops.curve.vertex_add(location=(379593508.4, 6241433.667, 6290133.463))
ops.curve.vertex_add(location=(379596603.59999996, 6302006.888, 6263034.492))
ops.curve.vertex_add(location=(379599723.9, 6362389.361, 6235746.838))
ops.curve.vertex_add(location=(379602869.2, 6422580.787, 6208272.819))
ops.curve.vertex_add(location=(379606039.2, 6482580.897, 6180614.743))
ops.curve.vertex_add(location=(379609233.8, 6542389.449999999, 6152774.902))
ops.curve.vertex_add(location=(379612452.8, 6602006.231000001, 6124755.573000001))
ops.curve.vertex_add(location=(379618963.3, 6720663.762, 6068187.495))
ops.curve.vertex_add(location=(379623913.5, 6809238.557, 6025264.931))
ops.curve.vertex_add(location=(379628916.90000004, 6897379.600000001, 5981960.437))
ops.curve.vertex_add(location=(379633972.8, 6985086.67, 5938281.429))
ops.curve.vertex_add(location=(379639080.5, 7072359.669000001, 5894235.246))
ops.curve.vertex_add(location=(379644239.40000004, 7159198.614, 5849829.14))
ops.curve.vertex_add(location=(379649448.90000004, 7245603.637, 5805070.28))
ops.curve.vertex_add(location=(379654708.5, 7331574.979, 5759965.75))
ops.curve.vertex_add(location=(379660017.5, 7417112.987000001, 5714522.546))
ops.curve.vertex_add(location=(379665375.40000004, 7502218.1110000005, 5668747.575999999))
ops.curve.vertex_add(location=(379670781.5, 7586890.897, 5622647.659))
ops.curve.vertex_add(location=(379676235.3, 7671131.987, 5576229.524999999))
ops.curve.vertex_add(location=(379681736.2, 7754942.115, 5529499.814))
ops.curve.vertex_add(location=(379687283.70000005, 7838322.100000001, 5482465.073))
ops.curve.vertex_add(location=(379692877.09999996, 7921272.847, 5435131.76))
ops.curve.vertex_add(location=(379698516.0, 8003795.34, 5387506.241))
ops.curve.vertex_add(location=(379704199.8, 8085890.641, 5339594.79))
ops.curve.vertex_add(location=(379709927.90000004, 8167559.887, 5291403.586999999))
ops.curve.vertex_add(location=(379715699.8, 8248804.284, 5242938.725))
ops.curve.vertex_add(location=(379721515.0, 8329625.106, 5194206.199))
ops.curve.vertex_add(location=(379727373.0, 8410023.693, 5145211.919000001))
ops.curve.vertex_add(location=(379733273.2, 8490001.445, 5095961.697))
ops.curve.vertex_add(location=(379739215.09999996, 8569559.821, 5046461.258))
ops.curve.vertex_add(location=(379745198.2, 8648700.339000002, 4996716.234999999))
ops.curve.vertex_add(location=(379751222.1, 8727424.565, 4946732.171))
ops.curve.vertex_add(location=(379757286.29999995, 8805734.121, 4896514.518))
ops.curve.vertex_add(location=(379763390.20000005, 8883630.671999998, 4846068.641))
ops.curve.vertex_add(location=(379769533.3, 8961115.932, 4795399.813))
ops.curve.vertex_add(location=(379775715.29999995, 9038191.658, 4744513.221))
ops.curve.vertex_add(location=(379781935.6, 9114859.646000002, 4693413.965))
ops.curve.vertex_add(location=(379788193.8, 9191121.730999999, 4642107.057))
ops.curve.vertex_add(location=(379794489.40000004, 9266979.784, 4590597.424))
ops.curve.vertex_add(location=(379800822.0, 9342435.712000001, 4538889.907))
ops.curve.vertex_add(location=(379807191.1, 9417491.452, 4486989.263))
ops.curve.vertex_add(location=(379813596.29999995, 9492148.97, 4434900.166999999))
ops.curve.vertex_add(location=(379820037.20000005, 9566410.263, 4382627.211))
ops.curve.vertex_add(location=(379826513.4, 9640277.352, 4330174.903))
ops.curve.vertex_add(location=(379833024.3, 9713752.284, 4277547.673))
ops.curve.vertex_add(location=(379839569.7, 9786837.125, 4224749.872))
ops.curve.vertex_add(location=(379846149.09999996, 9859533.967, 4171785.769))
ops.curve.vertex_add(location=(379852762.09999996, 9931844.918000001, 4118659.558))
ops.curve.vertex_add(location=(379859408.3, 10003772.1, 4065375.356))
ops.curve.vertex_add(location=(379866087.3, 10075317.67, 4011937.204))
ops.curve.vertex_add(location=(379872798.8, 10146483.76, 3958349.068))
ops.curve.vertex_add(location=(379879542.29999995, 10217272.569999998, 3904614.8400000003))
ops.curve.vertex_add(location=(379886317.5, 10287686.26, 3850738.342))
ops.curve.vertex_add(location=(379893124.0, 10357727.03, 3796723.321))
ops.curve.vertex_add(location=(379899961.5, 10427397.08, 3742573.455))
ops.curve.vertex_add(location=(379906829.6, 10496698.63, 3688292.355))
ops.curve.vertex_add(location=(379913727.9, 10565633.889999999, 3633883.559))
ops.curve.vertex_add(location=(379920656.1, 10634205.08, 3579350.5420000004))
ops.curve.vertex_add(location=(379927613.8, 10702414.440000001, 3524696.71))
ops.curve.vertex_add(location=(379934600.8, 10770264.19, 3469925.404))
ops.curve.vertex_add(location=(379941616.6, 10837756.58, 3415039.903))
ops.curve.vertex_add(location=(379948661.0, 10904893.83, 3360043.4209999996))
ops.curve.vertex_add(location=(379955733.59999996, 10971678.190000001, 3304939.1089999997))
ops.curve.vertex_add(location=(379962834.2, 11038111.9, 3249730.06))
ops.curve.vertex_add(location=(379969962.3, 11104197.18, 3194419.3049999997))
ops.curve.vertex_add(location=(379977117.7, 11169936.29, 3139009.8159999996))
ops.curve.vertex_add(location=(379984300.09999996, 11235331.45, 3083504.507))
ops.curve.vertex_add(location=(379991509.2, 11300384.899999999, 3027906.235))
ops.curve.vertex_add(location=(379998744.59999996, 11365098.85, 2972217.802))
ops.curve.vertex_add(location=(380006006.2, 11429475.549999999, 2916441.954))
ops.curve.vertex_add(location=(380013293.5, 11493517.200000001, 2860581.3819999998))
ops.curve.vertex_add(location=(380020606.4, 11557226.02, 2804638.7260000003))
ops.curve.vertex_add(location=(380027944.4, 11620604.219999999, 2748616.573))
ops.curve.vertex_add(location=(380035307.5, 11683653.99, 2692517.4579999996))
ops.curve.vertex_add(location=(380042695.3, 11746377.54, 2636343.8660000004))
ops.curve.vertex_add(location=(380050107.4, 11808777.05, 2580098.234))
ops.curve.vertex_add(location=(380057543.8, 11870854.7, 2523782.948))
ops.curve.vertex_add(location=(380065004.0, 11932612.649999999, 2467400.349))
ops.curve.vertex_add(location=(380072487.90000004, 11994053.08, 2410952.73))
ops.curve.vertex_add(location=(380079995.1, 12055178.13, 2354442.338))
ops.curve.vertex_add(location=(380087525.5, 12115989.94, 2297871.375))
ops.curve.vertex_add(location=(380095078.8, 12176490.66, 2241241.999))
ops.curve.vertex_add(location=(380102654.7, 12236682.4, 2184556.326))
ops.curve.vertex_add(location=(380110253.09999996, 12296567.28, 2127816.427))
ops.curve.vertex_add(location=(380117873.59999996, 12356147.39, 2071024.3340000003))
ops.curve.vertex_add(location=(380125516.1, 12415424.84, 2014182.0359999998))
ops.curve.vertex_add(location=(380133180.3, 12474401.700000001, 1957291.482))
ops.curve.vertex_add(location=(380140866.0, 12533080.03, 1900354.584))
ops.curve.vertex_add(location=(380148572.90000004, 12591461.9, 1843373.212))
ops.curve.vertex_add(location=(380156300.9, 12649549.360000001, 1786349.201))
ops.curve.vertex_add(location=(380164049.79999995, 12707344.42, 1729284.348))
ops.curve.vertex_add(location=(380171819.20000005, 12764849.120000001, 1672180.413))
ops.curve.vertex_add(location=(380179609.1, 12822065.459999999, 1615039.1199999999))
ops.curve.vertex_add(location=(380187419.1, 12878995.440000001, 1557862.16))
ops.curve.vertex_add(location=(380195249.20000005, 12935641.040000001, 1500651.188))
ops.curve.vertex_add(location=(380203099.0, 12992004.23, 1443407.825))
ops.curve.vertex_add(location=(380210968.5, 13048086.97, 1386133.661))
ops.curve.vertex_add(location=(380218857.29999995, 13103891.2, 1328830.2510000002))
ops.curve.vertex_add(location=(380226765.4, 13159418.86, 1271499.121))
ops.curve.vertex_add(location=(380234692.5, 13214671.860000001, 1214141.7650000001))
ops.curve.vertex_add(location=(380242638.4, 13269652.1, 1156759.645))
ops.curve.vertex_add(location=(380250602.9, 13324361.48, 1099354.196))
ops.curve.vertex_add(location=(380258585.9, 13378801.87, 1041926.821))
ops.curve.vertex_add(location=(380266587.2, 13432975.15, 984478.8955))
ops.curve.vertex_add(location=(380274606.6, 13486883.15, 927011.7672))
ops.curve.vertex_add(location=(380282643.90000004, 13540527.73, 869526.7558))
ops.curve.vertex_add(location=(380290699.0, 13593910.690000001, 812025.1541))
ops.curve.vertex_add(location=(380298771.59999996, 13647033.86, 754508.2284))
ops.curve.vertex_add(location=(380306861.7, 13699899.020000001, 696977.2191))
ops.curve.vertex_add(location=(380314969.0, 13752507.97, 639433.3411))
ops.curve.vertex_add(location=(380323093.40000004, 13804862.47, 581877.7844))
ops.curve.vertex_add(location=(380331234.7, 13856964.27, 524311.7146))
ops.curve.vertex_add(location=(380339392.70000005, 13908815.13, 466736.2735))
ops.curve.vertex_add(location=(380347567.4, 13960416.76, 409152.57910000003))
ops.curve.vertex_add(location=(380355758.5, 14011770.89, 351561.7267))
ops.curve.vertex_add(location=(380363965.90000004, 14062879.209999999, 293964.789))
ops.curve.vertex_add(location=(380372189.5, 14113743.42, 236362.81650000002))
ops.curve.vertex_add(location=(380380429.0, 14164365.190000001, 178756.8382))
ops.curve.vertex_add(location=(380388684.40000004, 14214746.17, 121147.8618))
ops.curve.vertex_add(location=(380396955.5, 14264888.03, 63536.87436))
ops.curve.vertex_add(location=(380405242.09999996, 14314792.39, 5924.842419))
ops.curve.vertex_add(location=(380413544.1, 14364460.870000001, -51687.287390000005))
ops.curve.vertex_add(location=(380421861.5, 14413895.09, -109298.58799999999))
ops.curve.vertex_add(location=(380430193.90000004, 14463096.64, -166908.1515))
ops.curve.vertex_add(location=(380438541.3, 14512067.1, -224515.0887))
ops.curve.vertex_add(location=(380446903.6, 14560808.03, -282118.52900000004))
ops.curve.vertex_add(location=(380455280.7, 14609321.01, -339717.6196))
ops.curve.vertex_add(location=(380463672.29999995, 14657607.57, -397311.5257))
ops.curve.vertex_add(location=(380472078.4, 14705669.23, -454899.42939999996))
ops.curve.vertex_add(location=(380480498.8, 14753507.530000001, -512480.53030000004))
ops.curve.vertex_add(location=(380488933.4, 14801123.95, -570054.0441))
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False
curve.name='Gateway_trajectory'
ops.object.mode_set(mode='OBJECT')
bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
bpy.context.scene.frame_set(0)
bpy.context.object.data.bevel_depth = 130.0/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth", index =-1)
#Gateway = bpy.data.objects['Gateway_trajectory']
#Gateway.select_set(True)
bpy.ops.transform.resize(value=(RATIO, RATIO, RATIO))
bpy.context.scene.frame_set(5867)
bpy.context.object.data.bevel_depth = 5/4
bpy.context.object.data.keyframe_insert(data_path="bevel_depth",index=-1) #give some thickness to the trajectory

traj = bpy.data.objects['Gateway_trajectory']
bpy.context.scene.frame_set(0)
traj.location = ((-379441475.0+traj.location.x)*RATIO, (-1526.278258+traj.location.y)*RATIO, (-7662095.166+traj.location.z)*RATIO) 
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
traj.location = (traj.location.x-6.757167284376919e-06, traj.location.y-0.1375111819480721, traj.location.z--2.439339357351855e-05)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1.0)
traj.location = (traj.location.x-0.0, traj.location.y-0.1375111728934607, traj.location.z--4.378647929570434e-05)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2.0)
traj.location = (traj.location.x-2.0271520043024793e-05, traj.location.y-0.27502229781099485, traj.location.z--0.00014602250269035721)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4.0)
traj.location = (traj.location.x-2.0271520043024793e-05, traj.location.y-0.27502219942656014, traj.location.z--0.00022393270421616762)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6.0)
traj.location = (traj.location.x-3.3785861887736246e-05, traj.location.y-0.27502205874222374, traj.location.z--0.00030184290574197803)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8.0)
traj.location = (traj.location.x-4.0543040086049587e-05, traj.location.y-0.27502187764999464, traj.location.z--0.0003797531073814753)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10.0)
traj.location = (traj.location.x-4.7300207370426506e-05, traj.location.y-0.2750216539875776, traj.location.z--0.00045759573708892276)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(12.0)
traj.location = (traj.location.x-0.00012838628390454687, traj.location.y-0.5500424734643226, traj.location.z--0.0011488545073916612)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(16.0)
traj.location = (traj.location.x-0.00015541497123194858, traj.location.y-0.5500410828381752, traj.location.z--0.0014604277417902267)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(20.0)
traj.location = (traj.location.x-0.00018920083675766364, traj.location.y-0.55003935975913, traj.location.z--0.001772000976302479)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(24.0)
traj.location = (traj.location.x-0.00022298669864539988, traj.location.y-0.5500373042271853, traj.location.z--0.0020835742108147315)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(28.0)
traj.location = (traj.location.x-0.00025677256417111494, traj.location.y-0.5500349182694952, traj.location.z--0.002395079873622308)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(32.0)
traj.location = (traj.location.x-0.0002905584260588512, traj.location.y-0.5500321985074708, traj.location.z--0.0027065855363161972)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(36.0)
traj.location = (traj.location.x-0.0006824744486948475, traj.location.y-1.1000549126473027, traj.location.z--0.006347823204578162)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(44.0)
traj.location = (traj.location.x-0.0008176178962457925, traj.location.y-1.1000400536244737, traj.location.z--0.007593980999104133)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(52.0)
traj.location = (traj.location.x-0.0009527613547106739, traj.location.y-1.1000225349784554, traj.location.z--0.008839936078629762)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(60.0)
traj.location = (traj.location.x-0.0010811476422531996, traj.location.y-1.100002364817863, traj.location.z--0.010085823586223341)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(68.0)
traj.location = (traj.location.x-0.0012162910861661658, traj.location.y-1.0999795390883857, traj.location.z--0.01133171109381692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(76.0)
traj.location = (traj.location.x-0.0013514345519070048, traj.location.y-1.099954051032853, traj.location.z--0.012577463458114835)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(84.0)
traj.location = (traj.location.x-0.001486577995819971, traj.location.y-1.0999259209227858, traj.location.z--0.013823148250594386)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(92.0)
traj.location = (traj.location.x-0.0016217214542848524, traj.location.y-1.0998951352438322, traj.location.z--0.015068630327959909)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(100.0)
traj.location = (traj.location.x-0.0017501077381893992, traj.location.y-1.09986168723883, traj.location.z--0.016314044833393382)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(108.0)
traj.location = (traj.location.x-0.0018920083603006788, traj.location.y-1.099825597179283, traj.location.z--0.0175592566239402)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(116.0)
traj.location = (traj.location.x-0.0020203946478432044, traj.location.y-1.09978685155086, traj.location.z--0.018804333270850293)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(124.0)
traj.location = (traj.location.x-0.002155538102670107, traj.location.y-1.099745457110732, traj.location.z--0.02004934234605571)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(132.0)
traj.location = (traj.location.x-0.0022839243792986963, traj.location.y-1.0997014138588774, traj.location.z--0.02129401356262406)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(140.0)
traj.location = (traj.location.x-0.0024258250123239122, traj.location.y-1.0996547150381595, traj.location.z--0.022538617207487732)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(148.0)
traj.location = (traj.location.x-0.0025542112889525015, traj.location.y-1.0996053741628913, traj.location.z--0.023783018137237377)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(156.0)
traj.location = (traj.location.x-0.0026893547510553617, traj.location.y-1.0995533912330906, traj.location.z--0.025027216351531933)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(164.0)
traj.location = (traj.location.x-0.002824498194968328, traj.location.y-1.0994987527344122, traj.location.z--0.026271144279348846)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(172.0)
traj.location = (traj.location.x-0.002959641657071188, traj.location.y-1.0994414789383669, traj.location.z--0.027514869491596983)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(180.0)
traj.location = (traj.location.x-0.003088027937337756, traj.location.y-1.0993815563306057, traj.location.z--0.028758324417140102)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(188.0)
traj.location = (traj.location.x-0.006588243410078576, traj.location.y-2.198572776619798, traj.location.z--0.0612462027499987)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(204.0)
traj.location = (traj.location.x-0.00629092780945939, traj.location.y-1.9514592129521624, traj.location.z--0.05853353575810161)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(218.0)
traj.location = (traj.location.x-0.006709872519422788, traj.location.y-1.9512306042844152, traj.location.z--0.062449047067389074)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(233.0)
traj.location = (traj.location.x-0.007135574400308542, traj.location.y-1.9509872514657616, traj.location.z--0.06636286908337752)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(247.0)
traj.location = (traj.location.x-0.007554519106633961, traj.location.y-1.9507291815249275, traj.location.z--0.07027486666288496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(261.0)
traj.location = (traj.location.x-0.007980220994795673, traj.location.y-1.95045640121905, traj.location.z--0.07418497223386566)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(275.0)
traj.location = (traj.location.x-0.008392408530198736, traj.location.y-1.9501689173053123, traj.location.z--0.07809311822495602)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(289.0)
traj.location = (traj.location.x-0.008818110407446511, traj.location.y-1.9498667770839404, traj.location.z--0.0819989667770642)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(304.0)
traj.location = (traj.location.x-0.009237055124685867, traj.location.y-1.9495499670405678, traj.location.z--0.0859027206056453)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(318.0)
traj.location = (traj.location.x-0.009662757001933642, traj.location.y-1.9492185412325753, traj.location.z--0.08980410942410799)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(332.0)
traj.location = (traj.location.x-0.010074944533698726, traj.location.y-1.948872499659977, traj.location.z--0.09370306565995179)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(346.0)
traj.location = (traj.location.x-0.010500646421860438, traj.location.y-1.9485118625942874, traj.location.z--0.09759945417044946)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(360.0)
traj.location = (traj.location.x-0.010912833957263501, traj.location.y-1.948136677335718, traj.location.z--0.10149320738344159)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(375.0)
traj.location = (traj.location.x-0.011338535838149255, traj.location.y-1.9477469438842547, traj.location.z--0.10538419015574618)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(389.0)
traj.location = (traj.location.x-0.011750723373552319, traj.location.y-1.9473427027829544, traj.location.z--0.10927240248713588)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(403.0)
traj.location = (traj.location.x-0.012176425258076051, traj.location.y-1.9469239675461338, traj.location.z--0.11315750651908729)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(417.0)
traj.location = (traj.location.x-0.01259536996803945, traj.location.y-1.9464907854740403, traj.location.z--0.11703963739500978)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(432.0)
traj.location = (traj.location.x-0.013007557499804534, traj.location.y-1.9460431633238073, traj.location.z--0.12091865997149398)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(446.0)
traj.location = (traj.location.x-0.013433259384328267, traj.location.y-1.9455811483956822, traj.location.z--0.1247943039617212)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(460.0)
traj.location = (traj.location.x-0.01384544691973133, traj.location.y-1.9451047609611507, traj.location.z--0.1286667045088734)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(474.0)
traj.location = (traj.location.x-0.014264391629694728, traj.location.y-1.9446140348060794, traj.location.z--0.13253552375454092)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(488.0)
traj.location = (traj.location.x-0.014683336343296105, traj.location.y-1.9441089969592014, traj.location.z--0.1364008292704284)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(503.0)
traj.location = (traj.location.x-0.015095523867785232, traj.location.y-1.9435897149921857, traj.location.z--0.14026248591301282)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(517.0)
traj.location = (traj.location.x-0.015521225763222901, traj.location.y-1.9430561010617993, traj.location.z--0.14412029096706647)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(531.0)
traj.location = (traj.location.x-0.015933413294987986, traj.location.y-1.9425084322121933, traj.location.z--0.14797431200452138)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(545.0)
traj.location = (traj.location.x-0.01634560082675307, traj.location.y-1.94194643815635, traj.location.z--0.15182441388174084)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(559.0)
traj.location = (traj.location.x-0.016764545540354447, traj.location.y-1.9413703891812446, traj.location.z--0.15567032631190614)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(574.0)
traj.location = (traj.location.x-0.017183490246679867, traj.location.y-1.940780285286877, traj.location.z--0.15951211686660827)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(588.0)
traj.location = (traj.location.x-0.01759567778572091, traj.location.y-1.9401759913297525, traj.location.z--0.16334965040255156)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(602.0)
traj.location = (traj.location.x-0.018007865321123973, traj.location.y-1.9395578451685935, traj.location.z--0.16718285934803134)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(616.0)
traj.location = (traj.location.x-0.01842681003108737, traj.location.y-1.938925576516354, traj.location.z--0.1710116085594109)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(630.0)
traj.location = (traj.location.x-0.018838997566490434, traj.location.y-1.9382794556600658, traj.location.z--0.17483576289328084)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(645.0)
traj.location = (traj.location.x-0.019251185101893498, traj.location.y-1.9376194150279673, traj.location.z--0.17865532234986858)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(659.0)
traj.location = (traj.location.x-0.01966337264093454, traj.location.y-1.9369455221917349, traj.location.z--0.18247015178531)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(673.0)
traj.location = (traj.location.x-0.020082317350897938, traj.location.y-1.9362577771514395, traj.location.z--0.18628004848483215)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(687.0)
traj.location = (traj.location.x-0.04139444013344473, traj.location.y-3.87039745093945, traj.location.z--0.3839702588390992)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(716.0)
traj.location = (traj.location.x-0.037029306553449715, traj.location.y-3.3355316191819355, traj.location.z--0.34333154090245444)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(740.0)
traj.location = (traj.location.x-0.03824559763961588, traj.location.y-3.33329344085908, traj.location.z--0.35459446397794636)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(765.0)
traj.location = (traj.location.x-0.03946864590761834, traj.location.y-3.330985663657131, traj.location.z--0.3658296826452556)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(789.0)
traj.location = (traj.location.x-0.04069169417198282, traj.location.y-3.3286090308650813, traj.location.z--0.3770362509002325)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(814.0)
traj.location = (traj.location.x-0.04190798526178696, traj.location.y-3.3261638127698347, traj.location.z--0.38821369574071696)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(838.0)
traj.location = (traj.location.x-0.04311751918066875, traj.location.y-3.3236504823734805, traj.location.z--0.39936093601920675)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(863.0)
traj.location = (traj.location.x-0.044333810274110874, traj.location.y-3.321069445106403, traj.location.z--0.41047743116160973)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(887.0)
traj.location = (traj.location.x-0.045543344185716705, traj.location.y-3.3184213766858477, traj.location.z--0.42156223516406044)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(912.0)
traj.location = (traj.location.x-0.04674612093731412, traj.location.y-3.315706547398733, traj.location.z--0.43261473988076204)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(936.0)
traj.location = (traj.location.x-0.04795565485255793, traj.location.y-3.3129255653906, traj.location.z--0.4436341344512016)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(961.0)
traj.location = (traj.location.x-0.04915843160051736, traj.location.y-3.310078971235299, traj.location.z--0.4546196080144682)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(985.0)
traj.location = (traj.location.x-0.050361208344838815, traj.location.y-3.307167102791425, traj.location.z--0.46557068756857234)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1010.0)
traj.location = (traj.location.x-0.05155047074731556, traj.location.y-3.304190703348013, traj.location.z--0.4764863595375459)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1034.0)
traj.location = (traj.location.x-0.05274649031707668, traj.location.y-3.301150245907081, traj.location.z--0.4873661509193994)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1059.0)
traj.location = (traj.location.x-0.0539425098868378, traj.location.y-3.2980461358991136, traj.location.z--0.4982092508532219)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1083.0)
traj.location = (traj.location.x-0.05512501512203016, traj.location.y-3.2948791841847083, traj.location.z--0.5090150511937281)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1108.0)
traj.location = (traj.location.x-0.05631427751723095, traj.location.y-3.2916496610509114, traj.location.z--0.5197828762234735)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1132.0)
traj.location = (traj.location.x-0.05749678274514736, traj.location.y-3.2883583097865596, traj.location.z--0.5305120502250702)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1157.0)
traj.location = (traj.location.x-0.05867253080214141, traj.location.y-3.28500567096566, traj.location.z--0.5412019650531761)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1181.0)
traj.location = (traj.location.x-0.05985503603005782, traj.location.y-3.2815924203052873, traj.location.z--0.5518518774188692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1206.0)
traj.location = (traj.location.x-0.061024026912491536, traj.location.y-3.278118963235954, traj.location.z--0.5624613143197621)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1230.0)
traj.location = (traj.location.x-0.062193017791287275, traj.location.y-3.274586043046668, traj.location.z--0.573029600038808)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1255.0)
traj.location = (traj.location.x-0.063362008673721, traj.location.y-3.2709942003110655, traj.location.z--0.5835561940021421)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1279.0)
traj.location = (traj.location.x-0.06453099955979269, traj.location.y-3.2673440431748872, traj.location.z--0.5940404204924334)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1304.0)
traj.location = (traj.location.x-0.06568647609310574, traj.location.y-3.2636362473553504, traj.location.z--0.6044817389358172)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1328.0)
traj.location = (traj.location.x-0.13483938179706456, traj.location.y-6.515921593246276, traj.location.z--1.2401132332891507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1377.0)
traj.location = (traj.location.x-0.11126360614434816, traj.location.y-5.205334426985672, traj.location.z--1.022543623483159)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1416.0)
traj.location = (traj.location.x-0.102648210919142, traj.location.y-4.6761163077663355, traj.location.z--0.9427335076679242)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1452.0)
traj.location = (traj.location.x-0.10501997854953515, traj.location.y-4.667721196368461, traj.location.z--0.9638539294632551)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1487.0)
traj.location = (traj.location.x-0.10737147464897134, traj.location.y-4.659167494126706, traj.location.z--0.9848346804981247)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1522.0)
traj.location = (traj.location.x-0.10970945642111474, traj.location.y-4.650457836338404, traj.location.z--1.0056738011926996)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1557.0)
traj.location = (traj.location.x-0.11205419535690453, traj.location.y-4.641595398874642, traj.location.z--1.0263693995384529)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1593.0)
traj.location = (traj.location.x-0.11436514843080658, traj.location.y-4.6325828170328975, traj.location.z--1.0469194483837896)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1628.0)
traj.location = (traj.location.x-0.116682858675631, traj.location.y-4.623423063969113, traj.location.z--1.0673223935786496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1663.0)
traj.location = (traj.location.x-0.1189802973967744, traj.location.y-4.6141191804110235, traj.location.z--1.0875764106866654)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1699.0)
traj.location = (traj.location.x-0.12126422178698704, traj.location.y-4.604673936799429, traj.location.z--1.1076799455580044)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1734.0)
traj.location = (traj.location.x-0.12354138899536338, traj.location.y-4.595090373862178, traj.location.z--1.1276313764712995)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1769.0)
traj.location = (traj.location.x-0.12580504185461905, traj.location.y-4.585371532326803, traj.location.z--1.1474292168481952)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1805.0)
traj.location = (traj.location.x-0.12805518037930597, traj.location.y-4.575520115062545, traj.location.z--1.1670721828265869)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1840.0)
traj.location = (traj.location.x-0.1302985617185186, traj.location.y-4.565539433083956, traj.location.z--1.1865589229711304)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1875.0)
traj.location = (traj.location.x-0.13252167155587813, traj.location.y-4.555432121688511, traj.location.z--1.2058881534195507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1910.0)
traj.location = (traj.location.x-0.1347380242041254, traj.location.y-4.545201424318975, traj.location.z--1.2250587254523566)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1946.0)
traj.location = (traj.location.x-0.13693410534324357, traj.location.y-4.5348502465596425, traj.location.z--1.244069557921648)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(1981.0)
traj.location = (traj.location.x-0.13913018647872377, traj.location.y-4.524381426423247, traj.location.z--1.2629195696801503)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2016.0)
traj.location = (traj.location.x-0.14129923892687657, traj.location.y-4.513798072209056, traj.location.z--1.2816079498669524)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2052.0)
traj.location = (traj.location.x-0.14346829136411543, traj.location.y-4.503103089501337, traj.location.z--1.3001336173345521)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2087.0)
traj.location = (traj.location.x-0.14561707230313914, traj.location.y-4.492299383884415, traj.location.z--1.3184959639371527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2122.0)
traj.location = (traj.location.x-0.1477590960530506, traj.location.y-4.481390063657784, traj.location.z--1.3366941788142412)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2158.0)
traj.location = (traj.location.x-0.149880848290195, traj.location.y-4.470377899262246, traj.location.z--1.3547274511050205)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2193.0)
traj.location = (traj.location.x-0.1519890861782187, traj.location.y-4.459265863853716, traj.location.z--1.3725953753792055)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2228.0)
traj.location = (traj.location.x-0.15409056689895806, traj.location.y-4.448056795444813, traj.location.z--1.39029734349117)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2264.0)
traj.location = (traj.location.x-0.15617853327057674, traj.location.y-4.436753734763386, traj.location.z--1.4078328148669925)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2299.0)
traj.location = (traj.location.x-0.15824622813306632, traj.location.y-4.425359317106597, traj.location.z--1.4252015192198542)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2334.0)
traj.location = (traj.location.x-0.16030716580644366, traj.location.y-4.41387665077383, traj.location.z--1.442402983547879)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2369.0)
traj.location = (traj.location.x-0.16235458914889023, traj.location.y-4.402308371062816, traj.location.z--1.4594369375638507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2405.0)
traj.location = (traj.location.x-0.16438849813857814, traj.location.y-4.390657383557482, traj.location.z--1.4763032461244165)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2440.0)
traj.location = (traj.location.x-0.166402135611861, traj.location.y-4.378926391127038, traj.location.z--1.4930016389426441)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2475.0)
traj.location = (traj.location.x-0.1684157730815059, traj.location.y-4.367118299355809, traj.location.z--1.5095320484469994)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2511.0)
traj.location = (traj.location.x-0.17040238187109935, traj.location.y-4.355235743541073, traj.location.z--1.5258944070654366)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2546.0)
traj.location = (traj.location.x-0.17238223347158055, traj.location.y-4.343281358980278, traj.location.z--1.542088849941706)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2581.0)
traj.location = (traj.location.x-0.17434181357020861, traj.location.y-4.331257983685873, traj.location.z--1.558115174360296)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2617.0)
traj.location = (traj.location.x-0.17630139365428477, traj.location.y-4.319168117811898, traj.location.z--1.5739736506085364)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2652.0)
traj.location = (traj.location.x-0.1782407022328698, traj.location.y-4.307014599370689, traj.location.z--1.589664548973019)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2687.0)
traj.location = (traj.location.x-0.18015973928413587, traj.location.y-4.294799793372988, traj.location.z--1.605187801882039)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2722.0)
traj.location = (traj.location.x-0.18207201916811755, traj.location.y-4.28252647025937, traj.location.z--1.6205438823377563)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2758.0)
traj.location = (traj.location.x-0.1839775418775389, traj.location.y-4.270196995040408, traj.location.z--1.6357329930555693)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2793.0)
traj.location = (traj.location.x-0.18585603589963284, traj.location.y-4.257813935441675, traj.location.z--1.6507556070370697)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2828.0)
traj.location = (traj.location.x-0.18772777274352848, traj.location.y-4.245379791617097, traj.location.z--1.66561192699794)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2864.0)
traj.location = (traj.location.x-0.18958599523466546, traj.location.y-4.232896996148952, traj.location.z--1.6803024935116468)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2899.0)
traj.location = (traj.location.x-0.19143070339850965, traj.location.y-4.22036798161929, traj.location.z--1.6948279147238736)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2934.0)
traj.location = (traj.location.x-0.19326189721323317, traj.location.y-4.207794977895162, traj.location.z--1.7091884609217232)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(2970.0)
traj.location = (traj.location.x-0.1950795766642841, traj.location.y-4.195180485130663, traj.location.z--1.7233849429654242)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3005.0)
traj.location = (traj.location.x-0.19688374178804224, traj.location.y-4.1825266656205144, traj.location.z--1.7374176987141254)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3040.0)
traj.location = (traj.location.x-0.1986743925663177, traj.location.y-4.1698359519476185, traj.location.z--1.751287539028283)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3075.0)
traj.location = (traj.location.x-0.20045152898455854, traj.location.y-4.157110506406582, traj.location.z--1.7649951396251709)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3111.0)
traj.location = (traj.location.x-0.20222190824279096, traj.location.y-4.144352356149852, traj.location.z--1.7785411086505292)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3146.0)
traj.location = (traj.location.x-0.20396525881369598, traj.location.y-4.131563933759139, traj.location.z--1.7919261893930525)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3181.0)
traj.location = (traj.location.x-0.20570860937368707, traj.location.y-4.118747198814731, traj.location.z--1.8051512602855269)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3217.0)
traj.location = (traj.location.x-0.20743168841363513, traj.location.y-4.105904246040097, traj.location.z--1.8182169970449422)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3252.0)
traj.location = (traj.location.x-0.2091480102899368, traj.location.y-4.093037170158823, traj.location.z--1.8311242781040278)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3287.0)
traj.location = (traj.location.x-0.2108440606498334, traj.location.y-4.08014799832273, traj.location.z--1.843873914323126)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3323.0)
traj.location = (traj.location.x-0.21253335382789373, traj.location.y-4.067238622540231, traj.location.z--1.8564669192786027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3358.0)
traj.location = (traj.location.x-0.214202375493187, traj.location.y-4.0543110699630915, traj.location.z--1.8689039686873912)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3393.0)
traj.location = (traj.location.x-0.21586463998391991, traj.location.y-4.041367232599612, traj.location.z--1.8811861436972777)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3428.0)
traj.location = (traj.location.x-0.21751339012917015, traj.location.y-4.0284088673149085, traj.location.z--1.8933144578840029)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3464.0)
traj.location = (traj.location.x-0.43992573048672057, traj.location.y-8.017894324990095, traj.location.z--3.8224027396273073)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3534.0)
traj.location = (traj.location.x-0.33449356440178235, traj.location.y-5.985151892025044, traj.location.z--2.9003517889617)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3587.0)
traj.location = (traj.location.x-0.33808838029654, traj.location.y-5.955842520171927, traj.location.z--2.9261594587634363)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3640.0)
traj.location = (traj.location.x-0.3416358959766512, traj.location.y-5.926518214966961, traj.location.z--2.951466016635777)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3693.0)
traj.location = (traj.location.x-0.34513611145303, traj.location.y-5.897187287732947, traj.location.z--2.976276665601347)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3746.0)
traj.location = (traj.location.x-0.3485957838893228, traj.location.y-5.867857509218084, traj.location.z--3.0005972844009534)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3799.0)
traj.location = (traj.location.x-0.35201491328916745, traj.location.y-5.838536650171363, traj.location.z--3.0244334814873355)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3852.0)
traj.location = (traj.location.x-0.3554002568234864, traj.location.y-5.809232075910927, traj.location.z--3.0477910004578916)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3905.0)
traj.location = (traj.location.x-0.35873830015407293, traj.location.y-5.779950949039744, traj.location.z--3.070675787624225)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(3958.0)
traj.location = (traj.location.x-0.36204255761913373, traj.location.y-5.750700229445897, traj.location.z--3.093093789298507)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4011.0)
traj.location = (traj.location.x-0.36529951486954815, traj.location.y-5.721486404015138, traj.location.z--3.1150510193644436)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4064.0)
traj.location = (traj.location.x-0.3685226862653508, traj.location.y-5.6923159596333335, traj.location.z--3.1365534917057403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4117.0)
traj.location = (traj.location.x-0.3717053146210674, traj.location.y-5.663195112899302, traj.location.z--3.1576072877778643)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4170.0)
traj.location = (traj.location.x-0.3748541571148962, traj.location.y-5.6341296074100455, traj.location.z--3.1782186917515105)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4223.0)
traj.location = (traj.location.x-0.37795569938680273, traj.location.y-5.605125254333757, traj.location.z--3.198393717510328)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4276.0)
traj.location = (traj.location.x-0.38103021298593376, traj.location.y-5.576187391837266, traj.location.z--3.218138581653136)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4329.0)
traj.location = (traj.location.x-0.38406418354134075, traj.location.y-5.547321290515129, traj.location.z--3.2374595007791527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4382.0)
traj.location = (traj.location.x-0.3870576110602997, traj.location.y-5.518532018246901, traj.location.z--3.256362826630493)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4436.0)
traj.location = (traj.location.x-0.3900172527137329, traj.location.y-5.489824237481798, traj.location.z--3.274854437947397)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4489.0)
traj.location = (traj.location.x-0.3929431085016404, traj.location.y-5.461202543097215, traj.location.z--3.292940956759253)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4542.0)
traj.location = (traj.location.x-0.3958351784349361, traj.location.y-5.432671394827253, traj.location.z--3.310628126662891)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4595.0)
traj.location = (traj.location.x-0.3986867053208698, traj.location.y-5.4042348469754415, traj.location.z--3.3279225696877006)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4648.0)
traj.location = (traj.location.x-0.40150444635219174, traj.location.y-5.375896886273836, traj.location.z--3.344830164573466)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4701.0)
traj.location = (traj.location.x-0.40428840151798795, traj.location.y-5.347661499454148, traj.location.z--3.3613571279198027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4754.0)
traj.location = (traj.location.x-0.4070453279928188, traj.location.y-5.319531929959226, traj.location.z--3.377509541181496)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4807.0)
traj.location = (traj.location.x-0.40976846859120997, traj.location.y-5.29151196180635, traj.location.z--3.393293553386229)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4860.0)
traj.location = (traj.location.x-0.41245106617861893, traj.location.y-5.263604500579049, traj.location.z--3.4087150432740145)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4913.0)
traj.location = (traj.location.x-0.4150998778823123, traj.location.y-5.235812857292558, traj.location.z--3.4237802274438423)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(4966.0)
traj.location = (traj.location.x-0.4177284180659626, traj.location.y-5.208139937530859, traj.location.z--3.4384949170641903)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5019.0)
traj.location = (traj.location.x-0.42031641523499275, traj.location.y-5.180588376591345, traj.location.z--3.4528649908754687)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5072.0)
traj.location = (traj.location.x-0.4228773836912296, traj.location.y-5.153160809771066, traj.location.z--3.466896395189565)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5125.0)
traj.location = (traj.location.x-0.4254045662928547, traj.location.y-5.125859669653096, traj.location.z--3.4805948736033088)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5178.0)
traj.location = (traj.location.x-0.42790472019987646, traj.location.y-5.098687388818803, traj.location.z--3.493966237285292)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5231.0)
traj.location = (traj.location.x-0.4303710882522864, traj.location.y-5.07164599442001, traj.location.z--3.507016162260527)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5284.0)
traj.location = (traj.location.x-0.4328104275955411, traj.location.y-5.044737446037516, traj.location.z--3.5197501894108996)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5337.0)
traj.location = (traj.location.x-0.4352227382732963, traj.location.y-5.0179638383941665, traj.location.z--3.5321739271892625)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5390.0)
traj.location = (traj.location.x-0.4376080202382582, traj.location.y-4.99132679321167, traj.location.z--3.5442930516213096)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5443.0)
traj.location = (traj.location.x-0.43995275917768595, traj.location.y-4.964828067354915, traj.location.z--3.556112833301313)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5496.0)
traj.location = (traj.location.x-0.44228398376799305, traj.location.y-4.938468944686747, traj.location.z--3.5676386103959885)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5549.0)
traj.location = (traj.location.x-0.44458142249277444, traj.location.y-4.912251182072168, traj.location.z--3.5788759237868817)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5602.0)
traj.location = (traj.location.x-0.4468518325338664, traj.location.y-4.886175860658568, traj.location.z--3.5898298413534917)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5655.0)
traj.location = (traj.location.x-0.449095213880355, traj.location.y-4.860243926450153, traj.location.z--3.6005055661189544)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5708.0)
traj.location = (traj.location.x-0.45131156653587823, traj.location.y-4.834457744457836, traj.location.z--3.610908233534417)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5761.0)
traj.location = (traj.location.x-0.45350764766772045, traj.location.y-4.808815625387751, traj.location.z--3.621042911479435)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5814.0)
traj.location = (traj.location.x-0.4556699429413129, traj.location.y-4.7833221641178625, traj.location.z--3.6309146002618604)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5867.0)
traj.location = (traj.location.x-0.4578119667021383, traj.location.y-4.757974657778959, traj.location.z--3.6405280299026685)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5920.0)
traj.location = (traj.location.x-0.4599269617610844, traj.location.y-4.732775809239911, traj.location.z--3.649888268281302)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(5973.0)
traj.location = (traj.location.x-0.4620216853072634, traj.location.y-4.707725618501172, traj.location.z--3.6589999778469746)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6026.0)
traj.location = (traj.location.x-0.464089380166115, traj.location.y-4.682825436996723, traj.location.z--3.6678676859054633)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6079.0)
traj.location = (traj.location.x-0.4661300463230873, traj.location.y-4.658074589009516, traj.location.z--3.6764963251927725)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6132.0)
traj.location = (traj.location.x-0.4681504409745685, traj.location.y-4.633473750257167, traj.location.z--3.6848901527277746)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6185.0)
traj.location = (traj.location.x-0.4701438069278083, traj.location.y-4.609024272173883, traj.location.z--3.693053830959684)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6238.0)
traj.location = (traj.location.x-0.4721236585355655, traj.location.y-4.584724803324775, traj.location.z--3.7009917520506974)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6291.0)
traj.location = (traj.location.x-0.4740697242850729, traj.location.y-4.560577370862688, traj.location.z--3.708708105447954)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6344.0)
traj.location = (traj.location.x-0.47600227568182163, traj.location.y-4.536579947634664, traj.location.z--3.7162072833137643)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6397.0)
traj.location = (traj.location.x-0.477907798391243, traj.location.y-4.512734560793433, traj.location.z--3.7234935426668017)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6450.0)
traj.location = (traj.location.x-0.4797998067588196, traj.location.y-4.489040534620813, traj.location.z--3.730570802667387)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6503.0)
traj.location = (traj.location.x-0.48165802925723256, traj.location.y-4.465496517683164, traj.location.z--3.7374432527626595)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6556.0)
traj.location = (traj.location.x-0.4835027374101628, traj.location.y-4.442105212849128, traj.location.z--3.744114879684332)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6609.0)
traj.location = (traj.location.x-0.485327174046688, traj.location.y-4.41886391724961, traj.location.z--3.7505896025926972)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6662.0)
traj.location = (traj.location.x-0.48713133917044615, traj.location.y-4.395773982319042, traj.location.z--3.756871273076314)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6715.0)
traj.location = (traj.location.x-0.488908475592325, traj.location.y-4.372833380906172, traj.location.z--3.7629636075799624)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6768.0)
traj.location = (traj.location.x-0.4906788548505574, traj.location.y-4.350044815879301, traj.location.z--3.768870390120611)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6821.0)
traj.location = (traj.location.x-0.4924154482432641, traj.location.y-4.327404908652852, traj.location.z--3.7745953371430403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6874.0)
traj.location = (traj.location.x-0.4941452844541345, traj.location.y-4.304915010660466, traj.location.z--3.7801418948054106)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6927.0)
traj.location = (traj.location.x-0.49584133481403114, traj.location.y-4.282574446185663, traj.location.z--3.785513644409434)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(6980.0)
traj.location = (traj.location.x-0.49753738517392776, traj.location.y-4.260381863793782, traj.location.z--3.7907140996846636)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7033.0)
traj.location = (traj.location.x-0.49920640683194506, traj.location.y-4.238338614918803, traj.location.z--3.7957466392176116)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7086.0)
traj.location = (traj.location.x-0.500848399802635, traj.location.y-4.216442672409698, traj.location.z--3.800614506451211)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7139.0)
traj.location = (traj.location.x-0.5024903927806008, traj.location.y-4.194694036265787, traj.location.z--3.8053211475435376)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7192.0)
traj.location = (traj.location.x-0.504098599889403, traj.location.y-4.173091355052861, traj.location.z--3.8098696032221824)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7245.0)
traj.location = (traj.location.x-0.5057000498272828, traj.location.y-4.151635980205697, traj.location.z--3.814263116930249)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7298.0)
traj.location = (traj.location.x-0.5072744710705592, traj.location.y-4.130325208854515, traj.location.z--3.8185047969670904)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7351.0)
traj.location = (traj.location.x-0.5088421351392753, traj.location.y-4.109159040999884, traj.location.z--3.822597684060412)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7404.0)
traj.location = (traj.location.x-0.5103895276988624, traj.location.y-4.088138152359079, traj.location.z--3.8265447513663275)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7457.0)
traj.location = (traj.location.x-0.5119166487347684, traj.location.y-4.0672598400625475, traj.location.z--3.8303488368972296)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7510.0)
traj.location = (traj.location.x-0.5134370125961141, traj.location.y-4.046524779828019, traj.location.z--3.8340129813809654)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7563.0)
traj.location = (traj.location.x-0.5149303477737703, traj.location.y-4.025930944503671, traj.location.z--3.8375398201148414)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7616.0)
traj.location = (traj.location.x-0.5164169257768663, traj.location.y-4.005479685523483, traj.location.z--3.8409322586832104)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7669.0)
traj.location = (traj.location.x-0.5178832322526432, traj.location.y-3.985168300019268, traj.location.z--3.844192999955027)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7722.0)
traj.location = (traj.location.x-0.5193360243902134, traj.location.y-3.964996112272729, traj.location.z--3.8473245440843016)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7775.0)
traj.location = (traj.location.x-0.5207685450113786, traj.location.y-3.944963798002277, traj.location.z--3.8503297290835405)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7828.0)
traj.location = (traj.location.x-0.5221943084507075, traj.location.y-3.9250700057723407, traj.location.z--3.85321091996326)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7881.0)
traj.location = (traj.location.x-0.5236065575554676, traj.location.y-3.905312032714164, traj.location.z--3.855970684448991)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7934.0)
traj.location = (traj.location.x-0.5249917779692623, traj.location.y-3.885691905979911, traj.location.z--3.85861152269473)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(7987.0)
traj.location = (traj.location.x-0.5263769983721431, traj.location.y-3.86620692270003, traj.location.z--3.861135867282627)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8040.0)
traj.location = (traj.location.x-0.5277351900913345, traj.location.y-3.84685708287509, traj.location.z--3.8635459480796897)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8093.0)
traj.location = (traj.location.x-0.5290933818141639, traj.location.y-3.827641035069746, traj.location.z--3.865844197668082)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8146.0)
traj.location = (traj.location.x-0.5304245448351139, traj.location.y-3.8085581035674068, traj.location.z--3.868032913486573)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8199.0)
traj.location = (traj.location.x-0.5317557078669779, traj.location.y-3.789607612650343, traj.location.z--3.870114190258633)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8252.0)
traj.location = (traj.location.x-0.5330598421969626, traj.location.y-3.770788210884234, traj.location.z--3.8720903929948207)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8305.0)
traj.location = (traj.location.x-0.5343639765414991, traj.location.y-3.7520998982693072, traj.location.z--3.8739634812751547)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8358.0)
traj.location = (traj.location.x-0.5356478393587167, traj.location.y-3.733540647653399, traj.location.z--3.875735617394895)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8411.0)
traj.location = (traj.location.x-0.5369181878268137, traj.location.y-3.7151097833191216, traj.location.z--3.877408963649401)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8464.0)
traj.location = (traj.location.x-0.5381750219603418, traj.location.y-3.6968073052669297, traj.location.z--3.878985344475197)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8517.0)
traj.location = (traj.location.x-0.5394250989120337, traj.location.y-3.6786311863445462, traj.location.z--3.880466922167656)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8570.0)
traj.location = (traj.location.x-0.5406616615218809, traj.location.y-3.6605821022697, traj.location.z--3.8818555549492118)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8623.0)
traj.location = (traj.location.x-0.5418847097898833, traj.location.y-3.6426566744555657, traj.location.z--3.883153121313903)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8676.0)
traj.location = (traj.location.x-0.5430942437051272, traj.location.y-3.6248569300543068, traj.location.z--3.8843615132701075)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8729.0)
traj.location = (traj.location.x-0.5442970204458106, traj.location.y-3.6071788147621646, traj.location.z--3.8854825484972793)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8782.0)
traj.location = (traj.location.x-0.545479525673727, traj.location.y-3.5896243557306207, traj.location.z--3.886518017646189)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8835.0)
traj.location = (traj.location.x-0.5466620309089194, traj.location.y-3.5721901743743274, traj.location.z--3.887469670824572)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8888.0)
traj.location = (traj.location.x-0.5478242646131548, traj.location.y-3.5548776221265825, traj.location.z--3.8883392311114804)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8941.0)
traj.location = (traj.location.x-0.5489797411537438, traj.location.y-3.5376839961189717, traj.location.z--3.889128381042916)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(8994.0)
traj.location = (traj.location.x-0.5501217033379362, traj.location.y-3.520608620634107, traj.location.z--3.8898387693690353)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9047.0)
traj.location = (traj.location.x-0.5512501511911978, traj.location.y-3.5036521713896036, traj.location.z--3.890472004296928)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9100.0)
traj.location = (traj.location.x-0.5523785990299075, traj.location.y-3.4868112697981815, traj.location.z--3.8910296940337297)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9153.0)
traj.location = (traj.location.x-0.5534867753594881, traj.location.y-3.4700872672955256, traj.location.z--3.891513372457638)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9206.0)
traj.location = (traj.location.x-0.5545881945181463, traj.location.y-3.4534774610114027, traj.location.z--3.891924566689678)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9259.0)
traj.location = (traj.location.x-0.5556828564949683, traj.location.y-3.4369825266642238, traj.location.z--3.892264776822211)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9312.0)
traj.location = (traj.location.x-0.5567572469626612, traj.location.y-3.4206004371013705, traj.location.z--3.8925354556473692)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9365.0)
traj.location = (traj.location.x-0.557831637426716, traj.location.y-3.4043298408882947, traj.location.z--3.8927380424429394)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9418.0)
traj.location = (traj.location.x-0.5588925135416503, traj.location.y-3.388172089460113, traj.location.z--3.892873931889377)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9471.0)
traj.location = (traj.location.x-0.5599398753111018, traj.location.y-3.372123804230114, traj.location.z--3.8929445106260987)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9524.0)
traj.location = (traj.location.x-0.5609804799205449, traj.location.y-3.3561849851981833, traj.location.z--3.8929511237359047)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9577.0)
traj.location = (traj.location.x-0.5620210845154361, traj.location.y-3.3403556323644352, traj.location.z--3.8928950933272106)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9630.0)
traj.location = (traj.location.x-0.5630346604302758, traj.location.y-3.3246337185772745, traj.location.z--3.89277771380403)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9683.0)
traj.location = (traj.location.x-0.5640482363305637, traj.location.y-3.3090185681189723, traj.location.z--3.8926002501766597)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9736.0)
traj.location = (traj.location.x-0.5650550550744811, traj.location.y-3.2935088295553214, traj.location.z--3.8923639586035232)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9789.0)
traj.location = (traj.location.x-0.5660551166329242, traj.location.y-3.278105854320984, traj.location.z--3.892070041861345)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9842.0)
traj.location = (traj.location.x-0.5670349066786002, traj.location.y-3.262806263829134, traj.location.z--3.8917197162412442)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9895.0)
traj.location = (traj.location.x-0.5680146967279143, traj.location.y-3.247609382362839, traj.location.z--3.8913141237053885)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(9948.0)
traj.location = (traj.location.x-0.5689809724281076, traj.location.y-3.232516561356533, traj.location.z--3.890854446759022)
traj.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(10001.0)
traj.location = (traj.location.x-0.5699404909537407, traj.location.y-3.217523746506572, traj.location.z--3.890341773306943)
traj.keyframe_insert(data_path="location", index =-1)

col = bpy.data.materials.new('Gateway_trajectory')  # Changing the
col.roughness = 1  # color and some texture for the trajectories
col.specular_intensity = 0
col.use_backface_culling = True
bpy.context.object.data.twist_smooth = 0.5
bpy.data.materials['Gateway_trajectory'].use_nodes = True
col.node_tree.nodes.new('ShaderNodeEmission')
node_1 = col.node_tree.nodes["Material Output"]
node_2 = col.node_tree.nodes["Emission"]
col.node_tree.links.new(node_1.inputs["Surface"], node_2.outputs[0])
bpy.data.materials['Gateway_trajectory'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 0, 1, 1)
mesh=bpy.context.object.data
mesh.materials.clear()
mesh.materials.append(col)

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Gateway_trajectory = bpy.data.objects['Gateway_trajectory']
Gateway_trajectory.name = 'GatewayTraj'

print("importing Cone.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Cone.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Cone'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Cone']
ob.select_set(True)
ob.active_material.use_backface_culling = True
for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Cone':
        material.name = 'GatewayCone'
bpy.data.materials['GatewayCone'].node_tree.nodes["Emission"].inputs[0].default_value = (0, 0, 1, 1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.ops.object.select_all(action='DESELECT')  # Move the constraint and rotate it
bpy.data.objects['Cone'].location.x = 2*RATIO
bpy.data.objects['Cone'].location.y = -20.7*RATIO
bpy.data.objects['Cone'].location.z = 0*RATIO
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.ops.transform.rotate(value=1.5707963267948966, orient_axis='X')
bpy.ops.transform.rotate(value=0.0, orient_axis='Y')
bpy.ops.transform.rotate(value=0.0, orient_axis='Z')

bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
Gateway = bpy.data.objects['Gateway']  # exactly the movement of the parent)
Gateway.select_set(True)
Cone = bpy.data.objects['Cone']
Cone.select_set(True)
bpy.context.view_layer.objects.active = Gateway
bpy.ops.object.parent_set(type='OBJECT')

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Cone = bpy.data.objects['Cone']
Cone.name = 'GatewayCone'

print("importing Sphere.blend...")
bpy.ops.object.select_all(action='DESELECT')  # Importing an object (constraint, 
blendfile = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/models/Sphere.blend'  # satellite or planets)
section = '\\Object\\'
obj = 'Sphere'
filepath = blendfile + section + obj                                                           
directory = blendfile + section
filename = obj
bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)
ob = bpy.data.objects['Sphere']
ob.select_set(True)
ob.active_material.use_backface_culling = True
for material in bpy.data.materials:  # Change the color and texture on some constraints
    if material.name == 'Sphere':
        material.name = 'GatewaySphere'
bpy.data.materials['GatewaySphere'].node_tree.nodes["Emission"].inputs[0].default_value = (1, 0, 0, 1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Sphere = bpy.data.objects['Sphere']
Sphere.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1000, RATIO*1000, RATIO*1000))

bpy.ops.object.select_all(action='DESELECT')  # Parent objects (the son will follow 
Gateway = bpy.data.objects['Gateway']  # exactly the movement of the parent)
Gateway.select_set(True)
Sphere = bpy.data.objects['Sphere']
Sphere.select_set(True)
bpy.context.view_layer.objects.active = Sphere
bpy.ops.object.constraint_add(type='COPY_LOCATION')
bpy.context.object.constraints["Copy Location"].target = bpy.data.objects['Gateway']

bpy.ops.object.select_all(action='DESELECT')  # Rename objects in the data of Blender
Sphere = bpy.data.objects['Sphere']
Sphere.name = 'GatewaySphere'

ob = bpy.data.objects['Gateway']
ob.rotation_mode = 'QUATERNION' # Set all the points and the attitude for each satellite
bpy.context.scene.frame_set(0)
ob.location = (0, 0, 0)
ob.keyframe_insert(data_path="location", index =-1)

bpy.context.scene.frame_set(0)
ob.rotation_quaternion = (1.0, -1.0, 0.0, 0.0)
ob.keyframe_insert(data_path="rotation_quaternion", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
OrionPlume = bpy.data.objects['OrionPlume'] # Setting everything to undisplayed here
OrionPlume.select_set(True)
OrionPlume.hide_viewport = False
OrionPlume.hide_render = False
OrionPlume.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPlume.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
OrionTraj = bpy.data.objects['OrionTraj'] # Setting everything to undisplayed here
OrionTraj.select_set(True)
OrionTraj.hide_viewport = False
OrionTraj.hide_render = False
OrionTraj.keyframe_insert(data_path="hide_viewport", index =-1)
OrionTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
OrionSphere1 = bpy.data.objects['OrionSphere1'] # Setting everything to undisplayed here
OrionSphere1.select_set(True)
OrionSphere1.hide_viewport = False
OrionSphere1.hide_render = False
OrionSphere1.keyframe_insert(data_path="hide_viewport", index =-1)
OrionSphere1.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
OrionCone = bpy.data.objects['OrionCone'] # Setting everything to undisplayed here
OrionCone.select_set(True)
OrionCone.hide_viewport = False
OrionCone.hide_render = False
OrionCone.keyframe_insert(data_path="hide_viewport", index =-1)
OrionCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
OrionPoint = bpy.data.objects['OrionPoint'] # Setting everything to undisplayed here
OrionPoint.select_set(True)
OrionPoint.hide_viewport = False
OrionPoint.hide_render = False
OrionPoint.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPoint.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayTraj = bpy.data.objects['GatewayTraj'] # Setting everything to undisplayed here
GatewayTraj.select_set(True)
GatewayTraj.hide_viewport = False
GatewayTraj.hide_render = False
GatewayTraj.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayCone = bpy.data.objects['GatewayCone'] # Setting everything to undisplayed here
GatewayCone.select_set(True)
GatewayCone.hide_viewport = False
GatewayCone.hide_render = False
GatewayCone.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewaySphere = bpy.data.objects['GatewaySphere'] # Setting everything to undisplayed here
GatewaySphere.select_set(True)
GatewaySphere.hide_viewport = False
GatewaySphere.hide_render = False
GatewaySphere.keyframe_insert(data_path="hide_viewport", index =-1)
GatewaySphere.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(0)  # undisplayed to displayed here
OrionPlume = bpy.data.objects['OrionPlume']
OrionPlume.select_set(True)
OrionPlume.hide_viewport = True
OrionPlume.hide_render = True
OrionPlume.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPlume.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(531.0)  # display or not objects and to change camera
OrionPlume = bpy.data.objects['OrionPlume'] # Setting everything to undisplayed here
OrionPlume.select_set(True)
OrionPlume.hide_viewport = False
OrionPlume.hide_render = False
OrionPlume.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPlume.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(531.0)  # display or not objects and to change camera
OrionTraj = bpy.data.objects['OrionTraj'] # Setting everything to undisplayed here
OrionTraj.select_set(True)
OrionTraj.hide_viewport = False
OrionTraj.hide_render = False
OrionTraj.keyframe_insert(data_path="hide_viewport", index =-1)
OrionTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(531.0)  # display or not objects and to change camera
OrionSphere1 = bpy.data.objects['OrionSphere1'] # Setting everything to undisplayed here
OrionSphere1.select_set(True)
OrionSphere1.hide_viewport = False
OrionSphere1.hide_render = False
OrionSphere1.keyframe_insert(data_path="hide_viewport", index =-1)
OrionSphere1.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(531.0)  # display or not objects and to change camera
OrionCone = bpy.data.objects['OrionCone'] # Setting everything to undisplayed here
OrionCone.select_set(True)
OrionCone.hide_viewport = False
OrionCone.hide_render = False
OrionCone.keyframe_insert(data_path="hide_viewport", index =-1)
OrionCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(531.0)  # display or not objects and to change camera
OrionPoint = bpy.data.objects['OrionPoint'] # Setting everything to undisplayed here
OrionPoint.select_set(True)
OrionPoint.hide_viewport = False
OrionPoint.hide_render = False
OrionPoint.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPoint.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayTraj = bpy.data.objects['GatewayTraj'] # Setting everything to undisplayed here
GatewayTraj.select_set(True)
GatewayTraj.hide_viewport = False
GatewayTraj.hide_render = False
GatewayTraj.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayCone = bpy.data.objects['GatewayCone'] # Setting everything to undisplayed here
GatewayCone.select_set(True)
GatewayCone.hide_viewport = False
GatewayCone.hide_render = False
GatewayCone.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewaySphere = bpy.data.objects['GatewaySphere'] # Setting everything to undisplayed here
GatewaySphere.select_set(True)
GatewaySphere.hide_viewport = False
GatewaySphere.hide_render = False
GatewaySphere.keyframe_insert(data_path="hide_viewport", index =-1)
GatewaySphere.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(531.0)  # undisplayed to displayed here
OrionSphere1 = bpy.data.objects['OrionSphere1']
OrionSphere1.select_set(True)
OrionSphere1.hide_viewport = True
OrionSphere1.hide_render = True
OrionSphere1.keyframe_insert(data_path="hide_viewport", index =-1)
OrionSphere1.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(6662.0)  # display or not objects and to change camera
OrionPlume = bpy.data.objects['OrionPlume'] # Setting everything to undisplayed here
OrionPlume.select_set(True)
OrionPlume.hide_viewport = False
OrionPlume.hide_render = False
OrionPlume.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPlume.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(6662.0)  # display or not objects and to change camera
OrionTraj = bpy.data.objects['OrionTraj'] # Setting everything to undisplayed here
OrionTraj.select_set(True)
OrionTraj.hide_viewport = False
OrionTraj.hide_render = False
OrionTraj.keyframe_insert(data_path="hide_viewport", index =-1)
OrionTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(6662.0)  # display or not objects and to change camera
OrionSphere1 = bpy.data.objects['OrionSphere1'] # Setting everything to undisplayed here
OrionSphere1.select_set(True)
OrionSphere1.hide_viewport = False
OrionSphere1.hide_render = False
OrionSphere1.keyframe_insert(data_path="hide_viewport", index =-1)
OrionSphere1.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(6662.0)  # display or not objects and to change camera
OrionCone = bpy.data.objects['OrionCone'] # Setting everything to undisplayed here
OrionCone.select_set(True)
OrionCone.hide_viewport = False
OrionCone.hide_render = False
OrionCone.keyframe_insert(data_path="hide_viewport", index =-1)
OrionCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(6662.0)  # display or not objects and to change camera
OrionPoint = bpy.data.objects['OrionPoint'] # Setting everything to undisplayed here
OrionPoint.select_set(True)
OrionPoint.hide_viewport = False
OrionPoint.hide_render = False
OrionPoint.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPoint.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayTraj = bpy.data.objects['GatewayTraj'] # Setting everything to undisplayed here
GatewayTraj.select_set(True)
GatewayTraj.hide_viewport = False
GatewayTraj.hide_render = False
GatewayTraj.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewayCone = bpy.data.objects['GatewayCone'] # Setting everything to undisplayed here
GatewayCone.select_set(True)
GatewayCone.hide_viewport = False
GatewayCone.hide_render = False
GatewayCone.keyframe_insert(data_path="hide_viewport", index =-1)
GatewayCone.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Creating a step to 
bpy.context.scene.frame_set(0)  # display or not objects and to change camera
GatewaySphere = bpy.data.objects['GatewaySphere'] # Setting everything to undisplayed here
GatewaySphere.select_set(True)
GatewaySphere.hide_viewport = False
GatewaySphere.hide_render = False
GatewaySphere.keyframe_insert(data_path="hide_viewport", index =-1)
GatewaySphere.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(6662.0)  # undisplayed to displayed here
OrionPlume = bpy.data.objects['OrionPlume']
OrionPlume.select_set(True)
OrionPlume.hide_viewport = True
OrionPlume.hide_render = True
OrionPlume.keyframe_insert(data_path="hide_viewport", index =-1)
OrionPlume.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(6662.0)  # undisplayed to displayed here
OrionSphere1 = bpy.data.objects['OrionSphere1']
OrionSphere1.select_set(True)
OrionSphere1.hide_viewport = True
OrionSphere1.hide_render = True
OrionSphere1.keyframe_insert(data_path="hide_viewport", index =-1)
OrionSphere1.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Setting everything that is not 
bpy.context.scene.frame_set(6662.0)  # undisplayed to displayed here
OrionTraj = bpy.data.objects['OrionTraj']
OrionTraj.select_set(True)
OrionTraj.hide_viewport = True
OrionTraj.hide_render = True
OrionTraj.keyframe_insert(data_path="hide_viewport", index =-1)
OrionTraj.keyframe_insert(data_path="hide_render", index =-1)

bpy.ops.object.select_all(action='DESELECT')  # Resizing the object
Camera = bpy.data.objects['Camera']
Camera.select_set(True)
bpy.ops.transform.resize(value=(RATIO*1, RATIO*1, RATIO*1))

bpy.ops.object.select_all(action='DESELECT') # Initialisation of the camera
sat = bpy.data.objects['Orion']
sat.select_set(True)
cam = bpy.data.objects['Camera']
cam.select_set(True)
bpy.context.view_layer.objects.active = Orion
bpy.ops.object.location_clear(clear_delta=False)
bpy.ops.object.rotation_clear(clear_delta=False)
cam.location.x = sat.location.x 
cam.location.y = sat.location.y 
cam.location.z = sat.location.z
bpy.context.scene.frame_set(0)
if 'XY' == 'YZ':
    cam.location.x += 125*RATIO*26.0
elif 'XY' == 'XZ':
    cam.location.y += 125*RATIO*26.0
elif 'XY' == 'XY':
    cam.location.z += 125*RATIO*26.0
    print(RATIO)
elif 'XY' == 'far':
    cam.location.x += 10000*RATIO*10000
cam.keyframe_insert(data_path="location", index =-1)
bpy.context.scene.frame_set(5867.0)
if 'XY' == 'YZ':
    cam.location.x = sat.location.x 
    cam.location.x += 125*RATIO*1
elif 'XY' == 'XZ':
    cam.location.y = sat.location.y 
    cam.location.y += 125*RATIO*1
elif 'XY' == 'XY':
    cam.location.z = sat.location.z
    cam.location.z += 125*RATIO*1
elif 'XY' == 'far':
    cam.location.x += 10000*RATIO*10000
cam.location.x -= sat.location.x 
cam.location.y -= sat.location.y 
cam.location.z -= sat.location.z
cam.keyframe_insert(data_path="location", index =-1)
cons1 = cam.constraints.new(type='COPY_LOCATION')
cons1.target = sat
cam.constraints["Copy Location"].use_offset = True
cons2 = cam.constraints.new(type='TRACK_TO')
cons2.target = sat
cons2.track_axis = 'TRACK_NEGATIVE_Z'
cons2.up_axis = 'UP_Y'
if 'XY' == 'XY':
    cons2.track_axis = 'TRACK_Y'

bpy.context.scene.render.resolution_x = 1920 #initialisation of the screen
bpy.context.scene.render.resolution_y = 1080
#bpy.context.scene.cycles.samples = 64
bpy.context.scene.eevee.taa_render_samples = 64
bpy.context.scene.render.fps = 33.0
bpy.context.scene.eevee.use_bloom = True
my_areas = bpy.context.workspace.screens[0].areas
my_shading = 'RENDERED'  # 'WIREFRAME' 'SOLID' 'MATERIAL' 'RENDERED'
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.shading.type = my_shading
for area in my_areas:
    if area.type == 'VIEW_3D':
        area.spaces[0].region_3d.view_perspective = 'CAMERA'
        break
for area in my_areas:
    for space in area.spaces:
        if space.type == 'VIEW_3D':
            space.overlay.show_overlays = False
bpy.context.scene.frame_current = 0
bpy.context.scene.render.filepath = '/home/ygary/gitlab/sempy-starting-pack/examples_sempy/examples_visualisation/movies/MovieRDV'
bpy.context.scene.render.image_settings.file_format = 'AVI_JPEG'
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.screen.animation_play()