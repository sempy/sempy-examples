# -*- coding: utf-8 -*-
# pylint: disable=line-too-long
"""
Created on Wed Apr 29 16:18:23 2020

@author: Valentin Prudhomme
"""
# __________________________________IMPORTS____________________________________

from sempy.visualisation.global_visualisation import GlobalVisualisation

# ________________________CREATING THE VISUALISATION___________________________
# Creating the visualisation. The user has to choose:
#    - software (str): "VTS" by default. Either "Blender" or "VTS"
#    - render (bool): False by default. If True, it will directly render a movie. If
#              False, it will show a preview of the scene to the user by
#              opening the GUI of the software
#    - config_file_name (str): "config_file" by default. Enter the name of the
#                        configuration file that will be use by the software to
#                        complete de scene. If the file does not exist, it will
#                        be created
#    - remove (bool): True by default. If True, it will remove the configuration file created for the
#                        visualisation after use
#    - data_path (str): Absolute path to the folder with all the data (trajectory, attitude and time files)
#    - time_step (str): ONLY FOR BLENDER. It will take the minimal time step for the time files of every satellites

GV = GlobalVisualisation(software="Blender", render=False,
                         config_file_name="config_file", remove=True,
                         data_path="data",
                         time_step=1)

# ___________________CHOOSING PARAMETERS FOR THE VISUALISATION_________________
# The user can choose different parameters such as:
#     FOR THE SCREEN:
#         - resolution (str): 1920*1080 by default. Resolution of the final
#                       movie (in pixels)
#         - fps (int): 30 by default. Number of Frame Per Second of the final
#                movie
#         - samples (int): 64 by default. If increased, the movie will have a
#                    better look but will take more time to render
#         - global_ratio (int: for example 1/1000): None by default. Ratio by which all distances are multiplied
#         - movie_duration (int): 60sec by default. Given in seconds
#         - movie_name (str): MovieRDV by default
#         - light_orientation (3 int separated with commas): 90,0,30 by default
#                             Initially, the light orientation is from the top to the bottom
#                             of the scene
#         - initially_paused (bool): If False, the preview (on Blender or VTS) will automatically start. If True,
#                             the user will have to start it himself
#         - minimize_broker (bool): ONLY FOR VTS. This allow to have the broker (with the time scale, and some options)
#                            minimized during the visualisation
#
#     FOR THE CAMERA:
#         - plan (str): ONLY FOR BLENDER. Plan of the scene. Either "XY", "XZ" or "YZ for each close view of the
#                 satellite or "far" for a view of all the trajectory. Blender and VTS allow to choose the view with the
#                 pad.
#         - centered_object (str): Name of the object in the center of the view (name of one of the object put in the
#                            scene by the user)
#         - close_view (int): ONLY FOR BLENDER. Distance of the camera from the centered object for the close view
#         - distant_view (int): ONLY FOR BLENDER. Added distance of the camera from the centered object for the far
#                         view. The initial distance is automatically calculated to see all the trajectories of the
#                         scene in the camera. The distant_view parameter will allow to multiply this number by an
#                         other one to have a closer or further view
#         - see_inside_constraint (bool): ONLY FOT BLENDER. If True, when inside a constraint (sphere, cone...), The
#                                  constraint disappear until you don't go out of it
#         - not_to_scale (bool): ONLY FOR BLENDER. If True, the satellites of the scene will not be to scale to see them
#                         all in the camera. The satellites are resize until they arrive to 1 kilometer where the get
#                         there initial size
#         - scale_ratio (int): ONLY FOR BLENDER. Scale ratio if not_to_scale=True. It will multiply the ratio
#                        automatically calculated by the scale_ratio to see the satellites bigger or smaller
#         - time_step (int): ONLY FOR BLENDER. Minimal time step between times in time files (1 second by default)

GV.Screen["resolution"] = "1920*1080"
GV.Screen["fps"] = 60
GV.Screen["samples"] = 64
GV.Screen["global_ratio"] = None
GV.Screen["box_size"] = 2000  # in meters
GV.Screen["movie_duration"] = 300
GV.Screen["light_orientation"] = 90, 0, 30
GV.Screen["initially_paused"] = False

GV.Camera["plan"] = "far"
GV.Camera["centered_object"] = "OrionTraj"
GV.Camera["distant_view"] = 1
GV.Camera["see_inside_constraint"] = False

GV.blendervisu.center = "Orion"
# _____________________________ADDING SATELLITES_______________________________
# If you want to just draw a trajectory without satellite on it, you need to add a satellite without both 3D
# files = None (file3dblender=None AND file3dvts=None)
#
# Mandatory arguments:
#     - name (str): Name of the satellite
#     - file3dblender (str): Name of the 3d file for Blender
#     - file3dvts (str): Name of the 3d file for VTS
#     - trajectoryfile (str): Name of the trajectory file (can be a list)
#     - time_file (str): Name of the time file (can be a list)
#
# Optional arguments:
#     - attitudefile (str): Name of the attitude file (can be a list)
#     - ratio (int): Scale ratio to multiply the size of the satellite in the visualisation by this ratio

GV.add_satellite(name="Orion", file3dblender="Orion.blend", file3dvts="Orion.obj",
                 trajectoryfile="5000_6000_transfert_pos.txt",
                 ratio=2, time_file="5000_6000_transfert_time.txt")

GV.add_satellite(name="NRHO_5000", file3dblender=None, file3dvts=None,
                 trajectoryfile="5000_NRHO_pos.txt")

GV.add_satellite(name="Gateway", file3dblender="Gateway.blend", file3dvts="Gateway.obj",
                 trajectoryfile="6000_NRHO_pos.txt",
                 attitudefile=[1, -1, 0, 0],
                 time_file="6000_NRHO_time.txt", ratio=1)

# _______________________________ADDING PLANETS________________________________
# Mandatory arguments:
#     - name (str): Name of the planet (str)
# Extra mandatory arguments for Blender:
#     - position_file (List of int): Position (list of 3 int): Only 1 position
#                                    possible for the moment

GV.add_planet(name="Earth", position_file=[0, 0, 0])
GV.add_planet(name="Moon", position_file=[384400, 0, 0])

# __________________ADDING CONSTRAINTS TO THE SATELLITES_______________________
# Mandatory arguments:
#     - satellite_name (str): Name of the satellite to which the constraint is
#                             attached to
#     - category (str): Category of the constraint ("Plume", "Sphere",
#                                                   "Trajectory", "Point", "Cone")
#     - label (str): Label of the constraint
# Optional arguments:
#     - display (bool): If True, this constraint will be displayed during this
#                render, else, it will not appear
#     - color (str): "red", "orange", "yellow", "sky blue", "blue", "pink",
#              "purple" or "green"
#     - pos_rot (List of 6 int): Position and rotation of the constraint in the
#                                satellite coordinates (list of 6 int)
#     - size of the constraint (int): useful for the sphere (defines the
#                               radius) and for the trajectory (defines the
#                               depth)
#     - active (bool): ONLY FOR TRAJECTORIES. IF True, it draws the trajectory step by step during the visualisation

GV.add_constraint(satellite_name="Orion",
                  category="Trajectory", label="OrionTraj",
                  display=True, color="yellow", size=1, active=True)

GV.add_constraint(satellite_name="NRHO_5000", category="Trajectory",
                  label="transfert_NRHO", display=True, color="green", size=1, active=False)

GV.add_constraint(satellite_name="Gateway", category="Trajectory",
                  label="GatewayTraj", display=True, color="blue", size=1, active=False)

# __________________________START THE VISUALISATION____________________________
# Start the visualisation of either Blender or VTS

GV.start()

