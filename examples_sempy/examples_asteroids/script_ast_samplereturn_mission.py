# -*- coding: utf-8 -*-
"""

Asteroid Ryugu round-trip mission analysis.
=======================================================

This example demonstrates an asteroid sample-return mission analysis for
asteroid Ryugu, visited by the Hayabusa2 spacecraft in 2018.
This example makes use of the `AsteroidRoundtripMission` class to compute
optimal trajectories (using Lambert's problem solutions) between Earth and the
asteroid in the given time span of launch and total mission duration.

@author: Irina KOVALENKO
"""

# %%
# 1) Import all necessary modules and classes.
#

import spiceypy as spice

from sempy.core.asteroids.asteroid_roundtrip_mission import AsteroidRoundtripMission
from sempy.core.init.load_kernels import load_kernels


# %%
# 2) Initialize Mission analysis object
#
# Create an AsteroidRoundtripMission object, setting the name of the target
# asteroid - Ryugu (SPK-ID: 2162173), the launch period - from 2020-01-01 to
# 2022-01-01, the mission duration limit to 400 days, the
# 12 km/s at an altitude of 125 km, and the altitude of the parking LEO 400 km.
#

ryugu_mission = AsteroidRoundtripMission('2162173',
                                         ['2020-01-01', '2021-01-01'],
                                         365, stay=8, leo_alt=400,
                                         step_dpr=7, step_tof=10,
                                         earth_reentry=12)


# %%
# 3) Load necessary SPICE kernels. Asteroid's SPICE kernels (SPK file) can be
# downloaded from https://ssd.jpl.nasa.gov/x/spk.html. SPICE kernels have to
# cover the launch period + maximim mission duration time span. Provide a path
# to the kernel file if not executing in the same folder.
#

load_kernels()
spice.furnsh('2162173.bsp')

# %%
# 4) To reduce the CPU time to calculate all solutions for a given departure
# date and time-of-flight ranges, asteroid_roundtrip_mission module usses
# parallel computing. For this procedure, all the state vectors of the Earth
# and asteroid for each combination of departure date / TOF are loaded from
# SPICE ephemerides, and stored in the list 'data_organized' (use
# 'eph_file = True' for this).
# Optionally, it can also be stored in a data file './data_preparation.data'.
# After this operation, SPICE kernels can be unloaded.


ryugu_mission.data_preparation()

# Unload SPICE kernels
spice.kclear

# %%
# 5) Compute all solutions for the given departure dates (with step 'step_dpr')
# and TOF ranges (with step 'step_tof'). These
# solutians are Lamber's problem solutions (for more details see documentation
# for 'lambert_solution' module).

# Optionally, use "to_file=True" to store all computed trajectories in a file.
ryugu_mission.mission_analysis_solver(to_file=True)

ryugu_mission.results_combination()

# %%
# 6) Compute the changes in velocity (delta-V) required to leave Earth's
# parking LEO, arrive at the asteroid, and the total delta-V.

ryugu_mission.delta_v_calc()


# %%
# 7) Optimal solution
# Calculates optimal launch opportunities by minimizing the total ΔV. The total
# delta-v includes manoeuvres of insertion from the parcking LEO (if
# specified), delta-v to match the asteroids velocity and to depart from the
# asteroid, and an Earth atmosphere reentry manoeuvre if required.

ryugu_mission.optimal_dv_solution()

# %%
# 8) Plot outbound (Earth-to-asteroid) and inbound (asteroid-to-Earth) mission
# delta-V as functions of departure date and outbound/inbound path
# durations (so-called pork-chop plot). This visualisation allows to analyse
# conditions for otimal delta-v launch opportunities.

ryugu_mission.porkchop_plot_outbound_dV()
ryugu_mission.porkchop_plot_inbound_dV()
