# -*- coding: utf-8 -*-
"""

Asteroid Ryugu rendez-vous mission analysis.
=======================================================

This example demonstrates an asteroid rdv mission analysis for asteroid Ryugu,
visited by the Hayabusa2 spacecraft in 2018.
This example makes use of the `AsteroidRdvMission` class to compute optimal
trajectories (using Lambert's problem solutions) between Earth and the asteroid
in the given time span of launch and total mission duration.

@author: Irina KOVALENKO
"""

# %%
# 1) Import all necessary modules and classes.
#

import spiceypy as spice

from sempy.core.asteroids.asteroid_rdv_mission import AsteroidRdvMission
from sempy.core.init.load_kernels import load_kernels


# %%
# 2) Initialize Mission analysis object
#
# Create an AsteroidRdvMission object, setting the name of the target
# asteroid - Ryugu (SPK-ID: 2162173), the launch period - from 2020-01-01 to
# 2025-01-01, the mission duration the range - from 10 to 365 days, and the
# altitude of the parking LEO 200 km.
#

ryugu_mission = AsteroidRdvMission('2162173',  ['2020-01-01', '2025-01-11'],
                                   [10, 365], lea_alt=200,
                                   step_dpr=15, step_tof=10)


# %%
# 3) Load necessary SPICE kernels. Asteroid's SPICE kernels (SPK file) can be
# downloaded from https://ssd.jpl.nasa.gov/x/spk.html. SPICE kernels have to
# cover the launch period + maximim mission duration time span. Provide a path
# to the kernel file if not executing in the same folder.
#

load_kernels()
spice.furnsh('2162173.bsp')


# %%
# 4) To reduce the CPU time to calculate all solutions for a given departure
# date and time-of-flight ranges, asteroid_rdv_mission module usses parallel
# computing. For this procedure, all the state vectors of the Earth and
# asteroid for each combination of departure date / TOF are loaded from
# SPICE ephemerides, and stored in the list 'data_organized' (use
# 'eph_file = True' for this).
# Optionally, it can also be stored in a data file './data_preparation.data'.
# After this operation, SPICE kernels can be unloaded.


ryugu_mission.data_preparation()

# Unload SPICE kernels
spice.kclear

# %%
# 5) Compute all solutions for the given departure dates (with step 'step_dpr')
# and TOF ranges (with step 'step_tof'). These
# solutians are Lamber's problem solutions (for more details see documentation
# for 'lambert_solution' module).

ryugu_mission.mission_analysis_solver(ryugu_mission.data_organized)

# %%
# 6) Compute the changes in velocity (delta-V) required to leave Earth's
# parking LEO, arrive at the asteroid, and the total delta-V.

ryugu_mission.delta_v_calc()

# %%
# 7) Plot total mission delta-V as a function of departure date and mission
# duration (so-called pork-chop plot). This visualisation allows to analyse
# conditions for otimal delta-v launch opportunities.
# Optionnaly, similar plots can be produced for characteristic energy (C3),
# departure and arrival V_infinity (velosity relative to Earth and to asteroid,
# respectively). Use porkchop_plot_C3_dep(), porkchop_plot_V_inf_dep(),
# porkchop_plot_V_inf_arr() for this.
#

ryugu_mission.porkchop_plot_total_dV()

# %%
# 8) Optimal solutions
# Calculate optimal launch opportunities

# - minimizing the total ΔV

ryugu_mission.optimal_dv_solution()

# - minimizing the departure C3

ryugu_mission.optimal_c3_departure_solution()

# - minimum arrival V-infinity

ryugu_mission.optimal_v_inf_arrival_solution()

# - minimum time of flight

ryugu_mission.optimal_tof_solution()
