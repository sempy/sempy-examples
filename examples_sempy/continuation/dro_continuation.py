# pylint: disable=invalid-name

"""
Computation of an entire family of Distant Retrograde Orbits (DROs) around the first or the
second primary of a CR3BP via continuation methods.

@author: Paolo GUARDABASSO

Created: 24/02/2021
"""

# %% Imports
from time import perf_counter
from copy import deepcopy
from warnings import simplefilter
from tqdm import tqdm, TqdmWarning

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO
from sempy.core.init.constants import DAYS2SEC


# %% Functions

def compute_kepler_state(alt_pdim_start, _center, _cr3bp):
    """ Computes the state vector of a circular 2-body problem planar orbit"""
    radius_center = _center.value[0]
    mu_center = _center.value[3]
    radius = alt_pdim_start + radius_center
    if _center == cr3bp.m1:
        x = _cr3bp.m1_pos[0] - radius / _cr3bp.L
    else:
        x = _cr3bp.m2_pos[0] - radius / _cr3bp.L
    adim_coeff_speed = 2 * np.pi * _cr3bp.L / _cr3bp.T
    v_y = np.sqrt(mu_center / radius) / adim_coeff_speed
    return np.array([x, 0, 0, 0, v_y, 0])


# %% Initialisation and environment definition

# save or not the results
save_abacus = False
max_orbits = 2000

# define environment (EM system)
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# Choice of central body of DROs
center = cr3bp.m1

if center == cr3bp.m2:
    central_primary = 'm2'
    other_primary = 'm1'
    per_alt_lim = 7000  # TODO Only valid for Moon DRO, find a better estimation
else:
    central_primary = 'm1'
    other_primary = 'm2'
    per_alt_lim = 0

# Creation of initial guesses (using Keplerian circular motion approximation)
periapsis_alt_1 = 500  # km
delta_h = 1000  # km
state0_guess_1 = compute_kepler_state(periapsis_alt_1, center, cr3bp)
state0_guess_2 = compute_kepler_state(periapsis_alt_1 + delta_h, center, cr3bp)

t0 = perf_counter()

# %% Orbit initialisation and computation
dro_1 = DRO(cr3bp, center, state0=state0_guess_1)
dro_2 = DRO(cr3bp, center, state0=state0_guess_2)

dro_1.computation()
dro_2.computation()

# %% Family continuation

# init continuation process
orbits = [deepcopy(dro_1)]  # list to be filled with all computed orbits

# format progress bar
simplefilter('ignore', TqdmWarning)
desc = dro_1.get_abacus_name(separator=' ')
pbar = tqdm(total=np.ceil(getattr(dro_1, other_primary + '_apsis')['periapsis_altitude_dim']),
            desc=desc)

count = 0

while True:
    # perturbation of the initial state
    state0_guess = dro_2.state0 + (dro_2.state0 - dro_1.state0)

    dro_1 = deepcopy(dro_2)
    dro_2 = DRO(cr3bp, center, state0=state0_guess)
    dro_2.computation()

    if getattr(dro_2, other_primary + '_apsis')['periapsis_altitude_dim'] > 0:
        orbits.append(deepcopy(dro_2))
        pbar.update(np.ceil(getattr(orbits[-2], other_primary + '_apsis')['periapsis_altitude_dim'])
                    - np.ceil(getattr(dro_2, other_primary + '_apsis')['periapsis_altitude_dim']))
        count += 1
    else:
        break
    if count > max_orbits:
        break

pbar.close()
tf = perf_counter()
print(f"\nElapsed time for abacus generation: {(tf - t0)} s")

# %% extract data to be stored in the abacus
state0_vec = [o.state0 for o in orbits]  # initial state
ax_vec = [o.Ax for o in orbits]  # vertical extension
rp_vec = [getattr(o, central_primary + '_apsis')['periapsis_radius'] for o in orbits]  # periselene
x0_vec = [o.state0[0] for o in orbits]
vy0_vec = [o.state0[4] for o in orbits]
cjac_vec = [o.C for o in orbits]  # Jacobi constant
period_vec = [o.T for o in orbits]  # orbit period
stability_idx_vec = [o.stability_idx for o in orbits]  # stability indexes (not stored)

if save_abacus:
    orbit = DRO(cr3bp, dro_1.center, dro_1.family, Azdim=ax_vec[0])
    orbit.save_abacus(state0_aba=state0_vec, ax_aba=ax_vec, rp_aba=rp_vec, cjac_aba=cjac_vec,
                      period_aba=period_vec)

# %% plot the family highlighting the DROs
fig1, ax1 = plt.subplots(1, 1)
ax1.axis('equal')

fig1.suptitle(desc + ' family in rotating reference frame')

# Plot Moon and Earth
prim_1 = plt.Circle((cr3bp.m1_pos[0], 0), cr3bp.R1 / cr3bp.L, facecolor='b', edgecolor='k',
                    linewidth=.25)
prim_2 = plt.Circle((cr3bp.m2_pos[0], 0), cr3bp.R2 / cr3bp.L, facecolor='gray', edgecolor='k',
                    linewidth=.25)
ax1.add_artist(prim_1)
ax1.add_artist(prim_2)

for dro in orbits[::20]:
    ax1.plot(dro.state_vec[:, 0], dro.state_vec[:, 1], color='tab:orange')
    ax1.scatter(dro.state_vec[0, 0],
                dro.state_vec[0, 1],
                color='b', zorder=9, s=3)  # Initial state
ax1.set_xlabel('x')
ax1.set_ylabel('y')

# %% write parameters vectors
rp_vec_dim = np.asarray(rp_vec) * cr3bp.L  # periapsis radius in km
x0_vec_dim = np.asarray(x0_vec) * cr3bp.L  # periapsis radius in km
vy0_vec_dim = np.asarray(vy0_vec) * 2 * np.pi * cr3bp.L / cr3bp.T  # periapsis radius in km
ax_vec_dim = np.asarray(ax_vec) * cr3bp.L  # vertical extension in km
stb_idx_vec = np.asarray(stability_idx_vec)  # stability indexes


# %% Plot parameters
def plot_params(x, y, y1=None, x_label='', y_label='', title='', stability=False):
    """ Function that plots family properties """
    _, ax = plt.subplots()
    ax.plot(x, y, 'b')
    if y1 is not None:
        ax.plot(x, y1, 'r')
    if stability:
        y2 = np.ones(np.size(x))
        ax.plot(x, y2, 'k')
        ax.plot(x, -y2, 'k')
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.grid()


plot_params(x0_vec_dim, cjac_vec, x_label='X0 [km]', y_label='Cjac [-]',
            title='Jacobi constant vs x of initial state')
plot_params(x0_vec_dim, np.asarray(period_vec) * (cr3bp.T / 2.0 / np.pi) / DAYS2SEC,
            x_label='x_0 [km]', y_label='T [days]', title='Orbit period vs x of initial state')
plot_params(x0_vec_dim, vy0_vec_dim,
            x_label='x_0 [km]', y_label='vy_0', title='vy of initial state vs x of initial state')
plot_params(x0_vec_dim, ax_vec_dim,
            x_label='x_0 [km]', y_label='Ax [km]', title='Planar extension vs x of initial state')
plot_params(x0_vec_dim, rp_vec_dim, x_label='x_0 [km]', y_label='Rp [km]',
            title='Periapsis radius vs x of initial state')
plot_params(x0_vec_dim, stb_idx_vec[:, 0], x_label='x_0 [km]',
            y_label='stability index [-]', title='Stability indexes vs x of initial state',
            stability=True)
