"""
Continuation of the EML-2 southern Halo and Butterfly families.

@author: Alberto FOSSA - Giordana BUCCHIONI'
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from copy import deepcopy
from warnings import simplefilter
from tqdm import tqdm, TqdmWarning


from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.halo import Halo
from sempy.core.orbits.nrho import NRHO
from sempy.core.orbits.butterfly import Butterfly
from sempy.core.diffcorr.diff_corr_3d import DiffCorr3D
from sempy.core.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from sempy.core.utils.pickle_encoder import load_pickle, save_pickle
from sempy.core.__init__ import DIRNAME


def halo_continuation(_cr3bp, _li, _fam, _az0_dim, _rpf_dim, _delta_s):
    """Continuation of the EML-2 Halo orbit family. """

    _halo = Halo(_cr3bp, _li, _fam, Azdim=_az0_dim)  # initial Halo (will be overwritten)
    _halo.computation(fix_dim=Halo.DiffCorrFixDim.z0)  # correction (x0, vy0) free
    _halo.computation(fix_dim=Halo.DiffCorrFixDim.min_norm)  # correction (x0, z0, vy0, T12) free
    _rp0 = _halo.m2_apsis['periapsis_radius']  # initial (max) periselene radius
    _rpf = _rpf_dim / _cr3bp.L  # final (min) periselene radius

    _diff_corr = DiffCorr3D(_cr3bp.mu, max_internal_steps=8_000_000)  # DC instance
    _orbs = [deepcopy(_halo)]  # list of Halos
    _bar = tqdm(total=1.0, desc=_halo.get_abacus_name(separator=' '))  # progress bar in (0, 1)

    while _delta_s > 1e-8 and _halo.m2_apsis['periapsis_radius'] > _rpf:
        _halo.state0[::2] += _delta_s * _halo.continuation['null_vec'][:3, 0]  # update state
        _halo.T12 += _delta_s * _halo.continuation['null_vec'][3, 0]  # update period
        _halo.state0, _halo.T12, _halo.T, _halo.continuation, _ = \
            _diff_corr.diff_corr_3d_cont(_halo.state0, _halo.T12, _delta_s, _halo.continuation)
        _halo.postprocess()  # postprocessing (Jacobi constant, stability characteristics, ...)
        if _halo.m2_apsis['apoapsis_radius'] < 1.0:  # good orbit
            _orbs.append(deepcopy(_halo))  # add Halo to list
            _up = (_halo.m2_apsis['periapsis_radius'] - _orbs[-2].m2_apsis['periapsis_radius']) \
                / (_rpf - _rp0)  # update progress bar
            _bar.update(_up)
        else:  # bad orbit, back one step
            _halo = _orbs[-1]
            _delta_s *= 0.1
    _bar.close()
    return _orbs


def plot_params(_x, _y, _y1=None, _x_label='', _y_label='', _title='', _stability=False):
    """Plot the orbit parameters across the family."""
    _, _ax = plt.subplots()
    _ax.plot(_x, _y, 'b')
    if _y1 is not None:
        _ax.plot(_x, _y1, 'r')
    if _stability:
        _ax.plot(_x, np.ones(np.size(_x)), 'k')
        _ax.plot(_x, -np.ones(np.size(_x)), 'k')
    _ax.set_xlabel(_x_label)
    _ax.set_ylabel(_y_label)
    _ax.set_title(_title)
    _ax.grid()


# %% definition of the EM system
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
t_c = cr3bp.T / 2.0 / np.pi  # characteristic time [s]

# %% input parameters
li = cr3bp.l2  # libration point L2
fam = Halo.Family.southern  # southern Halo family
az0_dim = 10.0  # vertical extension of the first computed Halo [km]
rpf_dim = 1000.0  # minimum periselene radius (inside the Moon to improve interpolation) [km]
per_max = 45. * 86400. / t_c  # maximum period, stopping condition for butterfly continuation
delta_s = 0.001  # continuation stepsize
nb_steps = 2001  # number of time steps in butterfly orbits state vectors

use_stored_halos = False  # if True use stored list of halos
use_stored_btfly = False  # if True use stored list of butterflies
fn_halos = 'eml2_halos.pkl'  # binary file in which the list of halos is stored
fn_butterflies = 'eml2_butterflies.pkl'  # binary file in which the list of butterflies is stored
dn_pkl = os.path.join(os.path.split(DIRNAME)[0], 'examples', 'continuation')  # directory for .pkl
plot_step = 50  # step at which successive orbits are displayed
plot_per_max = 20. * 86400. / t_c  # maximum period when plotting butterflies together with Halos

# %% continuation of the Halo family
simplefilter('ignore', TqdmWarning)
if use_stored_halos:
    halos = load_pickle(os.path.join(dn_pkl, fn_halos))
else:
    halos = halo_continuation(cr3bp, li, fam, az0_dim, rpf_dim, delta_s)
    # save_pickle(halos, os.path.join(dn_pkl, fn_halos))

# %% NRHOs extraction (projection of the periselene onto the x-axis falls inside the Moon)
nrhos, constr = [], [o.m2_apsis['periapsis_position'][0] for o in halos]
for i, o in enumerate(halos):
    if np.abs(constr[i] - cr3bp.m2_pos[0]) < cr3bp.m2.Rm / cr3bp.L:
        nrhos.append(o)

# %% NRHOs parameters extraction
state0_vec = np.asarray([o.state0 for o in nrhos])  # initial state
az_vec = np.asarray([o.Az for o in nrhos])  # vertical extension
rp_vec = np.asarray([o.m2_apsis['periapsis_radius'] for o in nrhos])  # periselene radius
cjac_vec = np.asarray([o.C for o in nrhos])  # Jacobi constant
per_vec = np.asarray([o.T for o in nrhos])  # orbit period
stb_idx_vec = np.asarray([o.stability_idx for o in nrhos])  # stability indexes

# %% alpha, beta parameters of Brook stability diagram
# period doubling bifurcation for f(alpha, beta) = 2 + beta - 2 * alpha = 0
alpha_vec = 2.0 - np.asarray([o.monodromy.trace() for o in nrhos])
beta_vec = 0.5 * (alpha_vec * alpha_vec + 2.0 -
                  np.asarray([np.linalg.matrix_power(o.monodromy, 2).trace() for o in nrhos]))

# %% NRHO parameters interpolation for first guess of the bifurcating orbit
x_interp = np.arange(az_vec.size, dtype=np.float64)
f_interp = CubicSpline(x_interp, 2. + beta_vec - 2. * alpha_vec, extrapolate=False)
f_roots = f_interp.roots()
g_interp = [CubicSpline(x_interp, o, extrapolate=False) for o in (state0_vec, per_vec)]
state0_interp, per_interp = [o(f_roots[f_roots.size - 1]) for o in g_interp]

# %% initialization and (double) correction of the bifurcating NRHO

# initialization and correction at periselene with (x0, z0, vy0, T12) free
nrho_bif = NRHO(cr3bp, li, fam, state0=state0_interp, T=per_interp)
nrho_bif.computation(NRHO.DiffCorrFixDim.min_norm)

# propagation to aposelene
prop = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=None)
_, state_apo, _, _ = prop.propagate([0.0, nrho_bif.T12], nrho_bif.state0)
state_apo = state_apo[-1]  # NRHO state at aposelene
state_apo[1::2] = 0.0  # reset spurious non-zero components (y0, vx0, vz0)

# correction at aposelene over an entire period
nrho_bif = NRHO(cr3bp, li, fam, state0=state_apo, T=2.0 * nrho_bif.T)
nrho_bif.computation(NRHO.DiffCorrFixDim.min_norm)

# %% compact singular value decomposition (SVD) of the Jacobian matrix at the last iteration.
# Only the non-zero singular values and the corresponding right-singular vectors of the input
# matrix are returned. In this case:
# dfx_mat: 3x4 input matrix with rank 3
# u_mat:   3x3 square matrix whose columns are the left-singular vectors of dfx_mat
# s_val:   1x3 array whose elements are the non-zero singular values of dfx_mat
# vh_mat:  3x4 rectangular matrix whose rows are the right-singular vectors of dfx_mat
#          corresponding to its non-zero singular values
# In this case the SVD is computed such that u_mat @ np.diag(s_val) @ vh_mat = dfx_mat.
# In its full form, the SVD would have returned vh_mat as a 4x4 square matrix with the first three
# rows equivalent to the above and the last one containing the right-singular vector corresponding
# to the zero singular value of dfx_mat. This vector constitutes the null space of dfx_mat.
# Note that in both cases singular values equal to zero are omitted from s_val which has always
# a dimension equal to the rank of the input matrix dfx_mat.

u_mat, s_val, vh_mat = np.linalg.svd(nrho_bif.continuation['dfx_mat'], full_matrices=True)

# modified null vector and first guess for butterfly initial state.
# The sign ambiguity of the modified null vector is solved forcing its first entry to be positive
# such that the x component of the butterfly's initial state will be slightly larger than the
# x component at the aposelene of the bifurcating NRHO (i.e. the butterfly's initial state is
# behind the NRHO as seen from either the Moon or the Earth)
null_vec_mod = np.sign(vh_mat[np.argmin(s_val), 0]) * \
               vh_mat[np.argmin(s_val)].reshape(nrho_bif.continuation['null_vec'].shape)
cont_bif = {'g_vec': nrho_bif.continuation['g_vec'], 'null_vec': null_vec_mod}
state0_bif = nrho_bif.state0.copy()
state0_bif[::2] += delta_s* cont_bif['null_vec'][:3, 0]
t12_bif = nrho_bif.T12 + delta_s* cont_bif['null_vec'][-1, 0]

# %% correction of the first butterfly orbit
diff_corr = DiffCorr3D(cr3bp.mu, max_internal_steps=8_000_000)

# correction with modified continuation to jump from the NRHO to the butterfly family

state0b, T12b, T, g_vec, null_vec, flag= \
   diff_corr. diff_corr_3d_purdue(state0_bif, t12_bif, 0)


# correction to obtain the direction of continuation for the butterfly family
lib = cr3bp.l2  # libration point L2
famb = Butterfly.Family.southern  # southern Butterfly family
state0_bif, t12_bif, t_bf, cont_bif = diff_corr.diff_corr_3d_full(state0_bif, t12_bif)
btfly = Butterfly(cr3bp,lib, famb, state0 = state0_bif,T=t12_bif*2) #Initialization of the first butterfly
btfly.computation(btfly.state0, btfly.T)
btfly.continuation = cont_bif
btfly.postprocess(nb_steps)

# %% continuation procedure : The first
btfly.fam_continuation(delta_sb=delta_s,n_orbits=1000,n_dysplay =10, save_abacus= False)
