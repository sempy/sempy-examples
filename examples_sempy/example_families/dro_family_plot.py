"""
Interpolation of the entire abacuses of Earth and Moon DRO families.

@author: Paolo GUARDABASSO
"""

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO
from sempy.core.plotting.util import sphere, set_axes_equal
from sempy.core.plotting.simple.utils import decorate_3d_axes


def set_axes_lim_labels_title(ax, title):
    """Convenience function to set the axes limits, labels and the figure title. """
    ax.set_xlabel('x [-]')
    ax.set_ylabel('y [-]')
    ax.set_zlabel('z [-]')
    ax.set_title(title)
    set_axes_equal(ax)


# %% environment
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

mi_list = [cr3bp.m1, cr3bp.m2]
dros_all = [[] for i in range(len(mi_list))]

for j, mi in enumerate(mi_list):
    # load abacus
    dro = DRO(cr3bp, mi, T=1)     # dummy orbit to get abacus data
    x0, ax, rp, cjac, per = dro.load_abacus()

    param_vec = np.linspace(ax[0], ax[-1]*0.99, 30)

    # %% interpolate Halo family
    for i, param in enumerate(param_vec):
        dros_all[j].append(DRO(cr3bp, mi, Axdim=param*cr3bp.L))
        dros_all[j][-1].interpolation()

# %% Plot one family (with axes)

# Settings
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.serif"] = ["Times New Roman"] + plt.rcParams["font.serif"]
plt.rcParams["font.size"] = "22"
plt.rcParams['xtick.major.pad'] = '18'
plt.rcParams['ytick.major.pad'] = '18'

fig4 = plt.figure(figsize=(8, 7))
ax4 = fig4.add_subplot(projection='3d')
ax4.view_init(elev=35., azim=-130)

# Choose the libration point and family
mi = 2
south = 1
cmap = plt.cm.get_cmap('viridis_r', len(dros_all[mi-1]))

for i, dro in enumerate(dros_all[mi-1]):
    ax4.plot(dro.state_vec[:, 0]*cr3bp.L, dro.state_vec[:, 1]*cr3bp.L, dro.state_vec[:, 2]*cr3bp.L,
             color=cmap(i), zorder=2, linewidth=2)

ax4.set_xlabel('x [km]', labelpad=32)
ax4.set_ylabel('y [km]', labelpad=32)
ax4.set_zlabel('z [km]', labelpad=32)

# Plot Moon
x_2, y_2, z_2 = sphere(100, cr3bp.R2, np.array(cr3bp.m2_pos)*cr3bp.L)
ax4.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='grey', linewidth=0)

# Plot Earth
x_1, y_1, z_1 = sphere(100, cr3bp.R1, np.array(cr3bp.m1_pos)*cr3bp.L)
ax4.plot_surface(x_1, y_1, z_1, rstride=4, cstride=4, color='tab:blue', linewidth=0)

# Plot libration points
pos_l1 = np.array(cr3bp.l1.position)
pos_l2 = np.array(cr3bp.l2.position)
# ax3.scatter(pos_l1[0]*cr3bp.L, 0, 0, marker='*', color='b')
ax4.scatter(cr3bp.m2_pos[0]*cr3bp.L, 0, 0, marker='.', color='grey', label='Moon', s=100)
ax4.scatter(cr3bp.m1_pos[0]*cr3bp.L, 0, 0, marker='.', color='tab:blue', label='Earth', s=100)
# ax4.scatter(pos_l2[0]*cr3bp.L, 0, 0, marker='*', color='r', label='EML$_2$')
# decorate_3d_axes(ax4, '', 'km')
set_axes_equal(ax4)
ax4.legend(loc='upper right', bbox_to_anchor=(1, 1), fancybox=True, shadow=True)
# fig4.tight_layout()
