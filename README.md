# DEPRECATED, working only with [legacy support](https://gitlab.isae-supaero.fr/sempy/sempy-src/-/tree/legacy_retrocompatibility_v1.0)

For newest version, check-out official [gitlab sempy](https://gitlab.com/EBlazquez/sempy)

# SEMpy starting pack: guide

@author: Emmanuel BLAZQUEZ, Yvan GARY

## Introduction

This guide is meant to help getting you started with SEMpy's starting pack.

## 1. Contents of the Starting Pack

* SEMpy documentation

* Examples to get started with the library

## 2. Accessing SEMpy's documentation

To access the documentation index, go to: 

''docs/html/index.html''

And open the file with your favorite brwoser (Chrome or Chromium recommended)

