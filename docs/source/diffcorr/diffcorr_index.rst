Differential Correction Subpackage
===================================

.. toctree::
   :maxdepth: 1

Single Shooting Module
----------------------

.. automodule:: sempy.core.diffcorr.diff_corr_3d
   :members:
   :show-inheritance:
   :inherited-members:

Multiple Shooting Module
------------------------

.. automodule:: sempy.core.diffcorr.multiple_shooting
   :members:
   :show-inheritance:
   :inherited-members:

Events Module
-------------

.. automodule:: sempy.core.diffcorr.ode_event
   :members:
   :show-inheritance:
   :inherited-members:
