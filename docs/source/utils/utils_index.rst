Utils Subpackage
================


Load and Encode data
--------------------

.. toctree::
    :maxdepth: 1

.. automodule:: sempy.core.utils.json_encoder
    :members:
    :inherited-members:

.. automodule:: sempy.core.utils.load_mat
    :members:
    :inherited-members:

.. automodule:: sempy.core.utils.pickle_encoder
    :members:
    :inherited-members:


Decomment a csv file
---------------------

.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.utils.csvdecomment
   :members:
   :show-inheritance:
   :inherited-members:
