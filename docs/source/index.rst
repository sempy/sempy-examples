.. sempy documentation master file, created by
   sphinx-quickstart on Fri Jul  5 11:12:31 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.




Sempy's remote repository: `sempy <https://openforge.isae.fr/projects/sempy/repository>`_

Welcome to Sempy's documentation!
=================================

**Sempy**, for Sun-Earth-Moon system in Python, is an astrodynamics package developed in Python for research and mission analysis focused in Circular Restricted Three Body Problem (CRTBP) and Fourth Body Problem in the Earth-Moon and Sun-Earth systems.

Sempy address the CRTBP in such a way that allows to perform the design of CRTBP periodic orbits according to parameters that uniquely identify these orbits for a given CRTBP system.  Not only that, some of sempy’s modules or classes can be used independently for other applications or other orbital mechanics packages/subpackage.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   asteroids/asteroids_index
   initialization/init_index
   orbits/orbits_index
   dynamics/dynamics_index
   propagation/propagation_index
   diffcorr/diffcorr_index
   gravity/gravity_index
   lambert/lambert_index
   crtbp/crtbp_index
   coc/coc_index
   utils/utils_index
   plotting/plotting_index
   visualisation/visualisation_index
   optimal_control/optimal_control_index
   examples/orbit_generation/index
   examples/ephemeris/index
   examples/lambert/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
