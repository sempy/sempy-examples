Lambert Subpackage
==================
.. toctree::
   :maxdepth: 1


Lambert Problem
---------------

.. automodule:: sempy.core.lambert.lambert_pbm
   :members:
   :show-inheritance:
   :inherited-members:


Lambert Problem in the CR3BP
----------------------------

.. automodule:: sempy.core.lambert.cr3bp_lambert_pbm
   :members:
   :show-inheritance:
   :inherited-members:

Optimization Module
-------------------

.. automodule:: sempy.core.lambert.opt_pbm
   :members:
   :show-inheritance:
   :inherited-members:


Solver Module
-------------

.. automodule:: sempy.core.lambert.opt_solver
   :members:
   :show-inheritance:
   :inherited-members:


Primer Vector
-------------

.. automodule:: sempy.core.lambert.primer_vector
   :members:
   :show-inheritance:
   :inherited-members:


Utils Module
------------

.. automodule:: sempy.core.lambert.utils
   :members:
   :show-inheritance:
   :inherited-members:



