{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\n# Halo orbit manifolds.\n\nThis script demonstrates how to generate manifold tubes associated to a given Halo in the\nEarth-Moon system.\n\nAllowed tubes are all possible combinations of stability (stable/unstable) and way\n(interior/exterior) and can be computed one by one or all together in a single function call.\nFor each tube, several positions along the orbit must be specified to compute a discrete set of\nmanifold branches that approximates the continuous tube.\n\nEach branch is described by an object of the `CrtbpOrbit` inner class `ManifoldBranch` and\nautomatically appended to the orbit's attribute `manifolds` when one of the two methods\n`CrtbpOrbit.all_manifolds_computation()` or `CrtbpOrbit.single_manifold_computation()` is called.\n\n@author: Alberto FOSSA', Paolo GUARDABASSO\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Firstly, all required modules and classes must be imported:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import matplotlib.pyplot as plt\nimport numpy as np\n\nfrom sempy.core.init.primary import Primary\nfrom sempy.core.init.cr3bp import Cr3bp\nfrom sempy.core.orbits.halo import Halo\nfrom sempy.core.plotting.util import sphere, set_axes_equal\nfrom sempy.core.diffcorr.ode_event import generate_impact_event\n\n\ndef set_axes_lim_labels_title(ax, title):\n    \"\"\"Convenience function to set the axes limits, labels and the figure title. \"\"\"\n    ax.set_xlabel('x [-]')\n    ax.set_ylabel('y [-]')\n    ax.set_zlabel('z [-]')\n    ax.set_title(title)\n    set_axes_equal(ax)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Initialize the CR3BP\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Generate the terminal impact event function in a format compatible with the propagation solver\n\"solve_ivp\":\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "m2_impact_event = generate_impact_event(cr3bp, \"m2\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Then, the reference Halo orbit is instantiated and interpolated as described in\n`Halo interpolation` example script:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "halo_1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=60e3)\nhalo_1.interpolation()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "To compute all manifolds tubes at once, we make use of the `CrtbpOrbit` class method\n`all_manifolds_computation()`. Its usage is as simple as:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "halo_1.all_manifolds_computation(11, 20., max_internal_steps=2_000_000)\n\n# Here, it is specified to compute 11 branches for each manifold equally spaced along the orbit.\n# Moreover, each branch will be propagated for an amount of time equal to 20 in non-dimensional\n# units.\n#\n# If a long propagation time is sought, numerical difficulties might be encountered by the\n# integration routines due to an imposed upper limit on the available integrator time steps.\n# To increase this limit, the `max_internal_steps` parameter should be set equal to an\n# higher value w.r.t. the default one specified in `default.ini`.\n#\n# If specific departure states are sought for the branches, the first positional argument can be\n# replaced by a Numpy array containing those positions expressed as fractions of the orbit's\n# period between 0 and 1. For example, passing ``numpy.array([0.25, 0.75])`` will compute two\n# branches per tube that depart the orbit at 1/4 and 3/4 of its period.\n#\n# A single manifold tube can also be computed with a function call similar to the above where\n# two positional arguments must be prepended to specify the manifold stability\n# (stable/unstable) and way (interior/exterior). In this example, a Lunar impact event is passed\n# to the propagator, so that manifolds that impact the surface are not propagated further. Also,\n# the state is propagated together with the State Transition Matrix through the parameter\n# 'with_stm'.\n\nhalo_2 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=50)\nhalo_2.interpolation()\n\nhalo_2.single_manifold_computation('unstable', 'interior', 11, 20., time_steps=20000,\n                                   events=m2_impact_event, with_stm=True)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Finally, if we are interested in a specific branch we can make use of the inner class\n`ManifoldBranch` to compute it directly.\n\nNota: differently as before, the computed branch is not automatically stored in the\norbit class attribute `manifolds`.\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "manifold_branch = halo_2.ManifoldBranch(halo_2, 'stable', 'exterior')\nmanifold_branch.computation(0.25, 20.)\n\n# where the two positional arguments in `ManifoldBranch.crtbp()` refer to the branch\n# position and propagation time respectively."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The obtained trajectories are then plotted as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig1 = plt.figure()\nax1 = fig1.add_subplot(111, projection='3d')\n\n# Plot Moon\nx_2, y_2, z_2 = sphere(100, cr3bp.R2 / cr3bp.L, cr3bp.m2_pos)\nax1.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)\n\n# Plot libration point\npos_l2 = np.array(cr3bp.l2.position)\nax1.scatter(pos_l2[0], pos_l2[1], pos_l2[2], marker='*', color='g')\n\n\nax1.plot(halo_1.state_vec[:, 0], halo_1.state_vec[:, 1], halo_1.state_vec[:, 2], 'k')\nfor kind in halo_1.manifolds:\n    for branch in halo_1.manifolds[kind]:\n        COLOR = 'r' if branch.stability == 'unstable' else 'g'\n        ax1.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2],\n                 color=COLOR)\nset_axes_lim_labels_title(ax1, 'Stable/unstable, interior/exterior manifold tubes')"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "fig2 = plt.figure()\nax2 = fig2.add_subplot(111, projection='3d')\n\n# Plot Moon\nax2.plot_surface(x_2, y_2, z_2, rstride=4, cstride=4, color='gray', linewidth=0)\n\nax2.plot(halo_2.state_vec[:, 0], halo_2.state_vec[:, 1], halo_2.state_vec[:, 2], color='b')\nfor branch in halo_2.manifolds['unstable_interior']:\n    ax2.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2], color='r')\nax2.plot(manifold_branch.state_vec[:, 0], manifold_branch.state_vec[:, 1],\n         manifold_branch.state_vec[:, 2], color='g')\nset_axes_lim_labels_title(ax2, 'Unstable interior tube, stable exterior branch')\nplt.show()"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}