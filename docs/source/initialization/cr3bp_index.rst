Cr3bp Module
==================
.. toctree::
   :maxdepth: 1

.. automodule:: sempy.core.init.cr3bp
   :members:
   :show-inheritance:
   :inherited-members:
