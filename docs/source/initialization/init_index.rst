Initialization Subpackage
==========================

@author: Edgar PEREZ

Modules
-------

.. toctree::
    :maxdepth: 1

    constants_index
    defaults_index
    primary_index
    cr3bp_index
    ephemeris_index
