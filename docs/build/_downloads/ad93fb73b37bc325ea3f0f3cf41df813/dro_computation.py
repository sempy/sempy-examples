"""
Distant Retrograde Orbit Computation.
=======================

@author: Edgar PEREZ, Alberto FOSSA', Paolo GUARDABASSO
"""

# %%
# 1) Imports
#
# To run a script it is necessary to import modules and classes needed to perform the required task,
# in this script we will compute a Distant Retrograde Orbit (DRO), therefore the following
# modules and classes are imported from the package.

import numpy as np
import matplotlib.pyplot as plt

from sempy.core.init.primary import Primary
from sempy.core.init.cr3bp import Cr3bp
from sempy.core.orbits.dro import DRO

# %%
# 2) Initialization and Computation
#
# To compute an orbit from a first guess, the first step is to create the CRTBP system of interest,
# this task is done by using Cr3bp class.
# As arguments, the user needs to pass the primaries to the Cr3bp class constructor, m1 and m2,
# just by using two instances of Primary class, for example: Primary.SUN and Primary.JUPITER.
# The primaries that are available in the package can be found in primary module. The CRTBP
# system of interest is contained in an object (cr3bp), as follows:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# Second step, initialize the orbit desired to be studied, here the user has to use the class
# corresponding to the desired orbit.
# For DROs, three required arguments must be passed to the class constructor:
# - The cr3bp object created before;
# - The center body around which we want our spacecraft to orbit;
# - An initial state OR the horizontal extension OR the orbital period
#
# When initialised through the orbital period and horizontal extensions, Limits apply to the
# initial approximation, computed supposing a 2-body problem orbit: for orbits that are
# far away from their central body, the kepler orbit approximation generates another kind of
# orbit or does not converge at all.
#
# The DRO initialization will be:

orbit = DRO(cr3bp, cr3bp.m2, state0=np.array([1-cr3bp.mu - 0.01, 0, 0, 0, 0.1, 0]))
# orbit = DRO(cr3bp, cr3bp.m2, Axdim=400000)
# orbit = DRO(cr3bp, cr3bp.m2, T=2*24*60*60/cr3bp.T*2*np.pi)

# %%
# Third step, the initial state goes through a process of differential correction until a closed
# orbit is found. To perform this task, it is only necessary to state the following line of code:

orbit.computation()

# 3) Visualisation
desc = orbit.get_abacus_name(separator=' ')

fig1, ax1 = plt.subplots(1, 1)
ax1.axis('equal')

fig1.suptitle(desc + ' in rotating reference frame')

# Plot Moon and Earth
prim_1 = plt.Circle((cr3bp.m1_pos[0], 0), cr3bp.R1 / cr3bp.L, facecolor='b', edgecolor='k',
                    linewidth=.25)
prim_2 = plt.Circle((cr3bp.m2_pos[0], 0), cr3bp.R2 / cr3bp.L, facecolor='gray', edgecolor='k',
                    linewidth=.25)
ax1.add_artist(prim_1)
ax1.add_artist(prim_2)

ax1.plot(orbit.state_vec[:, 0], orbit.state_vec[:, 1], color='tab:orange')

ax1.set_xlabel('x')
ax1.set_ylabel('y')
